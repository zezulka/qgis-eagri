#!/bin/bash
: '
The following script generates model for services listed in the services.txt file present in the
same directory as this script.

Generated model is a list of Python dataclasses which are read-only. No custom type mappings are done.

Model is automatically moved over to the plugin, making it instantly available for use by Eagris.
If model for the given service already exists in Eagris, it will be replaced by the newest definition from eAGRI.

Prerequisites:
- model is generated using the "xsdata" executable, make sure you have it installed and on your PATH
- what worked for my linux machine was pip install --user xsdata[cli]
'
set -eu

SERVICE_DEF_FILE=services.txt
OUTPUT_DESTINATION=../../eagris/eagri/ws/generated/

SERVICES=$(cat $SERVICE_DEF_FILE)
SERVICES_ONELINER=$(echo $(echo "${SERVICES[@]}"))
SUCCESSFULL_SERVICES=
FAILED_SERVICES=

trap \
 "rm -rf eagris" \
 SIGINT SIGTERM ERR EXIT

if ! command -v xsdata &> /dev/null
then
    echo "'xsdata' could not be found and is the required executable for this script. Consider installing it first."
    exit 1
fi

echo "$(tput setaf 6)========= generating models for $SERVICES_ONELINER =========$(tput sgr0)"

while read -r service
do
    echo
    echo "$(tput setaf 6)========= generating model for $service =========$(tput sgr0)"
    PROBE=$(curl -s https://eagri.cz/ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.aspx?SERVICEID="$service")
    if [[ "${PROBE}" =~ http://www.pds.eu/vOKO/v0200  ]]; then
        # https://xsdata.readthedocs.io/en/latest/codegen.html
        serviceid="$(echo "$service" | sed s/_//g | tr '[:upper:]' '[:lower:]')"
        xsdata https://eagri.cz/ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.aspx?SERVICEID="$service" \
            --package eagris.eagri.ws.generated."$serviceid" \
            --structure-style clusters \
            --relative-imports \
            --frozen
        SUCCESSFULL_SERVICES+=("$service")
    else
	      FAILED_SERVICES+=("$service")
    fi
done < $SERVICE_DEF_FILE

echo "$(tput setaf 2)OK: $(echo $(echo "${SUCCESSFULL_SERVICES[@]}")) $(tput setaf 1) NOK: $(echo $(echo "${FAILED_SERVICES[@]}")) $(tput sgr0)"
echo "$(tput setaf 2)moving generated model tree to $(realpath $OUTPUT_DESTINATION)$(tput sgr0)"
# move generated output directly to plugin
rsync -a -v --remove-source-files eagris/eagri/ws/generated/ $OUTPUT_DESTINATION
