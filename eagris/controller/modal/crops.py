import os
from datetime import date, timedelta
from typing import List, Optional

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QDialog, QComboBox, QDialogButtonBox, QAbstractButton
from qgis.gui import QgsDateTimeEdit

from eagris.common.qt_constants import NO_ITEM_SELECTED_INDEX
from eagris.controller.constants import EAGRI_CROP_MODAL_UI_PATH
from eagris.controller.panel.crops import table_widget_crops
from eagris.controller.panel.panel import QgisEagriPanel
from qgis.PyQt import uic

from eagris.eagri.crop_list.service import crop_list
from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelCrop
from eagris.eagri.ws.lpi_gpl01_c.model.response import EagriCrop
from eagris.qgis.feature_mapper import eagri_crop_to_tree_item
from eagris.temporal.interval import intervals_intersect


class QgisCropsModal(QDialog):
    dateFromEdit: QgsDateTimeEdit
    dateToEdit: QgsDateTimeEdit
    cropsComboBox: QComboBox
    buttonBox: QDialogButtonBox

    def __init__(self, panel: QgisEagriPanel):
        QDialog.__init__(self)
        ui_path = os.path.join(os.path.dirname(__file__), "..", EAGRI_CROP_MODAL_UI_PATH)
        uic.loadUi(ui_path, self)
        self.__panel = panel
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
        self.dateToEdit.clear()
        self.dateToEdit.setEmpty()
        self.cropsComboBox.currentIndexChanged.connect(self.__enable_ok_button_when_user_selects_crop)
        self.dateFromEdit.dateChanged.connect(self.__enable_ok_button_when_user_selects_compatible_date)
        self.dateToEdit.dateChanged.connect(self.__enable_ok_button_when_user_selects_compatible_date)

    # yet again, pyqtSlot does not seem to work in this setup
    def __enable_ok_button_when_user_selects_crop(self, ix):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(self.__is_form_valid())

    def __enable_ok_button_when_user_selects_compatible_date(self, date):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(self.__is_form_valid())

    @pyqtSlot(name="on_buttonBox_buttonClicked")
    def __validate_crop_form(self, btn: QAbstractButton):
        if self.buttonBox.buttonRole(btn) == QDialogButtonBox.ButtonRole.AcceptRole:
            self.__do_validate_crop_form()
        else:
            pass  # no-op

    def __do_validate_crop_form(self):
        if self.__is_form_valid():
            self.accept()
        else:
            self.reject()

    def __is_form_valid(self):
        return (
                self.cropsComboBox.currentIndex() > NO_ITEM_SELECTED_INDEX
                and self.dateFromEdit.date() is not None
                and
                self.__crop_from_form_does_not_intersect_with_existing_crops(
                    self.dateFromEdit.date().toPyDate(),
                    self.dateToEdit.date().toPyDate() if self.dateToEdit.date() else None
                )
        )

    def __crop_from_form_does_not_intersect_with_existing_crops(
            self,
            new_crop_valid_from: date,
            new_crop_valid_to: date
    ):
        all_current_crops = table_widget_crops(self.__panel.parcelDetailCrops)
        return len([
            c for c in
            all_current_crops[:-1]
            if intervals_intersect(new_crop_valid_from, new_crop_valid_to, c.valid_from, c.valid_to)
        ]) == 0 and self.__crop_from_form_is_compatible_with_last_crop(
            all_current_crops,
            new_crop_valid_from,
            new_crop_valid_to
        )

    def __crop_from_form_is_compatible_with_last_crop(
            self, crops: List[ParcelCrop], new_crop_valid_from: date, new_crop_valid_to: date
    ):
        return (not intervals_intersect(
            new_crop_valid_from,
            new_crop_valid_to,
            crops[-1].valid_from,
            crops[-1].valid_to
        ) or crops[-1].valid_from < new_crop_valid_from) if crops else True

    @pyqtSlot(name="on_buttonBox_accepted")
    def __add_crop(self):
        if self.__is_form_valid():
            self.__do_add_crop(
                self.cropsComboBox.currentData(),
                self.dateFromEdit.date().toPyDate(),
                self.dateToEdit.date().toPyDate() if self.dateToEdit.date() else None
            )

    def __do_add_crop(
            self,
            current_legislative_code: int,
            valid_from: date,
            valid_to: Optional[date]
    ):
        self.__modify_valid_to_of_last_crop_if_necessary(valid_from)
        self.__panel.parcelDetailCrops.addTopLevelItem(
            eagri_crop_to_tree_item(current_legislative_code, valid_from, valid_to)
        )

    def __modify_valid_to_of_last_crop_if_necessary(self, new_valid_from):
        if table_widget_crops(self.__panel.parcelDetailCrops):
            parcel_crop: ParcelCrop = (self.__panel.parcelDetailCrops
                                       .topLevelItem(self.__panel.parcelDetailCrops.topLevelItemCount() - 1)
                                       .data(0, Qt.UserRole))
            new_valid_to = self.__cut_valid_to_if_new_crop_is_younger(new_valid_from, parcel_crop)
            self.__panel.parcelDetailCrops.takeTopLevelItem(
                self.__panel.parcelDetailCrops.topLevelItemCount() - 1
            )
            self.__panel.parcelDetailCrops.addTopLevelItem(
                eagri_crop_to_tree_item(parcel_crop.crop_id, parcel_crop.valid_from, new_valid_to)
            )

    def __cut_valid_to_if_new_crop_is_younger(self, new_valid_from, c: ParcelCrop):
        return new_valid_from - timedelta(days=1) if c.valid_from < new_valid_from else c.valid_to

    def showEvent(self, e: QtGui.QShowEvent) -> None:
        super(QDialog, self).showEvent(e)
        if self.cropsComboBox.count() == 0:
            print("crops combobox is empty, populating with eagri crops...")
            self.__add_crops_to_combo_box(crop_list())

    def __add_crops_to_combo_box(self, crops: List[EagriCrop]):
        [
            self.cropsComboBox.addItem(c.name, c.id)
            for c in crops
        ]
