# -*- coding: utf-8 -*-

from PyQt5.QtCore import QThreadPool, pyqtSlot
from PyQt5.QtWidgets import QLineEdit, QPushButton, QComboBox, QRadioButton, QProgressBar
from qgis.gui import QgisInterface

from eagris.controller.worker.add_auth_data import AddAuthDataWorker
from eagris.eagri.auth.ws_key_hash_method import WsKeyHashMethod
from eagris.model.auth import EagriAuthData
from eagris.model.form import EagriModalData
from eagris.controller.panel.panel import QgisEagriPanel
from eagris.eagri.env import EagriEnv
from eagris.controller.constants import EAGRI_MODAL_UI_PATH

try:
    from qgis.PyQt.QtGui import QDialog
except ImportError:
    from qgis.PyQt.QtWidgets import QDialog

from qgis.PyQt import uic
import os


class QgisEagriAuthDataModal(QDialog):
    nameInput: QLineEdit
    loginInput: QLineEdit
    wsKeyInput: QLineEdit
    szridInput: QLineEdit
    envComboBox: QComboBox
    forceAddBtn: QPushButton
    addAuthBtn: QPushButton
    hashedWsKeyRadioButton: QRadioButton
    plainWsKeyRadioButton: QRadioButton
    progressBar: QProgressBar

    def __init__(self, iface: QgisInterface, panel: QgisEagriPanel):
        QDialog.__init__(self)
        ui_path = os.path.join(os.path.dirname(__file__), "..", EAGRI_MODAL_UI_PATH)
        uic.loadUi(ui_path, self)
        self.progressBar.hide()
        self.__iface = iface
        self.__credsTree = panel.creds_tree
        self.__addAuthDataWorker = AddAuthDataWorker(
            self.__iface,
            self.__credsTree,
            self.__add_auth_data_worker_success_callback,
            self.__add_auth_data_worker_finish_callback
        )
        self.__setup_text_input_change_event_listeners()
        self.__fill_envs()
        self.__threadpool = QThreadPool.globalInstance()

    def __add_auth_data_worker_success_callback(self, auth_data):
        self.__credsTree.add_auth_item(auth_data)
        self.wsKeyInput.clear()
        self.nameInput.clear()
        self.szridInput.clear()
        self.loginInput.clear()
        self.close()

    def __add_auth_data_worker_finish_callback(self):
        self.progressBar.hide()

    def __setup_text_input_change_event_listeners(self):
        [t.textChanged.connect(self.__check_all_eagri_data_filled_in) for t in [
            self.loginInput,
            self.wsKeyInput,
            self.szridInput
        ]]

    @pyqtSlot(name="on_addAuthBtn_clicked")
    def __add_auth_data_to_panel(self):
        (login, ws_key, szrid, name, env) = self.__collect_form_data()
        auth_data = EagriAuthData(
            login=login,
            wskey=ws_key,
            szrid=szrid,
            name=name,
            env=env
        )
        self.progressBar.show()
        self.__addAuthDataWorker.start(auth_data)

    @pyqtSlot(name="on_forceAddBtn_clicked")
    def __force_add_auth_data_to_panel(self):
        (login, ws_key, szrid, name, env) = self.__collect_form_data()
        auth_data = EagriAuthData(
            login=login,
            wskey=ws_key,
            szrid=szrid,
            name=name,
            env=env
        )
        self.__credsTree.add_auth_item(auth_data)

    def __fill_envs(self):
        self.envComboBox.addItems([EagriEnv.PROD.name, EagriEnv.TEST.name])
        self.envComboBox.setCurrentIndex(0)

    def __collect_form_data(self):
        return EagriModalData(
            login=self.loginInput.text(),
            ws_key=self.__collect_ws_key(),
            szrid=self.szridInput.text(),
            name=self.nameInput.text(),
            env=EagriEnv.fromStr(self.envComboBox.currentText())
        )

    def __collect_ws_key(self):
        hash_method = WsKeyHashMethod.SHA1 if self.hashedWsKeyRadioButton.isChecked() else WsKeyHashMethod.NONE
        return hash_method.hex_hash(self.wsKeyInput.text())

    def __check_all_eagri_data_filled_in(self, _text: str):
        self.addAuthBtn.setEnabled(self.__all_required_items_are_filled_in())
        self.forceAddBtn.setEnabled(self.__all_required_items_are_filled_in())

    def __all_required_items_are_filled_in(self):
        name = self.loginInput.text().strip()
        ws_key = self.wsKeyInput.text().strip()
        szrid = self.szridInput.text().strip()
        return all(s for s in [name, ws_key, szrid])
