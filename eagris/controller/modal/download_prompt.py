import os

from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QAbstractButton
from dateutil.utils import today
from qgis.gui import QgsDateTimeEdit, QgsCheckableComboBox, QgisInterface

from eagris.common.download_vector_feature import DownloadVectorFeature
from eagris.controller.constants import EAGRI_DOWNLOAD_PROMPT_UI_PATH
from eagris.controller.panel.panel import QgisEagriPanel
from eagris.controller.worker.eagri_data_download_worker import EagriDataDownloadWorker


class QgisEagriDownloadPrompt(QDialog):
    buttonBox: QDialogButtonBox
    validityDateEdit: QgsDateTimeEdit
    checkableComboBox: QgsCheckableComboBox

    def __init__(
            self,
            iface: QgisInterface,
            panel: QgisEagriPanel
    ):
        QDialog.__init__(self)
        ui_path = os.path.join(os.path.dirname(__file__), "..", EAGRI_DOWNLOAD_PROMPT_UI_PATH)
        uic.loadUi(ui_path, self)
        self.__panel = panel
        self.__download_data_worker = EagriDataDownloadWorker(iface)
        self.__setup_dialog()
        self.buttonBox.clicked.connect(self.__download_vector_layers)

    def __download_vector_layers(self, btn: QAbstractButton):
        self.hide()
        if self.buttonBox.buttonRole(btn) == QDialogButtonBox.ButtonRole.AcceptRole:
            for i in self.__panel.eagriUsersTreeWidget.selectedItems():
                auth_data = i.data(0, Qt.UserRole)
                print(f"Importing eAGRI vector layers for auth item {auth_data}...")
                self.__download_data_worker.start(
                    auth_data,
                    self.validityDateEdit.date().toPyDate(),
                    [DownloadVectorFeature[d] for d in self.checkableComboBox.checkedItemsData()]
                )
        self.__reset_dialog()

    def __setup_dialog(self):
        for feature in DownloadVectorFeature:
            self.checkableComboBox.addItemWithCheckState(feature.value, Qt.CheckState.Checked, feature.name)
        self.validityDateEdit.setDate(today())

    def __reset_dialog(self):
        self.checkableComboBox.selectAllOptions()
        self.validityDateEdit.setDate(today())
