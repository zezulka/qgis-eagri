"""
    TODO i18n
"""
from enum import Enum


class Lpiddb01bParcelAttribute(Enum):
    # join field
    ID = 'IDPARCELA'
    NAME = 'NAZEV'
    VALID_FROM = 'PARCELAOD'
    VALID_TO = 'PARCELADO'
    IDDPB = 'IDDPB'


class Lpiddb01bParcelVersionAttribute(Enum):
    # join field
    PARCEL_ID = 'IDPARCELA'
    VALID_FROM = 'PARCELAVERZEOD'
    VALID_TO = 'PARCELAVERZEDO'
    VERSION_ID = 'IDPARCELAVERZE'
    AREA = 'VYMERA'
    GEOMETRY = 'GEOMETRIE'


class Lpiddb01bParcelCropAttribute(Enum):
    PARCEL_ID = 'IDPARCELA'
    CROP_ID = 'KODPLODINY'
    CATCH_CROP = 'MEZIPLODINA'
    VALID_FROM = 'PLATNOSTOD'
    VALID_TO = 'PLATNOSTDO'


class Lpigdp11bAttribute(Enum):
    IDDPB = 'IDDPB'
    SQUARE = 'CTVEREC'
    ZKOD = 'ZKOD'
    STATUS = 'STAV'
    VALID_FROM = 'PLATNOSTOD'
    VALID_TO = 'PLATNOSTDO'
    AREA = 'VYMERA'
    CULTURE = 'KULTURA'
    CULTURE_NAME = 'KULTURANAZEV'
    GEOMETRY = 'GEOMETRIE'


class Lpigpl01cParcelCropAttribute(Enum):
    CROP_NAME = 'NAZEVPLODINY'
