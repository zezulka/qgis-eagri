from qgis.core import QgsPalLayerSettings, QgsVectorLayerSimpleLabeling, QgsVectorLayer

from eagris.controller.attribute_table_column_names import Lpigdp11bAttribute


def enable_dpb_layer_labeling(vector_layer: QgsVectorLayer):
    label_settings = QgsPalLayerSettings()
    label_settings.enabled = True
    label_settings.fieldName = Lpigdp11bAttribute.ZKOD.value

    # Create the labeling object
    labeling = QgsVectorLayerSimpleLabeling(label_settings)

    # Enable labeling for the layer
    vector_layer.setLabeling(labeling)
    vector_layer.setLabelsEnabled(True)
