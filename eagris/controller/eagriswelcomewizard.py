import os
from enum import Enum
from pathlib import Path
from typing import Callable

from PyQt5.QtWidgets import QWizard, QWizardPage, QGridLayout
from qgis.PyQt import uic
from qgis.gui import QgsFileWidget, QgisInterface

from eagris.common.plugin_constants import ALLOWED_AUTH_FILE_SUFFIXES
from eagris.configuration.cred_settings import setup_local_creds
from eagris.controller.constants import EAGRI_WELCOME_WIZARD_UI_PATH
from eagris.filesystem.utils import default_auth_file_path, make_file, path_with_default_file_name, make_dirs


class QgisEagriWelcomeWizard(QWizard):
    donePage: QWizardPage
    authSetupPage: QWizardPage
    authSetupPageGridLayout: QGridLayout
    authPathFileWidget: QgsFileWidget

    def __init__(
            self,
            iface: QgisInterface,
            wizard_done_callback: Callable[[], None]
    ):
        QWizard.__init__(self)
        ui_path = os.path.join(os.path.dirname(__file__), EAGRI_WELCOME_WIZARD_UI_PATH)
        uic.loadUi(ui_path, self)
        self.iface = iface
        self.find_finish_button().clicked.connect(self.__handle_finish_button_click)
        self.wizardDoneCallback = wizard_done_callback
        self.authPathFileWidget.setFilePath(default_auth_file_path())

    def __handle_finish_button_click(self):
        self.wizardDoneCallback()

    def validateCurrentPage(self) -> bool:
        # first of all, check that user didn't type rubbish
        if self.__auth_file_input_type() == _AuthFileInputType.UNKNOWN:
            return False
        # if not, create necessary files
        else:
            user_path = self.authPathFileWidget.filePath()
            final_path = self.__finalize_path(user_path)
            make_dirs(final_path.parent)
            make_file(final_path)
            return setup_local_creds(auth_creds_path=os.path.abspath(final_path))

    def __finalize_path(self, user_path) -> Path:
        file_input_type = self.__auth_file_input_type()
        if file_input_type == _AuthFileInputType.FILE:
            return Path(user_path)
        elif file_input_type == _AuthFileInputType.DIRECTORY:
            return path_with_default_file_name(user_path)
        else:
            raise ValueError("Unsupported file input type.")

    def __auth_file_input_type(self):
        user_input = self.authPathFileWidget.filePath()
        if os.path.isdir(user_input):
            return _AuthFileInputType.DIRECTORY
        elif user_input.endswith(tuple(ALLOWED_AUTH_FILE_SUFFIXES)):
            return _AuthFileInputType.FILE
        else:
            return _AuthFileInputType.UNKNOWN

    def find_finish_button(self):
        return self.button(QWizard.WizardButton.FinishButton)


class _AuthFileInputType(Enum):
    DIRECTORY = 'DIR'
    # absolute path of a directory where the auth file will reside
    FILE = 'FILE'
    # absolute path of the actual auth file
    UNKNOWN = 'N/A'
