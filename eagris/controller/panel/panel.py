# -*- coding: utf-8 -*-
from typing import Callable

from PyQt5.QtCore import Qt, QUrl, pyqtSlot
from PyQt5.QtGui import QKeyEvent, QDesktopServices
from PyQt5.QtWidgets import QDockWidget, QTreeWidgetItem, QToolButton, QAction, QTreeWidget, QLineEdit, QWidget, \
    QTabWidget, QTableWidget, QComboBox, QLabel
from qgis.gui import QgisInterface

from eagris.common.plugin_constants import DOCUMENTATION_URL
from eagris.controller.panel.credtree import EagrisCredentialsTree
from eagris.controller.constants import EAGRI_PANEL_UI_PATH

from qgis.PyQt import uic
import os

from eagris.controller.panel.parcel_detail_tab import EagrisParcelDetailTab
from eagris.controller.panel.update_handler import ParcelDetailUpdateHandler


class QgisEagriPanel(QDockWidget):
    """
    TODO Split general qgis panel logic from eagri credential tab logic
    """
    eagriUsersTreeWidget: QTreeWidget
    parcelDetail: QWidget
    parcelDetailCrops: QTableWidget
    eagriUsers: QWidget
    tabWidget: QTabWidget
    toolButton: QToolButton
    helpButton: QToolButton
    downloadButton: QToolButton
    deleteButton: QToolButton
    parcelDetailNameEdit: QLineEdit
    addCropButton: QToolButton
    removeCropButton: QToolButton
    updateParcelButton: QToolButton
    eagriUserComboBox: QComboBox
    blockIdLabel: QLabel
    parcelIdLabel: QLabel

    def __init__(
            self,
            iface: QgisInterface,
            # callback contains reference to the modal, evade circular dependency between components
            default_add_auth_data_callback: Callable[[], None],
            add_crop_button_callback: Callable[[], None],
            download_vector_layers_callback: Callable[[], None]
    ):
        QDockWidget.__init__(self)
        ui_path = os.path.join(os.path.dirname(__file__), "..", EAGRI_PANEL_UI_PATH)
        uic.loadUi(ui_path, self)
        self.__default_add_auth_data_callback = default_add_auth_data_callback
        self.__download_vector_layers_callback = download_vector_layers_callback
        self.__iface = iface
        self.__parcel_update_handler = ParcelDetailUpdateHandler(
            block_id_label=self.blockIdLabel,
            parcel_id_label=self.parcelIdLabel,
            parcel_name_edit=self.parcelDetailNameEdit,
            parcel_detail_crops=self.parcelDetailCrops
        )
        self.__setup_eagri_users_tree_widget()
        self.creds_tree = EagrisCredentialsTree(self.eagriUsersTreeWidget)
        self.parcel_detail_tab = EagrisParcelDetailTab(
            iface=self.__iface,
            eagri_users=self.eagriUsers,
            eagri_user_combo_box=self.eagriUserComboBox,
            add_crop_button=self.addCropButton,
            remove_crop_button=self.removeCropButton,
            update_parcel_button=self.updateParcelButton,
            parcel_detail=self.parcelDetail,
            parcel_detail_crops=self.parcelDetailCrops,
            parcel_detail_name_edit=self.parcelDetailNameEdit,
            tab_widget=self.tabWidget,
            block_id_label=self.blockIdLabel,
            parcel_id_label=self.parcelIdLabel,
            creds_tree=self.creds_tree,
            add_crop_button_callback=add_crop_button_callback
        )
        self.__setup_tool_button_actions()
        self.__setup_parcel_detail_tool()

    def __setup_eagri_users_tree_widget(self):
        self.eagriUsersTreeWidget.keyPressEvent = self.__handle_key_press
        # this signal does not seem to work with @pyqtSlot
        self.eagriUsersTreeWidget.itemDoubleClicked.connect(self.__handle_creds_tree_double_click)

    def __setup_parcel_detail_tool(self):
        self.__iface.mapCanvas().selectionChanged.connect(self.__parcel_update_handler.update_parcel_detail)

    def __tool_button_actions(self):
        import_manually = QAction("Manually", parent=self.toolButton)
        import_from_db = QAction("DB (TODO)", parent=self.toolButton)
        import_manually.triggered.connect(self.__default_add_auth_data_callback)
        import_from_db.setEnabled(False)
        return [import_manually, import_from_db]

    def __setup_tool_button_actions(self):
        icon = self.toolButton.icon()
        actions = self.__tool_button_actions()
        self.toolButton.setText(None)
        self.toolButton.setIcon(icon)
        self.toolButton.setDefaultAction(actions[0])
        self.toolButton.addActions(actions)
        self.toolButton.setText(None)
        self.toolButton.setIcon(icon)
        self.toolButton.setPopupMode(QToolButton.ToolButtonPopupMode.MenuButtonPopup)

    def __handle_creds_tree_double_click(self, _item: QTreeWidgetItem):
        self.__download_vector_layers_callback()

    @pyqtSlot(name="on_deleteButton_clicked")
    def __handle_delete_button_click(self):
        self.creds_tree.remove_all_selected_items()

    @pyqtSlot(name="on_toolButton_clicked")
    def __handle_tool_button_click(self):
        self.__default_add_auth_data_callback()

    @pyqtSlot(name="on_downloadButton_clicked")
    def __handle_download_button_click(self):
        self.__download_vector_layers_callback()

    @pyqtSlot(name="on_helpButton_clicked")
    def __handle_help_button_click(self):
        QDesktopServices.openUrl(QUrl(DOCUMENTATION_URL))

    def __handle_key_press(self, event: QKeyEvent):
        if event.key() == Qt.Key_Delete:
            self.creds_tree.remove_all_selected_items()
