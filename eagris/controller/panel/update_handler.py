from typing import List

from PyQt5.QtWidgets import QLineEdit, QTreeWidget, QLabel
from qgis.core import QgsFeature, QgsProject, QgsVectorLayer

from eagris.common.plugin_constants import QGIS_TABLE_PARCEL_PREFIX
from eagris.common.vector_layer_type import VectorLayerType
from eagris.controller.attribute_table_column_names import Lpiddb01bParcelVersionAttribute, Lpiddb01bParcelAttribute
from eagris.qgis.feature_mapper import crop_feature_to_tree_item

"""
    Much cleaner and nicer way of achieving the same thing would be to create
    a new instance of the QgsMapToolIdentifyFeature and connect custom logic to
    featureIdentified.

    Due to various canvas refreshes, though, the custom map tool is destroyed (or rather replaced) by
    QGIS after first map interaction. :/
"""


class ParcelDetailUpdateHandler:
    def __init__(
            self,
            block_id_label: QLabel,
            parcel_id_label: QLabel,
            parcel_name_edit: QLineEdit,
            parcel_detail_crops: QTreeWidget
    ):
        self.__block_id_label = block_id_label
        self.__parcel_id_label = parcel_id_label
        self.__parcel_name_edit = parcel_name_edit
        self.__crops_tree_widget = parcel_detail_crops

    def update_parcel_detail(self, layer: QgsVectorLayer):
        if layer.metadata().identifier().endswith(VectorLayerType.AGRICULTURAL_PARCEL_VERSION.layer_id()):
            self.__clear_parcel_detail()
            if len(layer.selectedFeatures()) > 0:
                crop_layers, parent_layer_id = self.__find_crop_layers(layer)
                self.__do_update_parcel_detail(crop_layers, layer, parent_layer_id)

    def __do_update_parcel_detail(self, crop_layers, layer, parent_layer_id):
        if len(crop_layers) > 0:
            last_selected_feature = layer.selectedFeatures()[-1]
            parcel_id_ix = last_selected_feature.fieldNameIndex(Lpiddb01bParcelVersionAttribute.PARCEL_ID.value)
            parcel_id = last_selected_feature.attributes()[parcel_id_ix]

            self.__block_id_label.setText(str(self.__find_block_id_in_attribute_map(last_selected_feature)))
            self.__parcel_id_label.setText(str(parcel_id))
            self.add_crops_to_tree_widget(crop_layers[0], parcel_id)
            self.__parcel_name_edit.setText(self.__find_parcel_name_in_attribute_map(last_selected_feature))
        else:
            print(f"No crop layers were found for layers with parent {parent_layer_id}, "
                  f"skipping parcel detail update.")

    def __find_crop_layers(self, layer):
        parent_layer_id = layer.metadata().parentIdentifier()
        print(f"Searching for crop layers with id {self.__crop_layer_id(parent_layer_id)} ...")
        crop_layers: List[QgsVectorLayer] = [
            # QgsProject.instance().mapLayers() is another QGIS 3-specific feature
            layer for layer in QgsProject.instance().mapLayers().values()
            if self.__vector_layer_is_crop_layer(layer, parent_layer_id)
        ]
        return crop_layers, parent_layer_id

    def add_crops_to_tree_widget(self, crop_layer, parcel_id):
        [
            self.__crops_tree_widget.addTopLevelItem(crop_feature_to_tree_item(f))
            for f in crop_layer.getFeatures()
            if f.attributes()[f.fieldNameIndex(Lpiddb01bParcelVersionAttribute.PARCEL_ID.value)] == parcel_id
        ]

    def __find_parcel_name_in_attribute_map(self, f: QgsFeature):
        default_name = ""
        return f.attributeMap().get(f"{QGIS_TABLE_PARCEL_PREFIX}{Lpiddb01bParcelAttribute.NAME.value}", default_name)

    def __find_block_id_in_attribute_map(self, f: QgsFeature):
        default_name = ""
        return f.attributeMap().get(f"{QGIS_TABLE_PARCEL_PREFIX}{Lpiddb01bParcelAttribute.IDDPB.value}", default_name)

    def __vector_layer_is_crop_layer(self, layer, parent_layer_id):
        return (
                isinstance(layer, QgsVectorLayer)
                and layer.metadata().identifier() == self.__crop_layer_id(parent_layer_id)
        )

    def __crop_layer_id(self, parent_id):
        return f"{parent_id}-{VectorLayerType.AGRICULTURAL_PARCEL_CROP.layer_id()}"

    def __clear_parcel_detail(self):
        self.__parcel_name_edit.setText("")
        self.__crops_tree_widget.clear()
