from typing import List

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTreeWidgetItem, QTreeWidget

from eagris.auth.auth_creds_service import insert, find_all, delete
from eagris.model.auth import EagriAuthData, logically_equal


def eagri_auth_to_tree_item(eagri_auth: EagriAuthData):
    item = QTreeWidgetItem()
    # 'None' is displayed if the value is 'None', this is a workaround to display an empty cell instead
    item.setText(0, str(eagri_auth.name) if eagri_auth.name else "")
    item.setText(1, str(eagri_auth.login))
    item.setText(2, str(eagri_auth.wskey))
    item.setText(3, str(eagri_auth.szrid))
    item.setText(4, str(eagri_auth.env.name))
    item.setData(0, Qt.UserRole, eagri_auth)
    return item


class EagrisCredentialsTree:
    def __init__(self, tree_widget: QTreeWidget):
        self.__tree_widget = tree_widget
        self.__populate_creds_tree()

    def add_auth_item(self, auth_data: EagriAuthData):
        self.__tree_widget.addTopLevelItem(eagri_auth_to_tree_item(auth_data))
        insert(auth_data)

    def auth_item_exists(self, auth_data: EagriAuthData):
        return any(
            logically_equal(self.__data_for_item(ix), auth_data) for ix in range(0, self.__tree_widget.topLevelItemCount())
        )

    def all_data(self) -> List[EagriAuthData]:
        return [
            self.__tree_widget.topLevelItem(ix).data(0, Qt.UserRole)
            for ix in range(0, self.__tree_widget.topLevelItemCount())
        ]

    def remove_all(self):
        self.__tree_widget.clear()

    def remove_all_selected_items(self):
        for i in self.__tree_widget.selectedItems():
            auth_data = i.data(0, Qt.UserRole)
            delete(auth_data)
            self.__tree_widget.invisibleRootItem().removeChild(i)

    def __data_for_item(self, item_index: int):
        return self.__tree_widget.topLevelItem(item_index).data(0, Qt.UserRole)

    def __populate_creds_tree(self):
        local_creds = find_all()
        self.__tree_widget.addTopLevelItems([eagri_auth_to_tree_item(cred) for cred in local_creds])
