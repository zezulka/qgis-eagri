from typing import List

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTableWidget

from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelCrop


def table_widget_crops(table_widget: QTableWidget) -> List[ParcelCrop]:
    return [
        table_widget.topLevelItem(i).data(0, Qt.UserRole)
        for i in range(table_widget.topLevelItemCount())
    ]
