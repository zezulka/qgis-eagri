from typing import Callable

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QToolButton, QTableWidget, QLineEdit, QTabWidget, QWidget, QComboBox, QLabel
from qgis.core import QgsGeometry
from qgis.gui import QgisInterface

from eagris.common.plugin_constants import PARCEL_DETAIL_TAB_INDEX
from eagris.controller.panel.credtree import EagrisCredentialsTree
from eagris.controller.panel.crops import table_widget_crops
from eagris.controller.worker.upload_parcel_detail_worker import UploadParcelDetailWorker
from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelCrop
from eagris.eagri.ws.lpi_zzp01_b.model.request import ParcelUpdateRequest, ParcelUpdateRequestParcel, \
    ParcelManagementRequestCrop
from eagris.model.auth import EagriAuthData


def combobox_name(eagri_auth: EagriAuthData) -> str:
    return f"[{eagri_auth.env.name}] {eagri_auth.name if eagri_auth.name else eagri_auth.login}"


def to_crop_update(parcel_crop: ParcelCrop):
    return ParcelManagementRequestCrop(
        legislative_code=parcel_crop.crop_id,
        valid_from=parcel_crop.valid_from,
        valid_to=parcel_crop.valid_to,
        catch_crop=parcel_crop.catch_crop
    )


# presumption: geometry is in Krovak (EPSG:5514)
def geometry_area_ha(g: QgsGeometry) -> float:
    return g.area() / 10_000


class EagrisParcelDetailTab:

    def __init__(
            self,
            iface: QgisInterface,
            eagri_users: QWidget,
            eagri_user_combo_box: QComboBox,
            update_parcel_button: QToolButton,
            add_crop_button: QToolButton,
            remove_crop_button: QToolButton,
            parcel_detail: QWidget,
            parcel_detail_crops: QTableWidget,
            parcel_detail_name_edit: QLineEdit,
            tab_widget: QTabWidget,
            block_id_label: QLabel,
            parcel_id_label: QLabel,
            creds_tree: EagrisCredentialsTree,
            add_crop_button_callback: Callable[[], None]
    ):
        self.eagri_users = eagri_users
        self.add_crop_button = add_crop_button
        self.remove_crop_button = remove_crop_button
        self.parcel_detail = parcel_detail
        self.parcel_detail_crops = parcel_detail_crops
        self.parcel_detail_name_edit = parcel_detail_name_edit
        self.tab_widget = tab_widget
        self.eagri_user_combo_box = eagri_user_combo_box

        self.creds_tree = creds_tree
        self.block_id_label = block_id_label
        self.parcel_id_label = parcel_id_label
        self.update_parcel_button = update_parcel_button
        self.__add_crop_button_callback = add_crop_button_callback
        self.__iface = iface

        self.__setup_parcel_detail_components()

        self.worker = UploadParcelDetailWorker(self.__iface)

    def __handle_add_crop_button_clicked(self):
        self.__add_crop_button_callback()

    def __handle_remove_crop_button_clicked(self):
        for i in self.parcel_detail_crops.selectedItems():
            self.parcel_detail_crops.invisibleRootItem().removeChild(i)

    def __handle_parcel_update(self):
        self.worker.start(self.__collect_parcel_update_request())

    def __collect_parcel_update_request(self) -> ParcelUpdateRequest:
        crops = table_widget_crops(self.parcel_detail_crops)

        mc = self.__iface.mapCanvas()
        layer = mc.currentLayer()
        g: QgsGeometry = layer.selectedFeatures()[0].geometry()

        return ParcelUpdateRequest(
            eagri_auth_data=self.eagri_user_combo_box.itemData(self.eagri_user_combo_box.currentIndex(), Qt.UserRole),
            iddpb=int(self.block_id_label.text()),
            parcels=[
                ParcelUpdateRequestParcel(
                    id=int(self.parcel_id_label.text()),
                    name=self.parcel_detail_name_edit.text(),
                    geometry=g.asWkt(),
                    area_ha=geometry_area_ha(g),
                    valid_from=crops[0].valid_from,
                    crops=[
                        to_crop_update(c)
                        for c in crops
                    ]
                )
            ]
        )

    def __handle_tab_change(self):
        self.tab_widget.currentChanged.connect(self.__reload_eagri_users)

    def __reload_eagri_users(self, index):
        if index == PARCEL_DETAIL_TAB_INDEX:
            self.eagri_user_combo_box.clear()
            [
                self.eagri_user_combo_box.addItem(combobox_name(d), d)
                for d in self.creds_tree.all_data()
            ]

    def __setup_parcel_detail_components(self):
        self.__handle_tab_change()
        self.add_crop_button.clicked.connect(self.__handle_add_crop_button_clicked)
        self.remove_crop_button.clicked.connect(self.__handle_remove_crop_button_clicked)
        self.update_parcel_button.clicked.connect(self.__handle_parcel_update)
        self.remove_crop_button.setEnabled(False)
        self.parcel_detail_name_edit.textChanged.connect(self.__handle_parcel_detail_toggle)
        self.parcel_detail_crops.itemSelectionChanged.connect(self.__enable_remove_button_if_any_crop_selected)
        self.parcel_detail_crops.setSortingEnabled(True)
        self.parcel_detail_crops.sortByColumn(2, Qt.SortOrder.AscendingOrder)

    def __handle_parcel_detail_toggle(self, text: str):
        if not text:
            self.tab_widget.setCurrentIndex(0)
            self.parcel_detail.setEnabled(False)
            self.eagri_users.activateWindow()
        else:
            self.tab_widget.setCurrentIndex(1)
            self.parcel_detail.setEnabled(True)
            self.parcel_detail.activateWindow()

    def __enable_remove_button_if_any_crop_selected(self):
        self.remove_crop_button.setEnabled(bool(self.parcel_detail_crops.selectedItems()))
