from datetime import date
from typing import List

from PyQt5 import QtGui
from qgis.core import QgsCoordinateReferenceSystem, QgsFeature, QgsSymbol, QgsVectorLayerJoinInfo, QgsVectorLayer, \
    QgsProject, QgsLayerMetadata

from .attribute_table_qgs_fields import LpisBlockListAttributeTableField, LpisBlockDetailParcelVersionAttributeTableField, \
    LpisBlockDetailParcelAttributeTableField, LpisBlockDetailParcelCropAttributeTableField
from .labeling import enable_dpb_layer_labeling
from ..common.plugin_constants import GEOMETRYLESS_VECTOR_LAYER_COLOR
from ..common.vector_layer_type import VectorLayerType
from ..common.qgis_constants import QGIS_REGISTRY, KROVAK_CRS
from ..common.geometry_type import GeometryType
from ..common.layer_provider_type import FeatureLayerProviderType
from ..common.data_provider import memory_data_provider_url
from ..error.exceptions import VectorLayerJoinException
from ..model.auth import EagriAuthData


def create_qgis_layer(
        layer_type: VectorLayerType,
        eagri_auth_data: EagriAuthData,
        validity: date,
        registry: QgsProject = QGIS_REGISTRY,
        crs: QgsCoordinateReferenceSystem = KROVAK_CRS,
        geometry_type: GeometryType = GeometryType.POLYGON,
        layer_provider: FeatureLayerProviderType = FeatureLayerProviderType.MEMORY
) -> QgsVectorLayer:
    layer = __setup_new_layer(
        crs=crs,
        geometry_type=geometry_type,
        layer_provider=layer_provider,
        layer_type=layer_type,
        auth_data=eagri_auth_data,
        validity=validity
    )
    __set_layer_attributes(layer, layer_type)
    __set_geometry_color(layer, layer_type)
    __set_dpb_layer_labeling(layer, layer_type)
    registry.addMapLayer(layer)
    return layer


def __setup_new_layer(
        crs: QgsCoordinateReferenceSystem,
        geometry_type: GeometryType,
        layer_provider: FeatureLayerProviderType,
        layer_type: VectorLayerType,
        auth_data: EagriAuthData,
        validity: date
) -> QgsVectorLayer:
    layer_title: str = __layer_title(layer_type, auth_data, validity)
    layer = QgsVectorLayer(memory_data_provider_url(geometry_type.value, crs), layer_title, layer_provider.value)
    layer.setMetadata(__layer_metadata(layer_type, auth_data))
    return layer


def __layer_title(
        layer_type: VectorLayerType,
        auth_data: EagriAuthData,
        validity: date
) -> str:
    return " / ".join([
        layer_type.layer_name(),
        auth_data.name if auth_data.name else auth_data.szrid,
        validity.strftime('%Y-%m-%d')
    ])


def __layer_metadata(
        layer_type: VectorLayerType,
        eagri_auth_data: EagriAuthData
) -> QgsLayerMetadata:
    meta = QgsLayerMetadata()
    parent_id = eagri_auth_data.szrid
    meta.setIdentifier(f"{parent_id}-{layer_type.layer_id()}")
    meta.setParentIdentifier(parent_id)
    return meta


def __set_layer_attributes(layer, layer_type):
    layer.startEditing()
    layer.dataProvider().addAttributes(__attributes_for_layer_type(layer_type))
    layer.commitChanges()


def __set_dpb_layer_labeling(
        vector_layer: QgsVectorLayer,
        layer_type: VectorLayerType
):
    if False and layer_type == VectorLayerType.DPB_LIST:
        enable_dpb_layer_labeling(vector_layer)
    else:
        pass


def __set_geometry_color(
        vector_layer: QgsVectorLayer,
        layer_type: VectorLayerType
):
    symbol = QgsSymbol.defaultSymbol(vector_layer.geometryType())
    symbol.setColor(QtGui.QColor(__color_for_layer_type(layer_type)))
    symbol.setOpacity(1)
    vector_layer.renderer().setSymbol(symbol)


# https://signatureedits.com/wp-content/uploads/2023/05/Complementary-Color-Scheme-Examples-8-min.jpg
def __color_for_layer_type(layer_type: VectorLayerType):
    if layer_type == VectorLayerType.DPB_LIST:
        return "#BE7333"
    elif layer_type == VectorLayerType.AGRICULTURAL_PARCEL_VERSION:
        return "#678CA8"
    elif layer_type == VectorLayerType.AGRICULTURAL_PARCEL_PARCEL:
        return GEOMETRYLESS_VECTOR_LAYER_COLOR
    elif layer_type == VectorLayerType.AGRICULTURAL_PARCEL_CROP:
        return GEOMETRYLESS_VECTOR_LAYER_COLOR
    else:
        raise ValueError(f"Could not determine layer color. Unsupported vector layer type: {layer_type}")


def __attributes_for_layer_type(layer_type: VectorLayerType):
    if layer_type == VectorLayerType.DPB_LIST:
        return [f.value for f in LpisBlockListAttributeTableField]
    elif layer_type == VectorLayerType.AGRICULTURAL_PARCEL_VERSION:
        return [f.value for f in LpisBlockDetailParcelVersionAttributeTableField]
    elif layer_type == VectorLayerType.AGRICULTURAL_PARCEL_PARCEL:
        return [f.value for f in LpisBlockDetailParcelAttributeTableField]
    elif layer_type == VectorLayerType.AGRICULTURAL_PARCEL_CROP:
        return [f.value for f in LpisBlockDetailParcelCropAttributeTableField]
    else:
        raise ValueError(f"Could not determine attribute fields. Unsupported vector layer type: {layer_type}")


def update_qgis_layer(layer: QgsVectorLayer, new_features: List[QgsFeature]) -> QgsVectorLayer:
    layer.dataProvider().addFeatures(new_features)
    layer.updateExtents()
    layer.reload()
    return layer


def link_vector_layers(
        joined_fields_prefix: str,
        target_layer: QgsVectorLayer,
        source_layer: QgsVectorLayer,
        target_layer_field_name,
        source_layer_field_name
):
    """
    Links two vector layers using a column present in both :target_layer and :source_layer.
    Joined columns are added to the :target_layer.
    """
    print(f"Linking {source_layer.name()} -> {target_layer.name()}")
    join_info = QgsVectorLayerJoinInfo()
    join_info.setJoinFieldName(source_layer_field_name)
    join_info.setTargetFieldName(target_layer_field_name)
    join_info.setJoinLayerId(source_layer.id())
    join_info.setJoinLayer(source_layer)
    join_info.setPrefix(joined_fields_prefix)
    join_info.setUsingMemoryCache(True)

    target_layer.startEditing()
    if not target_layer.addJoin(join_info):
        target_layer.commitChanges()
        raise VectorLayerJoinException(source_layer.name(), target_layer.name())
    else:
        target_layer.commitChanges()
        target_layer.updateExtents()
        target_layer.reload()
