from enum import Enum

from PyQt5.QtCore import QVariant
from qgis.core import QgsField

from eagris.controller.attribute_table_column_names import Lpigdp11bAttribute, Lpiddb01bParcelVersionAttribute, \
    Lpiddb01bParcelAttribute, Lpiddb01bParcelCropAttribute, Lpigpl01cParcelCropAttribute


class LpisBlockListAttributeTableField(Enum):
    IDDPB = QgsField(Lpigdp11bAttribute.IDDPB.value, QVariant.Int)
    SQUARE = QgsField(Lpigdp11bAttribute.SQUARE.value, QVariant.String)
    ZKOD = QgsField(Lpigdp11bAttribute.ZKOD.value, QVariant.String)
    STATUS = QgsField(Lpigdp11bAttribute.STATUS.value, QVariant.String)
    VALID_FROM = QgsField(Lpigdp11bAttribute.VALID_FROM.value, QVariant.Date)
    VALID_TO = QgsField(Lpigdp11bAttribute.VALID_TO.value, QVariant.Date)
    AREA = QgsField(Lpigdp11bAttribute.AREA.value, QVariant.Double)
    CULTURE = QgsField(Lpigdp11bAttribute.CULTURE.value, QVariant.String)
    CULTURE_NAME = QgsField(Lpigdp11bAttribute.CULTURE_NAME.value, QVariant.String)
    GEOMETRY = QgsField(Lpigdp11bAttribute.GEOMETRY.value, QVariant.String)


class LpisBlockDetailParcelVersionAttributeTableField(Enum):
    PARCEL_ID = QgsField(Lpiddb01bParcelVersionAttribute.PARCEL_ID.value, QVariant.Int)
    VALID_FROM = QgsField(Lpiddb01bParcelVersionAttribute.VALID_FROM.value, QVariant.Date)
    VALID_TO = QgsField(Lpiddb01bParcelVersionAttribute.VALID_TO.value, QVariant.Date)
    VERSION_ID = QgsField(Lpiddb01bParcelVersionAttribute.VERSION_ID.value, QVariant.Int)
    AREA = QgsField(Lpiddb01bParcelVersionAttribute.AREA.value, QVariant.Double)
    GEOMETRY = QgsField(Lpiddb01bParcelVersionAttribute.GEOMETRY.value, QVariant.String)


class LpisBlockDetailParcelAttributeTableField(Enum):
    PARCEL_ID = QgsField(Lpiddb01bParcelAttribute.ID.value, QVariant.Int)
    NAME = QgsField(Lpiddb01bParcelAttribute.NAME.value, QVariant.String)
    VALID_FROM = QgsField(Lpiddb01bParcelAttribute.VALID_FROM.value, QVariant.Date)
    VALID_TO = QgsField(Lpiddb01bParcelAttribute.VALID_TO.value, QVariant.Date)
    IDDPB = QgsField(Lpiddb01bParcelAttribute.IDDPB.value, QVariant.Int)


class LpisBlockDetailParcelCropAttributeTableField(Enum):
    PARCEL_ID = QgsField(Lpiddb01bParcelCropAttribute.PARCEL_ID.value, QVariant.Int)
    CROP_ID = QgsField(Lpiddb01bParcelCropAttribute.CROP_ID.value, QVariant.Int)
    CROP_NAME = QgsField(Lpigpl01cParcelCropAttribute.CROP_NAME.value, QVariant.String)
    CATCH_CROP = QgsField(Lpiddb01bParcelCropAttribute.CATCH_CROP.value, QVariant.Bool)
    VALID_FROM = QgsField(Lpiddb01bParcelCropAttribute.VALID_FROM.value, QVariant.Date)
    VALID_TO = QgsField(Lpiddb01bParcelCropAttribute.VALID_TO.value, QVariant.Date)
