from PyQt5.QtWidgets import QProgressDialog
from time import sleep


def block_download_event_consumer(queue, progress_dialog: QProgressDialog, blocks_size: int):
    progress_dialog.setValue(0)
    blocks_downloaded_so_far = 0
    while blocks_downloaded_so_far < blocks_size:
        sleep(0.05)  # keep the thread from spamming CPU if there are no blocks available
        if not queue.empty():
            queue.get()
            blocks_downloaded_so_far = progress_dialog.value() + 1
            progress_dialog.setValue(blocks_downloaded_so_far)
