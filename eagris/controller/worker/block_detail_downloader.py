from datetime import date
from multiprocessing import Pool, Manager, Queue
from threading import Thread
from typing import Tuple, List

from PyQt5.QtWidgets import QProgressDialog
from qgis.core import QgsFeature

from eagris.common.vector_layer_type import VectorLayerType
from eagris.controller.worker.block_download_event_consumer import block_download_event_consumer
from eagris.eagri.dpb_detail.service import dpb_detail
from eagris.eagri.ws.lpi_ddp01_b.model.response import DpbDetailResponse
from eagris.eagri.ws.lpi_gdp11_b.model.response import DpbListResponse, Dpb
from eagris.model.auth import EagriAuthData
from eagris.qgis.feature_mapper import parcel_version_to_feature, parcel_to_feature, parcel_crop_to_feature


class BlockDetailDownloader:
    """
    Component responsible for downloading DPB information.
    Download progress is shown to the user via QProgressDialog.
    """

    def __init__(self, progress_dialog: QProgressDialog):
        self.progress_dialog = progress_dialog

    def import_dpb_detail_data_layer(
            self,
            auth_data: EagriAuthData,
            dpb_list_response: DpbListResponse,
            validity_date: date,
            layers: List[VectorLayerType]
    ) -> List[List[QgsFeature]]:
        blocks = \
            [
                b for b in dpb_list_response.blocks
                if self.__block_is_within_validity(b, validity_date)
            ]
        blocks_size = len(blocks)

        """
        We're creating internal multiprocess pool for parallel block download.

        Each block download then emits a event which is sent to an event queue
        shared across processes. These events are then consumed separately by a
        listener thread which then writes the new progress into a QProgressDialog.

        This method returns all new layers, if requested, together with their content type
        designated by VectorLayerType.
        """
        with Pool() as p, Manager() as queue_manager:
            queue = queue_manager.Queue()
            blocks_with_auth = [(auth_data, dpb, queue) for dpb in blocks]
            block_download_event_consumer_t = Thread(
                target=block_download_event_consumer,
                args=[queue, self.progress_dialog, blocks_size]
            )
            block_download_event_consumer_t.start()

            self.progress_dialog.show()
            dpb_details = p.map(download_dpb_detail, blocks_with_auth)
            # wait 5 secs for consumer thread to finish
            block_download_event_consumer_t.join(5.0)

            parcel_version_features = self.__parcel_version_vector_layer(dpb_details, layers)
            parcel_features = self.__parcel_base_vector_layer(dpb_details, layers)
            parcel_crop_features = self.__parcel_crop_vector_layer(dpb_details, layers)
            return [parcel_version_features, parcel_features, parcel_crop_features]

    def __parcel_crop_vector_layer(self, dpb_details, layers):
        return self.__map_details_to_parcel_crops_vector_layer(dpb_details) \
            if VectorLayerType.AGRICULTURAL_PARCEL_CROP in layers else None

    def __parcel_base_vector_layer(self, dpb_details, layers):
        return self.__map_details_to_parcels_vector_layer(dpb_details) \
            if VectorLayerType.AGRICULTURAL_PARCEL_PARCEL in layers else None

    def __parcel_version_vector_layer(self, dpb_details, layers):
        return self.__map_details_to_parcel_version_vector_layer(dpb_details) \
            if VectorLayerType.AGRICULTURAL_PARCEL_VERSION in layers else None

    def __block_is_within_validity(self, b, validity_date):
        return b.valid_from <= validity_date and (not b.valid_to or b.valid_to >= validity_date)

    def __map_details_to_parcel_version_vector_layer(self, dpb_details):
        """
        Maps block details retrieved from eAGRI to parcel version features.
        These are the only features which contain geometries.
        :return: parcel version features
        """
        return [parcel_version_to_feature(parcel.id, parcel_version)
                for dpb_detail in dpb_details
                for parcel in dpb_detail.parcels
                for parcel_version in parcel.versions
                ]

    def __map_details_to_parcel_crops_vector_layer(self, dpb_details):
        return [parcel_crop_to_feature(parcel.id, parcel_crop)
                for dpb_detail in dpb_details
                for parcel in dpb_detail.parcels
                for parcel_crop in parcel.crops
                ]

    def __map_details_to_parcels_vector_layer(self, dpb_details):
        return [parcel_to_feature(parcel)
                for dpb_detail in dpb_details
                for parcel in dpb_detail.parcels
                ]


def download_dpb_detail(item: Tuple[EagriAuthData, Dpb, Queue]) -> DpbDetailResponse:
    auth_data, dpb, queue = item
    queue.put(dpb.id)
    return dpb_detail(auth_data, dpb)
