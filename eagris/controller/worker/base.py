from traceback import format_exc
from typing import Callable

from PyQt5.QtCore import QRunnable, pyqtSlot, qCritical

from eagris.controller.worker.signals import EagrisWorkerSignals
from eagris.error.exceptions import EagrisException


class EagrisWorker(QRunnable):
    """
    Worker thread
    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.
    """

    def __init__(self, fn: Callable, signals: EagrisWorkerSignals, *args, **kwargs):
        super(EagrisWorker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = signals

    @pyqtSlot()
    def run(self):
        """
        Initialise the runner function with passed args, kwargs.
        """
        try:
            self.signals.start.emit()
            r = self.fn(*self.args, **self.kwargs)
            self.signals.success.emit(r)
        except Exception as e:
            print(format_exc())
            if e is EagrisException:
                qCritical(e.message)
            else:
                print(e)
                qCritical(str(e))
            print(format_exc())
            self.signals.error.emit(e)
        finally:
            self.signals.finished.emit()
