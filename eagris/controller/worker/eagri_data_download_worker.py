from datetime import date
from typing import List, Tuple

from PyQt5.QtCore import QThreadPool, Qt
from PyQt5.QtWidgets import QProgressDialog, QMessageBox
from qgis.core import QgsFeature, QgsMessageLog, Qgis
from qgis.gui import QgisInterface

from eagris.common.download_vector_feature import DownloadVectorFeature, map_to_eagris_vector_layers
from eagris.common.plugin_constants import QGIS_TABLE_PARCEL_PREFIX
from eagris.common.vector_layer_type import VectorLayerType
from eagris.controller.attribute_table_column_names import Lpiddb01bParcelCropAttribute, Lpiddb01bParcelAttribute, \
    Lpiddb01bParcelVersionAttribute
from eagris.controller.vector_layer import create_qgis_layer, update_qgis_layer, link_vector_layers
from eagris.controller.worker.base import EagrisWorker
from eagris.controller.worker.block_detail_downloader import BlockDetailDownloader
from eagris.controller.worker.signals import EagrisWorkerSignals
from eagris.eagri.dpb_list.service import dpb_list
from eagris.eagri.ws.lpi_gdp11_b.model.response import DpbListResponse
from eagris.model.auth import EagriAuthData
from eagris.qgis.feature_mapper import dpb_to_feature


class EagriDataDownloadWorker:
    def __init__(self, iface: QgisInterface):
        self.__iface = iface
        self.__threadpool = QThreadPool.globalInstance()
        self.__layer_import_signals = EagrisWorkerSignals()
        self.__layer_import_signals.start.connect(self.__on_start)
        self.__layer_import_signals.finished.connect(self.__on_finished)
        self.__layer_import_signals.success.connect(self.__on_success)
        self.__layer_import_signals.error.connect(self.__on_error)

    def start(
            self,
            auth_data: EagriAuthData,
            validity_date: date,
            selected_features: List[DownloadVectorFeature]
    ):
        worker = EagrisWorker(
            self.__import_vector_layers,
            self.__layer_import_signals,
            auth_data,
            validity_date,
            selected_features
        )
        self.__threadpool.start(worker)

    def __import_vector_layers(
            self,
            auth_data: EagriAuthData,
            validity_date: date,
            selected_features: List[DownloadVectorFeature]
    ) -> Tuple[EagriAuthData, date, List[List[QgsFeature]]]:
        layers = map_to_eagris_vector_layers(selected_features)
        blocks = self.__dpb_list_with_progress_dialog(auth_data, validity_date)
        dpb_features = import_dpb_data_layer(blocks) if VectorLayerType.DPB_LIST in layers else None
        parcel_version_features, parcel_features, parcel_crop_features = (
            BlockDetailDownloader(self.__create_dpb_detail_progress_dialog(blocks))
            .import_dpb_detail_data_layer(
                auth_data=auth_data,
                dpb_list_response=blocks,
                validity_date=validity_date,
                layers=layers
            )
        )
        return (
            auth_data,
            validity_date,
            # note that some of the features might be None
            [dpb_features, parcel_version_features, parcel_features, parcel_crop_features]
        )

    def __dpb_list_with_progress_dialog(
            self,
            auth_data: EagriAuthData,
            validity_date: date
    ):
        pd = self.__create_dpb_list_message()
        pd.show()
        blocks = dpb_list(eagri_auth_data=auth_data, validity_date=validity_date)
        pd.hide()
        return blocks

    def __create_dpb_detail_progress_dialog(self, blocks_response: DpbListResponse):
        progress_dialog = QProgressDialog()
        progress_dialog.setMinimum(0)
        progress_dialog.setMaximum(len(blocks_response.blocks))
        progress_dialog.setWindowTitle("DPB details")
        progress_dialog.setLabelText("Downloading DPB details...")
        progress_dialog.setCancelButton(None)
        progress_dialog.setWindowModality(Qt.WindowModal)
        return progress_dialog

    def __create_dpb_list_message(self):
        msg_box = QMessageBox()
        msg_box.setWindowTitle("DPB list")
        msg_box.setText("Downloading DPB blocks, this might take a moment...")
        msg_box.setStandardButtons(QMessageBox.NoButton)
        msg_box.setWindowModality(Qt.WindowModal)
        return msg_box

    def __on_start(self):
        QgsMessageLog.logMessage("Importing eAGRI vector layers...", level=Qgis.MessageLevel.Info)
        self.__iface.statusBarIface().showMessage("Importing eAGRI vector layers...")

    def __on_error(self, _exception: Exception):
        QgsMessageLog.logMessage("Import did not finish successfully.", level=Qgis.MessageLevel.Critical)

    def __on_success(self, result: Tuple[EagriAuthData, date, List[List[QgsFeature]]]):
        auth_data, validity_date, [dpb_features, parcel_version_features, parcel_features, parcel_crop_features] = result

        self.__create_layer(
            auth_data, VectorLayerType.DPB_LIST, dpb_features, validity_date
        )
        parcel_layer = self.__create_layer(
            auth_data, VectorLayerType.AGRICULTURAL_PARCEL_PARCEL, parcel_features, validity_date
        )
        parcel_crop_layer = self.__create_layer(
            auth_data, VectorLayerType.AGRICULTURAL_PARCEL_CROP, parcel_crop_features, validity_date
        )
        parcel_version_layer = self.__create_layer(
            auth_data, VectorLayerType.AGRICULTURAL_PARCEL_VERSION, parcel_version_features, validity_date
        )

        if parcel_version_layer and parcel_layer:
            link_vector_layers(
                joined_fields_prefix=QGIS_TABLE_PARCEL_PREFIX,
                target_layer=parcel_version_layer,
                source_layer=parcel_layer,
                target_layer_field_name=Lpiddb01bParcelVersionAttribute.PARCEL_ID.value,
                source_layer_field_name=Lpiddb01bParcelAttribute.ID.value
            )
        if parcel_version_layer and parcel_crop_layer:
            link_vector_layers(
                joined_fields_prefix=QGIS_TABLE_PARCEL_PREFIX,
                target_layer=parcel_crop_layer,
                source_layer=parcel_layer,
                target_layer_field_name=Lpiddb01bParcelCropAttribute.PARCEL_ID.value,
                source_layer_field_name=Lpiddb01bParcelAttribute.ID.value
            )

    def __create_layer(
            self,
            auth_data: EagriAuthData,
            layer_type: VectorLayerType,
            features: List[QgsFeature],
            validity: date
    ):
        if features:
            print(f"Creating vector layer of type {layer_type.name} for user {auth_data.identifier()}...")
            layer = create_qgis_layer(layer_type=layer_type, eagri_auth_data=auth_data, validity=validity)
            return update_qgis_layer(
                layer,
                features
            )
        else:
            print(f"Skipping creation of vector layer of tpye {layer_type.name} for "
                  f"user {auth_data.identifier()} since it was not requested.")
            return None

    def __on_finished(self):
        self.__iface.statusBarIface().clearMessage()

        QgsMessageLog.logMessage("eAGRI layer import finished.", level=Qgis.MessageLevel.Info)


def import_dpb_data_layer(dpb_list_response: DpbListResponse):
    blocks = dpb_list_response.blocks
    return [dpb_to_feature(dpb) for dpb in blocks]
