from traceback import format_exc

from PyQt5.QtCore import qCritical, QThreadPool
from qgis.gui import QgisInterface
from qgis.core import QgsMessageLog, Qgis

from eagris.controller.worker.base import EagrisWorker
from eagris.controller.worker.signals import EagrisWorkerSignals
from eagris.eagri.parcel.management.service import update_parcel
from eagris.eagri.ws.lpi_zzp01_b.model.request import ParcelUpdateRequest


class UploadParcelDetailWorker:
    def __init__(
            self,
            iface: QgisInterface,

    ):
        self.__iface = iface
        self.__layer_import_signals = EagrisWorkerSignals()
        self.__layer_import_signals.start.connect(self.__on_start)
        self.__layer_import_signals.finished.connect(self.__on_finished)
        self.__layer_import_signals.error.connect(self.__on_error)
        self.__threadpool = QThreadPool.globalInstance()

    def start(self, parcel_update_request: ParcelUpdateRequest):
        worker = EagrisWorker(self.__update_parcel_at_eagri, self.__layer_import_signals, parcel_update_request)
        self.__threadpool.start(worker)

    def __update_parcel_at_eagri(self, parcel_update_request: ParcelUpdateRequest):
        update_parcel(parcel_update_request)

    def __on_start(self):
        QgsMessageLog.logMessage("Updating parcel...", level=Qgis.MessageLevel.Info)
        self.__iface.statusBarIface().showMessage("Updating parcel...")

    def __on_finished(self):
        self.__iface.statusBarIface().clearMessage()

    def __on_error(self, exception):
        print(exception)
        print(format_exc())
        qCritical(str(exception))
        qCritical(format_exc())
        QgsMessageLog.logMessage(f"Unknown Eagris error: {format_exc()}", level=Qgis.MessageLevel.Critical)
