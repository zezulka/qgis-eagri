from PyQt5.QtCore import QObject, pyqtSignal


class EagrisWorkerSignals(QObject):
    """
    Defines the signals available from a running worker thread.
    Supported signals are:
    - finished: No data
    - start: emitted before worker starts its job
    - error:`tuple` (exception)
    """
    finished = pyqtSignal()
    start = pyqtSignal()
    error = pyqtSignal(object)
    success = pyqtSignal(object)
