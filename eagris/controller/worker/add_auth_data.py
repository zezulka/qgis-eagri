from traceback import format_exc
from typing import Callable

from PyQt5.QtCore import QThreadPool, qCritical
from qgis.core import QgsMessageLog, Qgis
from qgis.gui import QgisInterface

from eagris.controller.panel.credtree import EagrisCredentialsTree
from eagris.controller.worker.base import EagrisWorker
from eagris.controller.worker.signals import EagrisWorkerSignals
from eagris.eagri.auth_probe.service import auth_data_probe
from eagris.model.auth import EagriAuthData


class AddAuthDataWorker:
    def __init__(
            self,
            iface: QgisInterface,
            creds_tree: EagrisCredentialsTree,
            success_callback: Callable[[EagriAuthData], None],
            finish_callback: Callable[[], None]
    ):
        self.__iface = iface
        self.__credsTree = creds_tree
        self.__successCallback = success_callback
        self.__finishCallback = finish_callback
        self.__threadpool = QThreadPool.globalInstance()
        self.__layer_import_signals = EagrisWorkerSignals()
        self.__layer_import_signals.start.connect(self.__on_start)
        self.__layer_import_signals.finished.connect(self.__on_finished)
        self.__layer_import_signals.error.connect(self.__on_error)

    def start(self, auth_data: EagriAuthData):
        worker = EagrisWorker(self.__verify_and_add_auth_data, self.__layer_import_signals, auth_data)
        self.__threadpool.start(worker)

    def __verify_and_add_auth_data(self, auth_data):
        """
        Logic to be executed inside the worker.
        """
        if self.__credsTree.auth_item_exists(auth_data):
            QgsMessageLog.logMessage(
                message="Authentication data are already present. Please check the form data and try again.",
                level=Qgis.MessageLevel.Warning
            )
        elif not auth_data_probe(auth_data):
            QgsMessageLog.logMessage(
                message="Authentication data are invalid. Please try again.",
                level=Qgis.MessageLevel.Warning
            )
        else:
            print(f"Adding auth data {auth_data}.")
            self.__successCallback(auth_data)

    def __on_start(self):
        QgsMessageLog.logMessage("Verifying authentication data...", level=Qgis.MessageLevel.Info)
        self.__iface.statusBarIface().showMessage("Verifying authentication data...")

    def __on_finished(self):
        self.__iface.statusBarIface().clearMessage()
        self.__finishCallback()

    def __on_error(self, exception):
        print(exception)
        print(format_exc())
        qCritical(str(exception))
        qCritical(format_exc())
        QgsMessageLog.logMessage(f"Unknown Eagris error: {format_exc()}", level=Qgis.MessageLevel.Critical)
