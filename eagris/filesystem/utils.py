import os
from pathlib import Path

from eagris.common.plugin_constants import DEFAULT_AUTH_FILE_NAME


def default_auth_file_path() -> str:
    return str(Path.home().joinpath(DEFAULT_AUTH_FILE_NAME))


def path_with_default_file_name(dir_path: str) -> Path:
    return Path(dir_path).joinpath(DEFAULT_AUTH_FILE_NAME)


def make_dirs(dir_path: Path):
    print(f"Making sure that directory {dir_path} exists...")
    os.makedirs(dir_path, exist_ok=True)


def make_file(file_path):
    with open(file_path, 'w+') as f:
        f.write("")


def path_exists(path: str):
    return path.startswith('file:memory') or os.path.exists(path)
