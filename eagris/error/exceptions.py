from textwrap import dedent

from eagris.eagri.auth.ws_key_hash_method import WsKeyHashMethod
from eagris.eagri.ws.lpi_gdp11_b.model.response import EagriErrorResponse


class EagrisException(Exception):
    def __init__(self, message: str = "Unknown Eagris plugin error."):
        super().__init__(message)
        self.message = message


class VectorLayerJoinException(EagrisException):
    def __init__(self, source, target):
        super().__init__(f"Vector layers could not be joined: {source}, {target}")


class XmlSubnodeNotFoundException(EagrisException):
    def __init__(self, subnode_name: str):
        super().__init__(f"Could not find subnode with name {subnode_name}")


class EagriRequestException(EagrisException):
    def __init__(self, status_code: int, eagri_error: EagriErrorResponse):
        super().__init__(dedent(
            f"""
            eAGRI data could not be downloaded.
            HTTP status code: {status_code}
            eAGRI error code: {eagri_error.code}
            eAGRI error message: {eagri_error.message}
            """
        ))


class InvalidWsKeyFormatException(EagrisException):
    def __init__(self, ws_key: str, hash_method: WsKeyHashMethod):
        super().__init__(dedent(
            f"""
            WS key '{ws_key}' provided in an erroneous format for hash method {hash_method}.
            """
        ))
