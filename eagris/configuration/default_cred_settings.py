from eagris.auth.auth_settings import AuthCredSettings

"""
Settings for an in-memory SQLite database.
"""
DEFAULT_AUTH_SETTINGS = AuthCredSettings(
    # create an in-memory database
    authCredsPath="file:memory?mode=memory&cache=shared",
    tableName="lpis_auth_data",
    loginField="login",
    szridField="szrid",
    wsKeyField="wskey",
    nameField="name",
    envField="env"
)
