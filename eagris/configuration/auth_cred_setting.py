from enum import Enum


class AuthCredSetting(Enum):
    AUTH_CREDS_PATH = "eagris/auth_creds_path"
    TABLE_NAME = "eagris/table_name"
    LOGIN_FIELD_NAME = "eagris/login_field"
    SZRID_FIELD_NAME = "eagris/szrid_field"
    WSKEY_FIELD_NAME = "eagris/wskey_field"


class OptionalAuthCredSetting(Enum):
    NAME_FIELD_NAME = "eagris/name_field"
    ENV_FIELD_NAME = "eagris/env_field"
