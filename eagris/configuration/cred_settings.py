import sqlite3
import traceback
from typing import List

from eagris.auth.auth_settings import AuthCredSettings
from eagris.auth.sqlite.utils import create_session, execute_sqlite_statement
from eagris.common.plugin_constants import PLUGIN_ENABLED_QGIS_SETTING
from eagris.common.qgis_constants import QGIS_SETTINGS
from eagris.configuration.auth_cred_setting import AuthCredSetting, OptionalAuthCredSetting
from eagris.configuration.default_cred_settings import DEFAULT_AUTH_SETTINGS
from eagris.error.exceptions import EagrisException
from eagris.filesystem.utils import path_exists


class QgisEagriConfigurationException(EagrisException):
    def __init__(self, message):
        super().__init__(message)


def load_settings():
    qgis_keys = __get_qgis_settings()
    __check_config_data_sanity(qgis_keys)
    qgis_variables = {key: QGIS_SETTINGS.value(key) for key in qgis_keys}

    auth_cred_settings = AuthCredSettings(
        qgis_variables[AuthCredSetting.AUTH_CREDS_PATH.value],
        qgis_variables[AuthCredSetting.TABLE_NAME.value],
        qgis_variables[AuthCredSetting.LOGIN_FIELD_NAME.value],
        qgis_variables[AuthCredSetting.SZRID_FIELD_NAME.value],
        qgis_variables[AuthCredSetting.WSKEY_FIELD_NAME.value],
        qgis_variables.get(OptionalAuthCredSetting.NAME_FIELD_NAME.value, None),
        qgis_variables.get(OptionalAuthCredSetting.ENV_FIELD_NAME.value, None)
    )
    # TODO deletegate logging to multiple streams
    print(f"Successfully loaded Eagris configuration, got {auth_cred_settings}")
    return auth_cred_settings


def setup_local_creds(auth_creds_path: str = DEFAULT_AUTH_SETTINGS.authCredsPath) -> bool:
    print(f"setting up local credentials using file {auth_creds_path}")
    QGIS_SETTINGS.setValue(PLUGIN_ENABLED_QGIS_SETTING, True)
    __setup_mandatory_fields(auth_creds_path)
    __setup_optional_fields()

    try:
        session = create_session(auth_creds_path)
        execute_sqlite_statement(
            f"""
                CREATE TABLE IF NOT EXISTS {DEFAULT_AUTH_SETTINGS.tableName}(
                    {DEFAULT_AUTH_SETTINGS.loginField} text NOT NULL,
                    {DEFAULT_AUTH_SETTINGS.szridField} text NOT NULL,
                    {DEFAULT_AUTH_SETTINGS.wsKeyField} text NOT NULL,
                    {DEFAULT_AUTH_SETTINGS.nameField} text NULL,
                    {DEFAULT_AUTH_SETTINGS.envField} text NULL
                );
                """,
            session
        )
        return True
    except sqlite3.Error:
        __erase_config(__get_qgis_settings())
        print(traceback.format_exc())
        return False


def __setup_mandatory_fields(auth_creds_path):
    QGIS_SETTINGS.setValue(AuthCredSetting.AUTH_CREDS_PATH.value, auth_creds_path)
    QGIS_SETTINGS.setValue(AuthCredSetting.TABLE_NAME.value, DEFAULT_AUTH_SETTINGS.tableName)
    QGIS_SETTINGS.setValue(AuthCredSetting.WSKEY_FIELD_NAME.value, DEFAULT_AUTH_SETTINGS.wsKeyField)
    QGIS_SETTINGS.setValue(AuthCredSetting.SZRID_FIELD_NAME.value, DEFAULT_AUTH_SETTINGS.szridField)
    QGIS_SETTINGS.setValue(AuthCredSetting.LOGIN_FIELD_NAME.value, DEFAULT_AUTH_SETTINGS.loginField)


def __setup_optional_fields():
    QGIS_SETTINGS.setValue(OptionalAuthCredSetting.NAME_FIELD_NAME.value, DEFAULT_AUTH_SETTINGS.nameField)
    QGIS_SETTINGS.setValue(OptionalAuthCredSetting.ENV_FIELD_NAME.value, DEFAULT_AUTH_SETTINGS.envField)


def __check_config_exists(setting: AuthCredSetting, qgis_keys: List[str]):
    if setting.value not in QGIS_SETTINGS.allKeys():
        __erase_config(qgis_keys)
        raise QgisEagriConfigurationException(f"Setting {setting.value} does not exist in user config. The remaining "
                                              f"config (if there was any) was erased.")


def __get_qgis_settings():
    return [prop for prop in QGIS_SETTINGS.allKeys() if prop.startswith(f"{PLUGIN_ENABLED_QGIS_SETTING}/")]


def __erase_config(qgis_keys: List[str]):
    [QGIS_SETTINGS.remove(key) for key in qgis_keys]
    __disable_eagris()


def __disable_eagris():
    QGIS_SETTINGS.remove(PLUGIN_ENABLED_QGIS_SETTING)


def __check_config_data_sanity(qgis_keys: List[str]):
    [__check_config_exists(e, qgis_keys) for e in AuthCredSetting]
    __check_auth_file_exists(qgis_keys)


def __check_auth_file_exists(qgis_keys: List[str]):
    auth_path = QGIS_SETTINGS.value(AuthCredSetting.AUTH_CREDS_PATH.value)
    if not path_exists(auth_path):
        __erase_config(qgis_keys)
        raise QgisEagriConfigurationException(f"Auth file does not exist on path {auth_path}. "
                                              f"The config (if there was any) was erased.")


def is_eagris_plugin_set_up() -> bool:
    return QGIS_SETTINGS.value(PLUGIN_ENABLED_QGIS_SETTING, defaultValue=False, type=bool)
