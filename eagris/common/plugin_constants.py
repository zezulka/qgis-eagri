from ..eagri.env import EagriEnv

DEFAULT_ENV = EagriEnv.PROD
DPB_LIST_MAX_WEEKS = 4 * 6 - 1
DEFAULT_AUTH_FILE_NAME = "credentials.db"
ALLOWED_AUTH_FILE_SUFFIXES = (".sqlite", ".sqlite3", ".db", ".db3", ".s3db", ".sl3")
DOCUMENTATION_URL = "https://zezulka.gitlab.io/qgis-eagri/"
DEFAULT_NS = "ns2"
GEOMETRYLESS_VECTOR_LAYER_COLOR = "#000000"
QGIS_TABLE_PARCEL_PREFIX = "PARCELA_"
PLUGIN_ENABLED_QGIS_SETTING = "eagris"
PARCEL_DETAIL_TAB_INDEX = 1
# requirement by eagri
PARCEL_AREA_MAX_DIGITS = 2
