"""
This module is a bridge between XML parsing implementor and Eagris.
"""
import unicodedata
from typing import List, Optional

from lxml import etree
from lxml.etree import Element

from eagris.eagri.namespaces import SOAP_NS
from eagris.error.exceptions import XmlSubnodeNotFoundException

XmlNode = Element


def xml_bool(b: bool) -> str:
    return "1" if b else "0"


def canonicalize_and_serialize_xml(element: XmlNode) -> bytes:
    return etree.tostring(element, method='c14n')


def serialize_xml(element: XmlNode) -> bytes:
    return etree.tostring(element)


def parse_xml_string(xml_string) -> XmlNode:
    """
    The input can be either a byte array or string. Type hint Union[str | bytes] is not included
    because of python 3.8 compatibility.
    """
    return etree.fromstring(xml_string)


# The intended effect of the first bracket mess is to inject a NS variable
#    - that's what the inner '{}' does.
# To refer to a namespace, we need to use another '{}' pair, this time literal.
# To escape the outer '{}', we use the last pair of '{}'.
def find_xml_subnode(root: XmlNode, subnode_name: str, strict: bool = True, namespace: str = SOAP_NS) -> XmlNode:
    subnode = root.find(f'.//{{{namespace}}}{subnode_name}')
    if subnode is None and strict:
        raise XmlSubnodeNotFoundException(subnode_name)
    else:
        return subnode


def find_xml_subnodes(root: XmlNode, subnode_name: str, namespace: str = SOAP_NS) -> List[XmlNode]:
    return root.findall(f'.//{{{namespace}}}{subnode_name}')


def find_xml_text(root: XmlNode, subnode_name: str, namespace: str, strict: bool = True) -> Optional[str]:
    subnode = find_xml_subnode(root, subnode_name, strict, namespace)
    if subnode is not None:
        return unicodedata.normalize(
            'NFKD',
            subnode.text.strip()
        ).encode('ascii', 'ignore').decode('ascii')
    else:
        return None
