from dataclasses import dataclass

from qgis.core import QgsVectorLayer

from eagris.common.vector_layer_type import VectorLayerType


@dataclass
class EagrisVectorLayer:
    qgis_vector_layer: QgsVectorLayer
    layer_type: VectorLayerType
