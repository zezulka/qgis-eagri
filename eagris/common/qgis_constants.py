from qgis.core import QgsProject, QgsSettings, QgsCoordinateReferenceSystem

QGIS_REGISTRY = QgsProject.instance()
QGIS_SETTINGS = QgsSettings()
KROVAK_SRID = 5514
KROVAK_CRS = QgsCoordinateReferenceSystem.fromEpsgId(KROVAK_SRID)
