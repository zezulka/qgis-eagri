from enum import Enum

PARCEL_VECTOR_LAYER_PREFIX = "ZP"


class VectorLayerType(Enum):
    DPB_LIST = ["DPB"]
    AGRICULTURAL_PARCEL_VERSION = [PARCEL_VECTOR_LAYER_PREFIX, "VERSION"]
    AGRICULTURAL_PARCEL_PARCEL = [PARCEL_VECTOR_LAYER_PREFIX, "PARCEL"]
    AGRICULTURAL_PARCEL_CROP = [PARCEL_VECTOR_LAYER_PREFIX, "CROP"]

    def layer_name(self):
        return " / ".join(["eAGRI"] + self.value)

    def layer_id(self):
        return "-".join(self.value)
