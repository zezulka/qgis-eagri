import functools

from enum import Enum
from typing import List

from eagris.common.vector_layer_type import VectorLayerType


class DownloadVectorFeature(Enum):
    DPB = 'DPB'
    AGRICULTURAL_PARCEL_VERSION = 'Parcel Geometry'
    """
    Parcel **geometry** is a crude oversimplification of data stored in this layer
    but it is the most apt naming for a newcoming user.
    """
    AGRICULTURAL_PARCEL_CROP = 'Crop'


def map_to_eagris_vector_layers(features: List[DownloadVectorFeature]):
    initial_layers = set()
    return functools.reduce(
        lambda acc, feature: set(acc.union(__map_to_eagris_vector_layer(feature))),
        features,
        initial_layers
    )


def __map_to_eagris_vector_layer(feature: DownloadVectorFeature):
    if feature == DownloadVectorFeature.DPB:
        return [VectorLayerType.DPB_LIST]
    elif feature == DownloadVectorFeature.AGRICULTURAL_PARCEL_VERSION:
        return [VectorLayerType.AGRICULTURAL_PARCEL_PARCEL, VectorLayerType.AGRICULTURAL_PARCEL_VERSION]
    elif feature == DownloadVectorFeature.AGRICULTURAL_PARCEL_CROP:
        return [VectorLayerType.AGRICULTURAL_PARCEL_PARCEL, VectorLayerType.AGRICULTURAL_PARCEL_CROP]
