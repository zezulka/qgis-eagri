from qgis.core import QgsCoordinateReferenceSystem


def memory_data_provider_url(geometry_type: str, crs: QgsCoordinateReferenceSystem):
    return "%s?crs=%s" % (geometry_type, crs.authid())
