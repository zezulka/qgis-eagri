from enum import Enum


class FeatureLayerProviderType(Enum):
    MEMORY = 'memory'
