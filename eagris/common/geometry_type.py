from enum import Enum


class GeometryType(Enum):
    NONE = 'none'
    POINT = 'point'
    LINESTRING = 'linestring'
    POLYGON = 'polygon'
    MULTIPOINT = 'multipoint'
    MULTILINESTRING = 'multilinestring'
    MULTIPOLYGON = 'multipolygon'
