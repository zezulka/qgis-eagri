from datetime import date
from typing import Optional

from PyQt5.QtCore import QDate, Qt
from PyQt5.QtWidgets import QTreeWidgetItem
from qgis.core import QgsGeometry, QgsFields, QgsFeature

from eagris.common.qt_constants import QDATE_FORMAT
from eagris.controller.attribute_table_column_names import Lpiddb01bParcelCropAttribute
from eagris.eagri.crop_list.service import find_crop_by_id
from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelVersion, Parcel, ParcelCrop
from eagris.eagri.ws.lpi_gdp11_b.model.response import Dpb


def date_to_q_date(d: date):
    return QDate(d.year, d.month, d.day) if d else None


def dpb_to_feature(dpb: Dpb):
    geom = QgsGeometry.fromWkt(dpb.geometry)

    f = QgsFeature()
    f.setGeometry(geom)

    f.setAttributes([
        dpb.id,
        dpb.square,
        dpb.zkod,
        dpb.status,
        date_to_q_date(dpb.valid_from),
        date_to_q_date(dpb.valid_to),
        dpb.area,
        dpb.culture,
        dpb.culture_name,
        dpb.geometry
    ])
    return f


def parcel_version_to_feature(parcel_id: int, parcel_version: ParcelVersion) -> QgsFeature:
    geom = QgsGeometry.fromWkt(parcel_version.geometry)

    f = QgsFeature()
    f.setGeometry(geom)

    f.setAttributes([
        parcel_id,
        date_to_q_date(parcel_version.valid_from),
        date_to_q_date(parcel_version.valid_to),
        parcel_version.id,
        parcel_version.area,
        parcel_version.geometry
    ])
    return f


def parcel_to_feature(parcel: Parcel) -> QgsFeature:
    f = QgsFeature()
    f.setAttributes([
        parcel.id,
        parcel.name,
        date_to_q_date(parcel.valid_from),
        date_to_q_date(parcel.valid_to),
        parcel.iddpb
    ])

    return f


def parcel_crop_to_feature(
        parcel_id: int,
        parcel_crop: ParcelCrop,
        fields: QgsFields = None  # used by tests for features without layers
):
    f = QgsFeature()
    if fields:
        f.setFields(fields)
    f.setAttributes([
        parcel_id,
        parcel_crop.crop_id,
        parcel_crop.crop_name,
        parcel_crop.catch_crop,
        date_to_q_date(parcel_crop.valid_from),
        date_to_q_date(parcel_crop.valid_to)
    ])

    return f


def crop_feature_to_tree_item(crop: QgsFeature) -> QTreeWidgetItem:
    attrs = crop.attributes()
    item = QTreeWidgetItem()

    valid_to_attribute = attrs[crop.fieldNameIndex(Lpiddb01bParcelCropAttribute.VALID_TO.value)]
    valid_from = __valid_from(attrs, crop)
    valid_to = __valid_to(valid_to_attribute)
    crop_id = attrs[crop.fieldNameIndex(Lpiddb01bParcelCropAttribute.CROP_ID.value)]
    eagri_crop = find_crop_by_id(crop_id)
    crop_name = eagri_crop.name
    item.setText(0, str(crop_id))
    item.setText(1, crop_name)
    item.setText(2, valid_from)
    item.setText(3, valid_to)
    # TODO do not hardwire catch_crop = False
    item.setData(
        0,
        Qt.UserRole,
        ParcelCrop(
            crop_id=eagri_crop.id,
            crop_name=eagri_crop.name,
            catch_crop=False,
            valid_from=valid_from,
            valid_to=valid_to
        )
    )
    return item


def eagri_crop_to_tree_item(
        crop_id: int,
        valid_from: date,
        valid_to: Optional[date],
) -> QTreeWidgetItem:
    item = QTreeWidgetItem()
    eagri_crop = find_crop_by_id(crop_id)

    item.setText(0, str(crop_id))
    item.setText(1, find_crop_by_id(crop_id).name)
    item.setText(2, str(valid_from))
    item.setText(3, str(valid_to))
    # TODO do not hardwire catch_crop = False
    item.setData(
        0,
        Qt.UserRole,
        ParcelCrop(
            crop_id=eagri_crop.id,
            crop_name=eagri_crop.name,
            catch_crop=False,
            valid_from=valid_from,
            valid_to=valid_to
        )
    )

    return item


def __valid_to(valid_to_attribute):
    return valid_to_attribute.toString(format=QDATE_FORMAT) if valid_to_attribute else None


def __valid_from(attrs, crop):
    return attrs[crop.fieldNameIndex(Lpiddb01bParcelCropAttribute.VALID_FROM.value)].toString(format=QDATE_FORMAT)
