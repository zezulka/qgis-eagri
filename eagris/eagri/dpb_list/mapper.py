from datetime import date

from eagris.eagri.ws.generated.lpigdp11b import ResponseType, DpbType
from eagris.eagri.ws.lpi_gdp11_b.model.response import Dpb, DpbListResponse


def map_block(dpb_element: DpbType):
    return Dpb(
        id=dpb_element.iddpb,
        square=dpb_element.ctverec,
        zkod=dpb_element.zkod,
        status=dpb_element.stav,
        valid_from=dpb_element.platnostod.to_date(),
        area=float(dpb_element.vymera),
        culture=dpb_element.kultura,
        culture_name=dpb_element.kulturanazev,
        geometry=dpb_element.geometrie.strip(),
        valid_to=dpb_element.platnostdo.to_date() if dpb_element.platnostdo else None
    )


def map_response(raw_response: ResponseType, validity_date: date):
    return DpbListResponse(
        raw_response.datzmenydpb.to_date() if raw_response.datzmenydpb else None,
        [
            b
            for b in [map_block(dpb) for dpb in raw_response.dpb]
            # filter out DPBs which are == validity_date - '1 day'
            if b.valid_from <= validity_date and (not b.valid_to or b.valid_to >= validity_date)
        ]
    )
