from datetime import date, timedelta

from eagris.eagri.ws.lpi_gdp11_b.model.request import DpbListRequest
from eagris.model.auth import EagriAuthData
from eagris.eagri.ws.lpi_gdp11_b.client import request
from eagris.eagri.dpb_list.mapper import map_response


def dpb_list(eagri_auth_data: EagriAuthData, validity_date: date):
    r = request(DpbListRequest(eagri_auth_data, validity_date - timedelta(days=1), validity_date))
    return map_response(raw_response=r, validity_date=validity_date)
