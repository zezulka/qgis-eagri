from eagris.eagri.crop_list.service import find_crop_by_id
from eagris.eagri.ws.generated.lpiddp01b import ResponseType, ZemParcelyType, ParcelaVerzeType, PlodinaType
from eagris.eagri.ws.lpi_ddp01_b.model.response import DpbDetailResponse, Parcel, ParcelVersion, ParcelCrop


def map_response(raw_response: ResponseType, active_parcels_only: bool):
    """
    Mapper expects that only one DPB is returned since we request DPB detail using its ID.
    """
    only_dpb = raw_response.dpb[0]
    return DpbDetailResponse(
        [__map_parcel(only_dpb.iddpb, parcel, active_parcels_only) for parcel in only_dpb.zemparcely]
    )


def __map_parcel(iddpb: int, parcel: ZemParcelyType, active_parcels_only: bool):
    return Parcel(
        id=parcel.idparcela,
        name=parcel.nazev,
        valid_from=parcel.parcelaod.to_date(),
        valid_to=parcel.parcelado.to_date() if parcel.parcelado else None,
        crops=[__map_parcel_crop(crop) for crop in parcel.plodina],
        versions=[
            __map_parcel_version(version) for version in parcel.parcelaverze
            if (not active_parcels_only) or (not version.parcelaverzedo)
        ],
        iddpb=iddpb
    )


def __map_parcel_crop(crop: PlodinaType):
    return ParcelCrop(
        crop_id=crop.kodplodiny,
        crop_name=find_crop_by_id(crop.kodplodiny).name,
        valid_from=crop.platnostod.to_date(),
        valid_to=crop.platnostdo.to_date() if crop.platnostdo else None,
        catch_crop=crop.meziplodina
    )


def __map_parcel_version(parcel_version: ParcelaVerzeType):
    return ParcelVersion(
        id=parcel_version.idparcelaverze,
        geometry=parcel_version.geometrie,
        valid_from=parcel_version.parcelaverzeod.to_date(),
        valid_to=parcel_version.parcelaverzedo.to_date() if parcel_version.parcelaverzedo else None,
        area=float(parcel_version.vymera)
    )
