from eagris.eagri.dpb_detail.mapper import map_response
from eagris.eagri.ws.generated.lpiddp01b import TypDataKodType
from eagris.eagri.ws.lpi_ddp01_b.client import request
from eagris.eagri.ws.lpi_ddp01_b.model.request import DpbDetailRequest
from eagris.eagri.ws.lpi_gdp11_b.model.response import Dpb
from eagris.model.auth import EagriAuthData


def dpb_detail(eagri_auth_data: EagriAuthData, dpb: Dpb, active_parcels_only: bool = True):
    r = request(DpbDetailRequest(
        eagri_auth_data=eagri_auth_data,
        dpb=dpb,
        data_types=[TypDataKodType.ZAKLAD, TypDataKodType.ZEMPARCELY])
    )
    return map_response(r, active_parcels_only)
