from eagris.eagri.parcel.management.mapper import map_creation_response, map_update_response
from eagris.eagri.ws.lpi_zzp01_b.client import create_request, update_request
from eagris.eagri.ws.lpi_zzp01_b.model.request import ParcelCreationRequest, ParcelUpdateRequest
from eagris.eagri.ws.lpi_zzp01_b.model.response import ParcelsUpdateResponse, ParcelsCreationResponse


def create_parcel(request: ParcelCreationRequest) -> ParcelsCreationResponse:
    r = create_request(request)
    return map_creation_response(r)


def update_parcel(request: ParcelUpdateRequest) -> ParcelsUpdateResponse:
    r = update_request(request)
    return map_update_response(r)
