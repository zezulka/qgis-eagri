from eagris.eagri.ws.generated.lpizzp01b import ResponseType
from eagris.eagri.ws.lpi_zzp01_b.model.response import ParcelsCreationResponse, ParcelsUpdateResponse, \
    ParcelCreationResponse, ParcelErrorCode, ParcelUpdateResponse


def map_creation_response(response: ResponseType) -> ParcelsCreationResponse:
    return ParcelsCreationResponse(
        [ParcelCreationResponse(
            success=p.status == 1,
            error_code=ParcelErrorCode[p.chyba] if p.chyba else None,
            id=p.idparcela
        ) for p in response.parcela]
    )


def map_update_response(response: ResponseType) -> ParcelsUpdateResponse:
    return ParcelsUpdateResponse(
        [ParcelUpdateResponse(
            success=p.status == 1,
            error_code=ParcelErrorCode[p.chyba] if p.chyba else None,
            id=p.idparcela
        ) for p in response.parcela]
    )
