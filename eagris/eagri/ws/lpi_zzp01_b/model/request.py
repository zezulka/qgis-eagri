from dataclasses import dataclass
from datetime import date
from typing import List, Optional

from eagris.common.plugin_constants import PARCEL_AREA_MAX_DIGITS
from eagris.model.auth import EagriAuthData


# There are quite a lot of restrictions and rules to be known when using LPI_ZZP01_B, see
# https://eagri.cz/public/portal/mze/farmar/elektronicka-vymena-dat/prehled-vystavenych-sluzeb/LPISPF/lpi-zzp01b

@dataclass
class ParcelManagementRequestCrop:
    legislative_code: int
    valid_from: date
    valid_to: Optional[date]
    catch_crop: bool


@dataclass
class ParcelCreationRequestParcel:
    name: str
    # WKT in EPSG:5514
    geometry: str
    area_ha: float
    valid_from: date
    crops: List[ParcelManagementRequestCrop]


def eagri_area(area: float):
    return round(area, PARCEL_AREA_MAX_DIGITS)


@dataclass
class ParcelUpdateRequestParcel:
    # IDPARCELA, a unique eAGRI identifier
    id: int
    name: str
    # WKT in EPSG:5514
    geometry: str
    area_ha: float
    valid_from: date
    # pass in list of ALL crops, even historic ones
    crops: List[ParcelManagementRequestCrop]


@dataclass
class ParcelCreationRequest:
    eagri_auth_data: EagriAuthData
    # IDDPB, a unique eAGRI identifier
    iddpb: int
    parcels: List[ParcelCreationRequestParcel]


@dataclass
class ParcelUpdateRequest:
    eagri_auth_data: EagriAuthData
    # IDDPB, a unique eAGRI identifier
    iddpb: int
    parcels: List[ParcelUpdateRequestParcel]
