from dataclasses import dataclass
from enum import Enum
from typing import List, Optional


class ParcelErrorCode(Enum):
    E01 = "chyba v pěstování",
    E02 = "platnost parcely zasahuje mimo platnost DPB",
    E03 = "platnosti pěstování jsou mimo platnost parcely nebo se kříží",
    E04 = "neexistující plodina",
    E05 = "parcela není nalezena",
    E06 = "obecná chyba při zakládání parcely",
    E07 = "součet výměr parcel k datu PARCELAOD nesouhlasí s výměrou IDDPB zaokrouhleno na 2 desetinná místa",
    E08 = "parcely nelze k datu založit z důvodu existence parcel na DPB, jejichž platnost je mladší než PARCELAOD",
    E09 = "parcely nelze k datu založit z důvodu nemožnosti ukončení existujících parcel k datu PARCELAOD mínus 1 den",
    E10 = "zaslaná geometrie je nevalidní",
    E11 = "nelze zadat souběh dvou a více plodin na jedné parcele",
    E12 = "nelze založit parcely, pokud mají vzájemný překryv"


@dataclass
class ParcelCreationResponse:
    success: bool
    error_code: Optional[ParcelErrorCode]
    id: int


@dataclass
class ParcelsCreationResponse:
    new_parcels: List[ParcelCreationResponse]


@dataclass
class ParcelUpdateResponse:
    success: bool
    error_code: Optional[ParcelErrorCode]
    id: int


@dataclass
class ParcelsUpdateResponse:
    new_parcels: List[ParcelUpdateResponse]
