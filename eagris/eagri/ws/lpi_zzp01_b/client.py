# flake8: noqa
from datetime import date
from typing import List, Optional

from xsdata.formats.dataclass.parsers import XmlParser

from eagris.common.plugin_constants import DEFAULT_NS
from eagris.common.xml import XmlNode, parse_xml_string, xml_bool
from eagris.eagri.eagri_soap_service_type import EagriSoapServiceType
from eagris.eagri.ws.common.requests import inject_signature_token_into_request, do_send_request, \
    wrap_with_common_elements
from eagris.eagri.ws.generated.lpizzp01b import ResponseType
from eagris.eagri.ws.lpi_zzp01_b.model.request import ParcelCreationRequest, ParcelUpdateRequest, \
    ParcelUpdateRequestParcel, ParcelCreationRequestParcel, ParcelManagementRequestCrop, eagri_area


def create_request(request: ParcelCreationRequest) -> ResponseType:
    login, wskey, szrid, _, _ = request.eagri_auth_data
    request_without_token = lpi_zzp01b_create_request(login, szrid, request.iddpb, request.parcels)
    r = inject_signature_token_into_request(request.eagri_auth_data, request_without_token)
    return XmlParser().from_bytes(do_send_request(r, request.eagri_auth_data.env, EagriSoapServiceType.LPI_ZZP01B))


"""
https://eagri.cz/public/portal/-q26171---i-YnMgUe/aktualizace-dvou-parcel
"""


def update_request(request: ParcelUpdateRequest) -> ResponseType:
    login, wskey, szrid, _, _ = request.eagri_auth_data
    request_without_token = lpi_zzp01b_update_request(login, szrid, request.iddpb, request.parcels)
    r = inject_signature_token_into_request(request.eagri_auth_data, request_without_token)
    return XmlParser().from_bytes(do_send_request(r, request.eagri_auth_data.env, EagriSoapServiceType.LPI_ZZP01B))


def lpi_zzp01b_create_request(login: str, szrid: str, iddpb: int,
                              parcels: List[ParcelCreationRequestParcel]) -> XmlNode:
    return parse_xml_string(
        wrap_with_common_elements(
            EagriSoapServiceType.LPI_ZZP01B,
            login,
            szrid,
            inner_xml=f"<{DEFAULT_NS}:IDDPB>{iddpb}</{DEFAULT_NS}:IDDPB>{__create_parcels_request(parcels)}"
        )
    )


def __create_parcels_request(parcels: List[ParcelCreationRequestParcel]) -> str:
    return "".join([__create_parcel_request(p) for p in parcels])


def __create_parcel_request(p: ParcelCreationRequestParcel) -> str:
    return f"<{DEFAULT_NS}:PARCELA><{DEFAULT_NS}:NAZEV>{p.name}</{DEFAULT_NS}:NAZEV><{DEFAULT_NS}:GEOMETRIE>{p.geometry}</{DEFAULT_NS}:GEOMETRIE><{DEFAULT_NS}:VYMERA>{eagri_area(p.area_ha)}</{DEFAULT_NS}:VYMERA><{DEFAULT_NS}:PARCELAOD>{p.valid_from}</{DEFAULT_NS}:PARCELAOD>{__parcel_crops_request(p.crops)}</{DEFAULT_NS}:PARCELA>"


def lpi_zzp01b_update_request(login: str, szrid: str, iddpb: int, parcels: List[ParcelUpdateRequestParcel]) -> XmlNode:
    return parse_xml_string(
        wrap_with_common_elements(
            EagriSoapServiceType.LPI_ZZP01B,
            login,
            szrid,
            inner_xml=f"<{DEFAULT_NS}:IDDPB>{iddpb}</{DEFAULT_NS}:IDDPB>{__update_parcels_request(parcels)}"
        )
    )


def __update_parcels_request(parcels: List[ParcelUpdateRequestParcel]) -> str:
    return "".join([__update_parcel_request(p) for p in parcels])


"""
TODO: work with geometry is broken for LPI_ZZP01B :)))
add this element once they fix it: <{DEFAULT_NS}:GEOMETRIE>{p.geometry}</{DEFAULT_NS}:GEOMETRIE>
"""


def __update_parcel_request(p: ParcelUpdateRequestParcel) -> str:
    return f"<{DEFAULT_NS}:PARCELA><{DEFAULT_NS}:IDPARCELA>{p.id}</{DEFAULT_NS}:IDPARCELA><{DEFAULT_NS}:NAZEV>{p.name}</{DEFAULT_NS}:NAZEV><{DEFAULT_NS}:GEOMETRIE>{p.geometry}</{DEFAULT_NS}:GEOMETRIE><{DEFAULT_NS}:VYMERA>{eagri_area(p.area_ha)}</{DEFAULT_NS}:VYMERA><{DEFAULT_NS}:PARCELAOD>{p.valid_from}</{DEFAULT_NS}:PARCELAOD>{__parcel_crops_request(p.crops)}</{DEFAULT_NS}:PARCELA>"


def __parcel_crops_request(crops: List[ParcelManagementRequestCrop]) -> str:
    return "".join([__update_parcel_crop_request(c) for c in crops])


def __update_parcel_crop_request(crop: ParcelManagementRequestCrop) -> str:
    return f"<{DEFAULT_NS}:PLODINA><{DEFAULT_NS}:KODPLODINY>{crop.legislative_code}</{DEFAULT_NS}:KODPLODINY><{DEFAULT_NS}:MEZIPLODINA>{xml_bool(crop.catch_crop)}</{DEFAULT_NS}:MEZIPLODINA><{DEFAULT_NS}:PLATNOSTOD>{crop.valid_from}</{DEFAULT_NS}:PLATNOSTOD>{__crop_valid_to(crop.valid_to)}</{DEFAULT_NS}:PLODINA>"


def __crop_valid_to(valid_to: Optional[date]):
    if valid_to is None:
        return ""
    else:
        return f"<{DEFAULT_NS}:PLATNOSTDO>{valid_to}</{DEFAULT_NS}:PLATNOSTDO>"
