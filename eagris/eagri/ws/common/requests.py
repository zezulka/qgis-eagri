# flake8: noqa
from typing import TypeVar
from requests import post

from eagris.eagri.auth.soap_headers import compute_authentication_token
from eagris.eagri.env import EagriEnv, requestUrl
from eagris.eagri.error.error_parser import parse_error_response
from eagris.eagri.namespaces import IMPLICIT_NS
from eagris.common.plugin_constants import DEFAULT_NS
from eagris.common.xml import XmlNode, find_xml_subnode, serialize_xml, parse_xml_string
from eagris.eagri.eagri_soap_service_type import EagriSoapServiceType
from eagris.error.exceptions import EagriRequestException
from eagris.model.auth import EagriAuthData


def wrap_with_common_elements(service: EagriSoapServiceType, login: str, szrid: str, inner_xml: str):
    return f'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><vOKO-wss:Token type="A01" xmlns:vOKO-wss="http://www.pds.eu/vOKO/v0200/wss"></vOKO-wss:Token></soap:Header><soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><Request xmlns="http://www.pds.eu/vOKO/v0200" xmlns:{DEFAULT_NS}="http://sitewell.cz/lpis/schemas/{service.value}" vOKOid="{service.value}"><UIDkey addressAD="default" dn="{login}"></UIDkey><RequestHeader><Subject subjectID="{szrid}"></Subject></RequestHeader><RequestContent><{DEFAULT_NS}:Request>{inner_xml}</{DEFAULT_NS}:Request></RequestContent></Request></soap:Body></soap:Envelope>'


def no_auth_request(service: EagriSoapServiceType, inner_xml: str = ""):
    return f'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><Request xmlns="http://www.pds.eu/vOKO/v0200" xmlns:{DEFAULT_NS}="http://sitewell.cz/lpis/schemas/{service.value}" vOKOid="{service.value}"><RequestContent><{DEFAULT_NS}:Request>{inner_xml}</{DEFAULT_NS}:Request></RequestContent></Request></soap:Body></soap:Envelope>'


def inject_signature_token_into_request(eagri_auth_data: EagriAuthData, request_without_token: XmlNode) -> XmlNode:
    login, wskey, szrid, _, _ = eagri_auth_data
    auth_token = compute_authentication_token(request_without_token, wskey)
    token = find_xml_subnode(request_without_token, 'Token', strict=True, namespace=IMPLICIT_NS)
    token.text = auth_token
    print(eagri_auth_data)
    return request_without_token


ResponseType = TypeVar("ResponseType")


def do_send_request(soap_request: XmlNode, env: EagriEnv, service: EagriSoapServiceType) -> XmlNode:
    headers = {'content-type': 'text/xml'}

    request_url = requestUrl(env, service)
    response = post(request_url,
                    data=serialize_xml(soap_request),
                    headers=headers)
    # TODO debugging logs should be a bit more clever than this :)
    #      ideally, the plugin should put its debugging logs to
    #      - stdout
    #      - QGIS plugin message log
    #      - (optionally) a file
    #     using print has one great benefit - we do not couple SOAP clients with QGIS
    print(f"URL: {request_url}")
    print(f"REQUEST: {serialize_xml(soap_request)}")
    print(f"RESPONSE: {response.content}")
    print(f"NAMESPACE: {service.namespace()}")
    if response.ok:
        soap_response_body = parse_xml_string(response.content)
        return serialize_xml(find_xml_subnode(soap_response_body, 'Response', strict=True, namespace=service.namespace()))
    else:
        raise EagriRequestException(
            response.status_code,
            parse_error_response(parse_xml_string(response.content)))
