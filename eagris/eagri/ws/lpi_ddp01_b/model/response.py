from datetime import date
from typing import List, Optional

from pydantic.dataclasses import dataclass


@dataclass
class ParcelVersion:
    id: int
    geometry: str
    valid_from: date
    valid_to: Optional[date]
    area: float


@dataclass
class ParcelCrop:
    crop_id: int
    crop_name: str
    catch_crop: bool
    valid_from: date
    valid_to: Optional[date]


@dataclass
class Parcel:
    id: int
    name: str
    valid_from: date
    valid_to: Optional[date]
    versions: List[ParcelVersion]
    crops: List[ParcelCrop]
    iddpb: int


@dataclass
class DpbDetailResponse:
    parcels: List[Parcel]
