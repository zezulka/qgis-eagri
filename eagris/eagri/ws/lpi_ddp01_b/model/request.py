from typing import List

from pydantic.dataclasses import dataclass

from eagris.eagri.ws.generated.lpiddp01b import TypDataKodType
from eagris.eagri.ws.lpi_gdp11_b.model.response import Dpb
from eagris.model.auth import EagriAuthData


@dataclass
class DpbDetailRequest:
    eagri_auth_data: EagriAuthData
    dpb: Dpb
    data_types: List[TypDataKodType]
