from typing import List

from xsdata.formats.dataclass.parsers import XmlParser

from eagris.eagri.ws.common.requests import wrap_with_common_elements, inject_signature_token_into_request, do_send_request
from eagris.eagri.ws.generated.lpiddp01b import ResponseType
from eagris.eagri.eagri_soap_service_type import EagriSoapServiceType
from eagris.common.plugin_constants import DEFAULT_NS
from eagris.common.xml import parse_xml_string
from eagris.eagri.ws.generated.lpiddp01b import TypDataKodType
from eagris.eagri.ws.lpi_ddp01_b.model.request import DpbDetailRequest

"""
Why do we keep XML documents as string oneliners?
Because eAGRI does not care about c14n spec and evidently strips whitespaces to compute document signature. :)
#https://www.w3.org/TR/2001/REC-xml-c14n-20010315#Example-WhitespaceInContent

That's why standard c14n is not a good idea.
"""


def request(list_request: DpbDetailRequest) -> ResponseType:
    login, wskey, szrid, _, _ = list_request.eagri_auth_data
    request_without_token = lpiddp01bRequest(login, szrid, list_request.dpb.id, list_request.data_types)
    r = inject_signature_token_into_request(list_request.eagri_auth_data, request_without_token)
    return XmlParser().from_bytes(do_send_request(r, list_request.eagri_auth_data.env, EagriSoapServiceType.LPI_DDP01B))


def lpiddp01bRequest(login: str, szrid: str, iddpb: int, data_types: List[TypDataKodType]):
    return parse_xml_string(
        wrap_with_common_elements(
            EagriSoapServiceType.LPI_DDP01B,
            login,
            szrid,
            inner_xml=f"<{DEFAULT_NS}:IDDPB>{iddpb}</{DEFAULT_NS}:IDDPB>{__typeData(data_types)}"
        )
    )


def __typeData(data_types: List[TypDataKodType]):
    return "".join([
        f"<{DEFAULT_NS}:TYPDATA><{DEFAULT_NS}:TYPDATAKOD>{kod.value}</{DEFAULT_NS}:TYPDATAKOD></{DEFAULT_NS}:TYPDATA>"
        for kod in data_types
    ])
