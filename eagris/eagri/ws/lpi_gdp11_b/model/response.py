from dataclasses import dataclass
from datetime import date
from typing import List, Optional


@dataclass
class Dpb:
    """
    Stands in Czech for "díl půdního bloku". It is also sometimes called an "LPIS block".

    Represents a continuous piece of agricultural land. Farmer can decide
    to divide a DPB into smaller parts called "zemědělská parcela", an agricultural parcel.
    https://www.szif.cz/cs/CmDocument?rid=%2Fapa_anon%2Fcs%2Fdokumenty_ke_stazeni%2Fnejcastejsi_dotazy%2Flpis%2F1527160825249.pdf
    """
    id: int
    square: str
    zkod: str
    status: str
    valid_from: date
    area: float
    culture: str
    culture_name: str
    valid_to: Optional[date]
    geometry: str


@dataclass
class DpbListResponse:
    changed: date
    blocks: List[Dpb]


@dataclass
class EagriErrorResponse:
    code: Optional[str]
    message: str
