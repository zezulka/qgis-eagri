from dataclasses import dataclass
from datetime import date

from eagris.model.auth import EagriAuthData


@dataclass
class DpbListRequest:
    eagri_auth_data: EagriAuthData
    date_from: date
    date_to: date
