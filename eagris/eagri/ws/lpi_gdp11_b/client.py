# flake8: noqa
from datetime import date

from xsdata.formats.dataclass.parsers import XmlParser

from eagris.common.plugin_constants import DEFAULT_NS
from eagris.common.xml import XmlNode, parse_xml_string
from eagris.eagri.eagri_soap_service_type import EagriSoapServiceType
from eagris.eagri.ws.common.requests import inject_signature_token_into_request, do_send_request, wrap_with_common_elements
from eagris.eagri.ws.generated.lpigdp11b import ResponseType
from eagris.eagri.ws.lpi_gdp11_b.model.request import DpbListRequest

"""
Why do we keep XML documents as string oneliners?
Because eAGRI does not care about c14n spec and evidently strips whitespaces to compute document signature. :)
#https://www.w3.org/TR/2001/REC-xml-c14n-20010315#Example-WhitespaceInContent

That's why standard c14n is not a good idea.
"""


def request(list_request: DpbListRequest) -> ResponseType:
    login, wskey, szrid, _, _ = list_request.eagri_auth_data
    request_without_token = lpiGdp11bRequest(login, szrid, list_request.date_from, list_request.date_to)
    r = inject_signature_token_into_request(list_request.eagri_auth_data, request_without_token)
    return XmlParser().from_bytes(do_send_request(r, list_request.eagri_auth_data.env, EagriSoapServiceType.LPI_GDP11B))


def lpiGdp11bRequest(login: str, szrid: str, date_from: date, date_to: date) -> XmlNode:
    return parse_xml_string(
        wrap_with_common_elements(
            EagriSoapServiceType.LPI_GDP11B,
            login,
            szrid,
            inner_xml=f"<{DEFAULT_NS}:GETDATA>true</{DEFAULT_NS}:GETDATA><{DEFAULT_NS}:DATOD>{date_from.isoformat()}</{DEFAULT_NS}:DATOD><{DEFAULT_NS}:DATDO>{date_to.isoformat()}</{DEFAULT_NS}:DATDO><{DEFAULT_NS}:TYPDATA><{DEFAULT_NS}:TYPDATAKOD>ZAKLADMIN</{DEFAULT_NS}:TYPDATAKOD></{DEFAULT_NS}:TYPDATA>"
        )
    )
