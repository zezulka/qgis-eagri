from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class Srvid:
    class Meta:
        name = "SRVid"

    server_id: Optional[str] = field(
        default=None,
        metadata={
            "name": "serverID",
            "type": "Attribute",
            "required": True,
        }
    )
