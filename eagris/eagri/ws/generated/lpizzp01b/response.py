from dataclasses import dataclass
from .response_type import ResponseType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_ZZP01B"


@dataclass(frozen=True)
class Response(ResponseType):
    class Meta:
        namespace = "http://sitewell.cz/lpis/schemas/LPI_ZZP01B"
