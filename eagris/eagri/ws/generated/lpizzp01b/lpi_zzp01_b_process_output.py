from dataclasses import dataclass, field
from typing import Optional
from .error import Error
from .response_1 import Response1

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class LpiZzp01BProcessOutput:
    class Meta:
        name = "Envelope"
        namespace = "http://schemas.xmlsoap.org/soap/envelope/"

    body: Optional["LpiZzp01BProcessOutput.Body"] = field(
        default=None,
        metadata={
            "name": "Body",
            "type": "Element",
        }
    )

    @dataclass(frozen=True)
    class Body:
        response: Optional[Response1] = field(
            default=None,
            metadata={
                "name": "Response",
                "type": "Element",
                "namespace": "http://www.pds.eu/vOKO/v0200",
            }
        )
        fault: Optional["LpiZzp01BProcessOutput.Body.Fault"] = field(
            default=None,
            metadata={
                "name": "Fault",
                "type": "Element",
            }
        )

        @dataclass(frozen=True)
        class Fault:
            faultcode: Optional[str] = field(
                default=None,
                metadata={
                    "type": "Element",
                    "namespace": "",
                }
            )
            faultstring: Optional[str] = field(
                default=None,
                metadata={
                    "type": "Element",
                    "namespace": "",
                }
            )
            faultactor: Optional[str] = field(
                default=None,
                metadata={
                    "type": "Element",
                    "namespace": "",
                }
            )
            detail: Optional["LpiZzp01BProcessOutput.Body.Fault.Detail"] = field(
                default=None,
                metadata={
                    "type": "Element",
                    "namespace": "",
                }
            )

            @dataclass(frozen=True)
            class Detail:
                error: Optional[Error] = field(
                    default=None,
                    metadata={
                        "name": "Error",
                        "type": "Element",
                        "namespace": "http://www.pds.eu/vOKO/v0200",
                    }
                )
