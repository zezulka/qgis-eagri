from dataclasses import dataclass, field
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_ZZP01B"


@dataclass(frozen=True)
class PlodinaType:
    class Meta:
        name = "plodinaType"

    kodplodiny: Optional[int] = field(
        default=None,
        metadata={
            "name": "KODPLODINY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "required": True,
        }
    )
    meziplodina: Optional[bool] = field(
        default=None,
        metadata={
            "name": "MEZIPLODINA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
        }
    )
    gps: Optional[bool] = field(
        default=None,
        metadata={
            "name": "GPS",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
        }
    )
