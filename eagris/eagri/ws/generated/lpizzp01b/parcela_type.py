from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .plodina_type import PlodinaType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_ZZP01B"


@dataclass(frozen=True)
class ParcelaType:
    class Meta:
        name = "parcelaType"

    idparcela: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDPARCELA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "total_digits": 10,
        }
    )
    nazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "NAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "required": True,
            "max_length": 50,
        }
    )
    geometrie: Optional[str] = field(
        default=None,
        metadata={
            "name": "GEOMETRIE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
        }
    )
    vymera: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    parcelaod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PARCELAOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "required": True,
        }
    )
    plodina: Tuple[PlodinaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PLODINA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "min_occurs": 1,
        }
    )
