from dataclasses import dataclass, field
from typing import Tuple
from .parcela_resp_type import ParcelaRespType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_ZZP01B"


@dataclass(frozen=True)
class ResponseType:
    parcela: Tuple[ParcelaRespType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PARCELA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "min_occurs": 1,
        }
    )
