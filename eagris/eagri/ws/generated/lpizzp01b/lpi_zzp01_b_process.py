
from .lpi_zzp01_b_process_input import LpiZzp01BProcessInput
from .lpi_zzp01_b_process_output import LpiZzp01BProcessOutput

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


class LpiZzp01BProcess:
    style = "document"
    location = "https://eagri.cz/ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.ashx"
    transport = "http://schemas.xmlsoap.org/soap/http"
    soap_action = "process"
    input = LpiZzp01BProcessInput
    output = LpiZzp01BProcessOutput
