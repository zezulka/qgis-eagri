from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class ResponseHeader:
    request_id: Optional[str] = field(
        default=None,
        metadata={
            "name": "RequestID",
            "type": "Element",
            "namespace": "http://www.pds.eu/vOKO/v0200",
            "required": True,
            "max_length": 100,
        }
    )
