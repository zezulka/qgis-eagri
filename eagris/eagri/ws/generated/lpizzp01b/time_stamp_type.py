from enum import Enum

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


class TimeStampType(Enum):
    BASE = "base"
    QUALIFIED = "qualified"
