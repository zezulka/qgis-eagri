from dataclasses import dataclass, field
from typing import Optional, Tuple
from .parcela_type import ParcelaType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_ZZP01B"


@dataclass(frozen=True)
class RequestType:
    iddpb: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    parcela: Tuple[ParcelaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PARCELA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_ZZP01B",
            "min_occurs": 1,
        }
    )
