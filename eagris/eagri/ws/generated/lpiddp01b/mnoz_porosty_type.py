from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional, Tuple, Union
from xsdata.models.datatype import XmlDate
from .prehlidka_type import PrehlidkaType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class MnozPorostyType:
    class Meta:
        name = "mnozPorostyType"

    rok: Optional[int] = field(
        default=None,
        metadata={
            "name": "ROK",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    ciszadosti: Optional[int] = field(
        default=None,
        metadata={
            "name": "CISZADOSTI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    datpodani: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATPODANI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    zadatel: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZADATEL",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "min_length": 0,
            "max_length": 255,
        }
    )
    zadatelico: Optional[Union[str, int]] = field(
        default=None,
        metadata={
            "name": "ZADATELICO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "max_length": 0,
            "total_digits": 8,
        }
    )
    druhkod: Optional[int] = field(
        default=None,
        metadata={
            "name": "DRUHKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    druhnazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "DRUHNAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    odrudakod: Optional[int] = field(
        default=None,
        metadata={
            "name": "ODRUDAKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    odrudanazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "ODRUDANAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    kategorie: Optional[str] = field(
        default=None,
        metadata={
            "name": "KATEGORIE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    generace: Optional[str] = field(
        default=None,
        metadata={
            "name": "GENERACE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vymerazadost: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERAZADOST",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    hodnoceni: Optional[str] = field(
        default=None,
        metadata={
            "name": "HODNOCENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    kvalitavazby: Optional[str] = field(
        default=None,
        metadata={
            "name": "KVALITAVAZBY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    zrusenazadost: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZRUSENAZADOST",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    cisrozhodnuti: Optional[str] = field(
        default=None,
        metadata={
            "name": "CISROZHODNUTI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    prehlidka: Tuple[PrehlidkaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PREHLIDKA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
