from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class EfasType:
    class Meta:
        name = "efasType"

    efaskod: Optional[str] = field(
        default=None,
        metadata={
            "name": "EFASKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vymefas: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMEFAS",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    vymefasprep: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMEFASPREP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
