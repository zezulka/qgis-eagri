from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class StavIdType(Enum):
    VALUE_4 = 4
    VALUE_5 = 5
