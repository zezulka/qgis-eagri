from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class AppInfo:
    app_module: Optional["AppInfo.AppModule"] = field(
        default=None,
        metadata={
            "name": "AppModule",
            "type": "Element",
            "namespace": "http://www.pds.eu/vOKO/v0200",
            "required": True,
        }
    )

    @dataclass(frozen=True)
    class AppModule:
        id: Optional[str] = field(
            default=None,
            metadata={
                "type": "Attribute",
                "required": True,
                "max_length": 35,
            }
        )
        version: Optional[str] = field(
            default=None,
            metadata={
                "type": "Attribute",
                "required": True,
                "max_length": 20,
            }
        )
