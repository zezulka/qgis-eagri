from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class GetSeznamDpbType(Enum):
    VALUE_0 = 0
    VALUE_1 = 1
    VALUE_2 = 2
    VALUE_3 = 3
