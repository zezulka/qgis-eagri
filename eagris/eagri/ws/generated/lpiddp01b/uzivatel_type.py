from dataclasses import dataclass, field
from typing import Optional, Union

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class UzivatelType:
    class Meta:
        name = "uzivatelType"

    iduzivatele: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDUZIVATELE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    jmeno: Optional[str] = field(
        default=None,
        metadata={
            "name": "JMENO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_length": 0,
            "max_length": 255,
        }
    )
    prijmeni: Optional[str] = field(
        default=None,
        metadata={
            "name": "PRIJMENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_length": 0,
            "max_length": 255,
        }
    )
    obchodnijmeno: Optional[str] = field(
        default=None,
        metadata={
            "name": "OBCHODNIJMENO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_length": 0,
            "max_length": 255,
        }
    )
    ic: Optional[Union[str, int]] = field(
        default=None,
        metadata={
            "name": "IC",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "max_length": 0,
            "total_digits": 8,
        }
    )
    pravniforma: Optional[int] = field(
        default=None,
        metadata={
            "name": "PRAVNIFORMA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
