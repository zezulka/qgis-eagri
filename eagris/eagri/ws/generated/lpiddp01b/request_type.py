from dataclasses import dataclass, field
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .get_seznam_dpb_type import GetSeznamDpbType
from .typ_data_type import TypDataType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class RequestType:
    getzmenenedpb: Optional[bool] = field(
        default=None,
        metadata={
            "name": "GETZMENENEDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    getseznamdpb: Optional[GetSeznamDpbType] = field(
        default=None,
        metadata={
            "name": "GETSEZNAMDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    datumzmenyod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATUMZMENYOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    ctverec: Optional[str] = field(
        default=None,
        metadata={
            "name": "CTVEREC",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_length": 1,
            "max_length": 8,
        }
    )
    zkod: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_length": 1,
            "max_length": 10,
        }
    )
    iddpb: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "total_digits": 10,
        }
    )
    datod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    datdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    typdata: Tuple[TypDataType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "TYPDATA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
