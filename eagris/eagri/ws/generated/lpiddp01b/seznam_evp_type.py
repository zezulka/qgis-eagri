from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate
from .druh_id_type import DruhIdType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class SeznamEvpType:
    class Meta:
        name = "seznamEvpType"

    evpid: Optional[int] = field(
        default=None,
        metadata={
            "name": "EVPID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    ctverec: Optional[str] = field(
        default=None,
        metadata={
            "name": "CTVEREC",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "min_length": 1,
            "max_length": 8,
        }
    )
    zkod: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "min_length": 1,
            "max_length": 10,
        }
    )
    vymera: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
            "fraction_digits": 2,
        }
    )
    vymeraprekryv: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERAPREKRYV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
            "fraction_digits": 2,
        }
    )
    geometrieevp: Optional[str] = field(
        default=None,
        metadata={
            "name": "GEOMETRIEEVP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    druhnazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "DRUHNAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    druhid: Optional[DruhIdType] = field(
        default=None,
        metadata={
            "name": "DRUHID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
