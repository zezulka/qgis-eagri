from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class TypDataKodType(Enum):
    ZAKLAD = "ZAKLAD"
    KATUZE = "KATUZE"
    EVP = "EVP"
    NS = "NS"
    EROZE = "EROZE"
    LFA = "LFA"
    MZP = "MZP"
    OPVZ = "OPVZ"
    OPV = "OPV"
    BPEJ = "BPEJ"
    AEKO = "AEKO"
    EFAS = "EFAS"
    GMO = "GMO"
    AZZP = "AZZP"
    ZEMPARCELY = "ZEMPARCELY"
    ZAKLADMIN = "ZAKLADMIN"
    EROZE2_G = "EROZE2G"
    HON = "HON"
    UPOB = "UPOB"
    UP = "UP"
    NEPRODPLOCHY = "NEPRODPLOCHY"
