from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class NeprodPlochyType:
    class Meta:
        name = "neprodPlochyType"

    id: Optional[int] = field(
        default=None,
        metadata={
            "name": "ID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    typnp: Optional[str] = field(
        default=None,
        metadata={
            "name": "TYPNP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    zkodnp: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZKODNP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    ctverecnp: Optional[str] = field(
        default=None,
        metadata={
            "name": "CTVERECNP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vymnp: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMNP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
