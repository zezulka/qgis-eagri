from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class BooleanNumberType(Enum):
    VALUE_0 = 0
    VALUE_1 = 1
