from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .apl_pasmo_type import AplPasmoType
from .eroze2g_type import Eroze2GType
from .neprod_plochy_type import NeprodPlochyType
from .opatreni_ns_type import OpatreniNsType
from .zakladni_type import ZakladniType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class ParcelaVerzeType:
    class Meta:
        name = "parcelaVerzeType"

    idparcelaverze: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDPARCELAVERZE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    geometrie: Optional[str] = field(
        default=None,
        metadata={
            "name": "GEOMETRIE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    multipolygon: Optional[bool] = field(
        default=None,
        metadata={
            "name": "MULTIPOLYGON",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vymera: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    parcelaverzeod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PARCELAVERZEOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    parcelaverzedo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PARCELAVERZEDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    zakladni: Tuple[ZakladniType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "ZAKLADNI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    opatrenins: Tuple[OpatreniNsType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "OPATRENINS",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    aplpasmo: Tuple[AplPasmoType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "APLPASMO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    eroze2_g: Tuple[Eroze2GType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "EROZE2G",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    neprodplochy: Tuple[NeprodPlochyType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "NEPRODPLOCHY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
