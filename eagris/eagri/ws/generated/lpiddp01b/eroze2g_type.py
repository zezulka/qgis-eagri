from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .ptopatreni_type import PtopatreniType
from .vysledna_eroze_type import VyslednaErozeType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class Eroze2GType:
    class Meta:
        name = "eroze2gType"

    vyslednaeroze: Optional[VyslednaErozeType] = field(
        default=None,
        metadata={
            "name": "VYSLEDNAEROZE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    seocelkem: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "SEOCELKEM",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    seomeocelkem: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "SEOMEOCELKEM",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    maxsouvseo: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "MAXSOUVSEO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    maxsouvseomeo: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "MAXSOUVSEOMEO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    osevod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    osevdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    ptopatreni: Tuple[PtopatreniType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PTOPATRENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
