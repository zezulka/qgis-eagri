from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class ZmeneneDpbType:
    class Meta:
        name = "zmeneneDpbType"

    iddpb: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
