from dataclasses import dataclass, field
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .historie_type import HistorieType
from .odpoc_plochy_type import OdpocPlochyType
from .parcela_verze_type import ParcelaVerzeType
from .plodina_type import PlodinaType
from .plodiny_rozsirene_type import PlodinyRozsireneType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class ZemParcelyType:
    class Meta:
        name = "zemParcelyType"

    idparcela: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDPARCELA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    nazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "NAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    poradi: Optional[int] = field(
        default=None,
        metadata={
            "name": "PORADI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    parcelaod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PARCELAOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    parcelado: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PARCELADO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    idparcelapredek: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDPARCELAPREDEK",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "total_digits": 10,
        }
    )
    plodina: Tuple[PlodinaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PLODINA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_occurs": 1,
        }
    )
    plodinyrozsirene: Tuple[PlodinyRozsireneType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PLODINYROZSIRENE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "min_occurs": 1,
        }
    )
    historie: Tuple[HistorieType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "HISTORIE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    odpocplochy: Tuple[OdpocPlochyType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "ODPOCPLOCHY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    parcelaverze: Tuple[ParcelaVerzeType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PARCELAVERZE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
