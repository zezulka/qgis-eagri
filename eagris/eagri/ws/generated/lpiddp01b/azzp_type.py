from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class AzzpType:
    class Meta:
        name = "azzpType"

    kod: Optional[int] = field(
        default=None,
        metadata={
            "name": "KOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    souradnicex: Optional[str] = field(
        default=None,
        metadata={
            "name": "SOURADNICEX",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    souradnicey: Optional[str] = field(
        default=None,
        metadata={
            "name": "SOURADNICEY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
