from dataclasses import dataclass, field
from typing import Optional
from .app_info import AppInfo
from .request import Request
from .request_header import RequestHeader
from .time_stamp import TimeStamp
from .uidkey import Uidkey

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class Request1:
    class Meta:
        name = "Request"
        namespace = "http://www.pds.eu/vOKO/v0200"

    uidkey: Optional[Uidkey] = field(
        default=None,
        metadata={
            "name": "UIDkey",
            "type": "Element",
            "required": True,
        }
    )
    time_stamp: Optional[TimeStamp] = field(
        default=None,
        metadata={
            "name": "TimeStamp",
            "type": "Element",
        }
    )
    app_info: Optional[AppInfo] = field(
        default=None,
        metadata={
            "name": "AppInfo",
            "type": "Element",
        }
    )
    request_header: Optional[RequestHeader] = field(
        default=None,
        metadata={
            "name": "RequestHeader",
            "type": "Element",
        }
    )
    request_content: Optional["Request1.RequestContent"] = field(
        default=None,
        metadata={
            "name": "RequestContent",
            "type": "Element",
            "required": True,
        }
    )
    v_okoid: Optional[str] = field(
        default=None,
        metadata={
            "name": "vOKOid",
            "type": "Attribute",
            "required": True,
            "max_length": 15,
        }
    )

    @dataclass(frozen=True)
    class RequestContent:
        request: Optional[Request] = field(
            default=None,
            metadata={
                "name": "Request",
                "type": "Element",
                "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            }
        )
