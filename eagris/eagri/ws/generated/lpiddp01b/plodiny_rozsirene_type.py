from dataclasses import dataclass, field
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class PlodinyRozsireneType:
    class Meta:
        name = "plodinyRozsireneType"

    pestovaniid: Optional[int] = field(
        default=None,
        metadata={
            "name": "PESTOVANIID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    kodplodiny: Optional[int] = field(
        default=None,
        metadata={
            "name": "KODPLODINY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    kodproduktu: Optional[int] = field(
        default=None,
        metadata={
            "name": "KODPRODUKTU",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    kodusp: Optional[int] = field(
        default=None,
        metadata={
            "name": "KODUSP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    kodpot: Optional[int] = field(
        default=None,
        metadata={
            "name": "KODPOT",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    pot: Optional[str] = field(
        default=None,
        metadata={
            "name": "POT",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    poznamka: Optional[str] = field(
        default=None,
        metadata={
            "name": "POZNAMKA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    meziplodina: Optional[bool] = field(
        default=None,
        metadata={
            "name": "MEZIPLODINA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
