from dataclasses import dataclass, field
from typing import Optional
from xsdata.models.datatype import XmlDate
from .kod_opatreni_type import KodOpatreniType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class OpatreniMeoType:
    class Meta:
        name = "opatreniMeoType"

    kodopatreni: Optional[KodOpatreniType] = field(
        default=None,
        metadata={
            "name": "KODOPATRENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    osevod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    osevdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
