from dataclasses import dataclass, field
from typing import Tuple
from .dpb_type import DpbType
from .seznam_dpb_type import SeznamDpbType
from .zmenene_dpb_type import ZmeneneDpbType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class ResponseType:
    zmenenedpb: Tuple[ZmeneneDpbType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "ZMENENEDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    dpb: Tuple[DpbType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "DPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    seznamdpb: Tuple[SeznamDpbType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "SEZNAMDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
