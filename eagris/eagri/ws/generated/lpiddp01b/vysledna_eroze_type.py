from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class VyslednaErozeType(Enum):
    SEO = "SEO"
    MEO = "MEO"
    NEO = "NEO"
