from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate, XmlDateTime

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class SeznamDpbType:
    class Meta:
        name = "seznamDpbType"

    iddpb: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    ctverec: Optional[str] = field(
        default=None,
        metadata={
            "name": "CTVEREC",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "min_length": 1,
            "max_length": 8,
        }
    )
    zkod: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "min_length": 1,
            "max_length": 10,
        }
    )
    vymera: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    datumzucineni: Optional[XmlDateTime] = field(
        default=None,
        metadata={
            "name": "DATUMZUCINENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    zmena_napoctu_dat: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "ZMENA_NAPOCTU_DAT",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    deleted_fb_id: Optional[bool] = field(
        default=None,
        metadata={
            "name": "DELETED_FB_ID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
