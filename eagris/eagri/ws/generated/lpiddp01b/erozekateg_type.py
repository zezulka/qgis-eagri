from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class ErozekategType(Enum):
    VALUE_1 = 1
    VALUE_2 = 2
    VALUE_3 = 3
