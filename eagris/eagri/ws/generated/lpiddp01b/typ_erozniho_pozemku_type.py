from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class TypEroznihoPozemkuType(Enum):
    EEP = "EEP"
    VEP = "VEP"
