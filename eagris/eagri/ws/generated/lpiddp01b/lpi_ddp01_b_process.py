
from .lpi_ddp01_b_process_input import LpiDdp01BProcessInput
from .lpi_ddp01_b_process_output import LpiDdp01BProcessOutput

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


class LpiDdp01BProcess:
    style = "document"
    location = "https://eagri.cz/ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.ashx"
    transport = "http://schemas.xmlsoap.org/soap/http"
    soap_action = "process"
    input = LpiDdp01BProcessInput
    output = LpiDdp01BProcessOutput
