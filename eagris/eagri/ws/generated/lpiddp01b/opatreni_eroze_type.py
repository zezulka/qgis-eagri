from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate
from .erozekateg_type import ErozekategType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class OpatreniErozeType:
    class Meta:
        name = "opatreniErozeType"

    erozekateg: Optional[ErozekategType] = field(
        default=None,
        metadata={
            "name": "EROZEKATEG",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vymprekryv: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMPREKRYV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    osevod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    osevdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    cfaktorod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "CFAKTOROD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    cfaktordo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "CFAKTORDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
