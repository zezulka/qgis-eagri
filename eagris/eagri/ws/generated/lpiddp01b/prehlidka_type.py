from dataclasses import dataclass, field
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class PrehlidkaType:
    class Meta:
        name = "prehlidkaType"

    poradi: Optional[int] = field(
        default=None,
        metadata={
            "name": "PORADI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    datprovedeni: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATPROVEDENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vysledek: Optional[str] = field(
        default=None,
        metadata={
            "name": "VYSLEDEK",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "min_length": 0,
            "max_length": 255,
        }
    )
    stav: Optional[int] = field(
        default=None,
        metadata={
            "name": "STAV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
