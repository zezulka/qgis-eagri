from dataclasses import dataclass, field
from typing import Optional
from xsdata.models.datatype import XmlDateTime
from .srvid import Srvid

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class Error:
    class Meta:
        namespace = "http://www.pds.eu/vOKO/v0200"

    error_code: Optional[str] = field(
        default=None,
        metadata={
            "name": "ErrorCode",
            "type": "Element",
            "required": True,
        }
    )
    error_description: Optional[str] = field(
        default=None,
        metadata={
            "name": "ErrorDescription",
            "type": "Element",
            "required": True,
        }
    )
    date_time: Optional[XmlDateTime] = field(
        default=None,
        metadata={
            "name": "DateTime",
            "type": "Element",
            "required": True,
        }
    )
    sid: Optional[str] = field(
        default=None,
        metadata={
            "name": "SID",
            "type": "Element",
            "max_length": 50,
        }
    )
    request_id: Optional[str] = field(
        default=None,
        metadata={
            "name": "RequestID",
            "type": "Element",
            "max_length": 100,
        }
    )
    srvid: Optional[Srvid] = field(
        default=None,
        metadata={
            "name": "SRVid",
            "type": "Element",
        }
    )
    v_okoid: Optional[str] = field(
        default=None,
        metadata={
            "name": "vOKOid",
            "type": "Element",
            "max_length": 15,
        }
    )
    response_lang: Optional[str] = field(
        default=None,
        metadata={
            "name": "ResponseLang",
            "type": "Element",
            "max_length": 3,
        }
    )
    show_xsltpath: Optional[str] = field(
        default=None,
        metadata={
            "name": "ShowXSLTpath",
            "type": "Element",
        }
    )
    show_xslttype: Optional[str] = field(
        default=None,
        metadata={
            "name": "ShowXSLTtype",
            "type": "Element",
        }
    )
    display: Optional[str] = field(
        default=None,
        metadata={
            "name": "Display",
            "type": "Element",
        }
    )
    desc: Optional[str] = field(
        default=None,
        metadata={
            "name": "Desc",
            "type": "Element",
        }
    )
    inner_exception: Optional["Error.InnerException"] = field(
        default=None,
        metadata={
            "name": "InnerException",
            "type": "Element",
        }
    )

    @dataclass(frozen=True)
    class InnerException:
        any_element: Optional[object] = field(
            default=None,
            metadata={
                "type": "Wildcard",
                "namespace": "##any",
            }
        )
