from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


class KodOpatreniType(Enum):
    P1 = "P1"
    P2 = "P2"
    P3 = "P3"
    Z0 = "Z0"
    Z1 = "Z1"
    Z2 = "Z2"
    Z3 = "Z3"
    S0 = "S0"
    S1 = "S1"
    S2 = "S2"
    S3 = "S3"
    V0 = "V0"
    V1 = "V1"
    V2 = "V2"
    V3 = "V3"
    V4 = "V4"
    K = "K"
    R = "R"
    LOS = "LOS"
    ST = "ST"
    PK = "PK"
