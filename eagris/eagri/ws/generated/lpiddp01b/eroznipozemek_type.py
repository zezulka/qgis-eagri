from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .geoinfo_type import GeoinfoType
from .prisluzdpb_type import PrisluzdpbType
from .ptopatreni_type import PtopatreniType
from .typ_erozniho_pozemku_type import TypEroznihoPozemkuType
from .vysledna_eroze_type import VyslednaErozeType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class EroznipozemekType:
    class Meta:
        name = "eroznipozemekType"

    id: Optional[int] = field(
        default=None,
        metadata={
            "name": "ID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    typ: Optional[TypEroznihoPozemkuType] = field(
        default=None,
        metadata={
            "name": "TYP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    datumvytvoreni: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATUMVYTVORENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    vytvorilkdo: Optional[str] = field(
        default=None,
        metadata={
            "name": "VYTVORILKDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    poznamka: Optional[str] = field(
        default=None,
        metadata={
            "name": "POZNAMKA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    geoinfo: Tuple[GeoinfoType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "GEOINFO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    prislusdpb: Optional[PrisluzdpbType] = field(
        default=None,
        metadata={
            "name": "PRISLUSDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    vyslednaeroze: Optional[VyslednaErozeType] = field(
        default=None,
        metadata={
            "name": "VYSLEDNAEROZE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    seocelkem: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "SEOCELKEM",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    seomeocelkem: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "SEOMEOCELKEM",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    maxsouvseo: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "MAXSOUVSEO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    maxsouvseomeo: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "MAXSOUVSEOMEO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    osevod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    osevdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "OSEVDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
    ptopatreni: Tuple[PtopatreniType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PTOPATRENI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
