from dataclasses import dataclass, field
from typing import Optional
from xsdata.models.datatype import XmlDateTime
from .time_stamp_type import TimeStampType

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class TimeStamp:
    value: Optional[XmlDateTime] = field(
        default=None,
        metadata={
            "required": True,
        }
    )
    type: Optional[TimeStampType] = field(
        default=None,
        metadata={
            "type": "Attribute",
            "required": True,
        }
    )
