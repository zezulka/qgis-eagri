from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_DDP01B"


@dataclass(frozen=True)
class HistorieType:
    class Meta:
        name = "historieType"

    parcelaid: Optional[int] = field(
        default=None,
        metadata={
            "name": "PARCELAID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
        }
    )
    parcelavymy: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "PARCELAVYMY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 12,
            "fraction_digits": 4,
        }
    )
    prekryv: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "PREKRYV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
            "total_digits": 10,
            "fraction_digits": 4,
        }
    )
    vlastnizakre: Optional[bool] = field(
        default=None,
        metadata={
            "name": "VLASTNIZAKRE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_DDP01B",
        }
    )
