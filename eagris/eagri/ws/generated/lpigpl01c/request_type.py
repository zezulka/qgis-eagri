from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"


@dataclass(frozen=True)
class RequestType:
    getdata: Optional[bool] = field(
        default=None,
        metadata={
            "name": "GETDATA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
        }
    )
