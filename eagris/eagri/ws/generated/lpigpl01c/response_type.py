from dataclasses import dataclass, field
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .plodina_type import PlodinaType
from .skupina_plodin_type import SkupinaPlodinType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"


@dataclass(frozen=True)
class ResponseType:
    datzmenycis: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATZMENYCIS",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
        }
    )
    skupinaplodin: Tuple[SkupinaPlodinType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "SKUPINAPLODIN",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
        }
    )
    plodina: Tuple[PlodinaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PLODINA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
        }
    )
