from dataclasses import dataclass, field
from typing import Optional, Union
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"


@dataclass(frozen=True)
class SkupinaPlodinType:
    class Meta:
        name = "skupinaPlodinType"

    id: Optional[int] = field(
        default=None,
        metadata={
            "name": "ID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "total_digits": 10,
        }
    )
    nazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "NAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "min_length": 0,
            "max_length": 150,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
        }
    )
    platnostdo: Optional[Union[str, XmlDate]] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "max_length": 0,
        }
    )
    datzmeny: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATZMENY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
        }
    )
