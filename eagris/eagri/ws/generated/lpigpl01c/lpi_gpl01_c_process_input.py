from dataclasses import dataclass, field
from typing import Optional
from .request_1 import Request1

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class LpiGpl01CProcessInput:
    class Meta:
        name = "Envelope"
        namespace = "http://schemas.xmlsoap.org/soap/envelope/"

    body: Optional["LpiGpl01CProcessInput.Body"] = field(
        default=None,
        metadata={
            "name": "Body",
            "type": "Element",
        }
    )

    @dataclass(frozen=True)
    class Body:
        request: Optional[Request1] = field(
            default=None,
            metadata={
                "name": "Request",
                "type": "Element",
                "namespace": "http://www.pds.eu/vOKO/v0200",
            }
        )
