from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"


@dataclass(frozen=True)
class ProduktType:
    class Meta:
        name = "produktType"

    id: Optional[int] = field(
        default=None,
        metadata={
            "name": "ID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "total_digits": 9,
        }
    )
    nazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "NAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "min_length": 1,
            "max_length": 255,
        }
    )
    mj: Optional[str] = field(
        default=None,
        metadata={
            "name": "MJ",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "min_length": 1,
            "max_length": 20,
        }
    )
