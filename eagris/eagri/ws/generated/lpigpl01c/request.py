from dataclasses import dataclass
from .request_type import RequestType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"


@dataclass(frozen=True)
class Request(RequestType):
    class Meta:
        namespace = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"
