
from .lpi_gpl01_c_process_input import LpiGpl01CProcessInput
from .lpi_gpl01_c_process_output import LpiGpl01CProcessOutput

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


class LpiGpl01CProcess:
    style = "document"
    location = "https://eagri.cz/ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.ashx"
    transport = "http://schemas.xmlsoap.org/soap/http"
    soap_action = "process"
    input = LpiGpl01CProcessInput
    output = LpiGpl01CProcessOutput
