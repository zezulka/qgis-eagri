from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class Uidkey:
    class Meta:
        name = "UIDkey"

    dn: Optional[str] = field(
        default=None,
        metadata={
            "type": "Attribute",
            "required": True,
            "max_length": 50,
        }
    )
    address_ad: Optional[str] = field(
        default=None,
        metadata={
            "name": "addressAD",
            "type": "Attribute",
        }
    )
    certificate_sn: Optional[str] = field(
        default=None,
        metadata={
            "name": "certificateSN",
            "type": "Attribute",
            "max_length": 50,
        }
    )
    certificate_owner: Optional[str] = field(
        default=None,
        metadata={
            "name": "certificateOwner",
            "type": "Attribute",
        }
    )
    certificate_organization: Optional[str] = field(
        default=None,
        metadata={
            "name": "certificateOrganization",
            "type": "Attribute",
        }
    )
    certificate_issuer: Optional[str] = field(
        default=None,
        metadata={
            "name": "certificateIssuer",
            "type": "Attribute",
        }
    )
    email: Optional[str] = field(
        default=None,
        metadata={
            "type": "Attribute",
            "max_length": 100,
        }
    )
