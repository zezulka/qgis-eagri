from dataclasses import dataclass, field
from typing import Optional, Tuple, Union
from xsdata.models.datatype import XmlDate
from .produkt_type import ProduktType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GPL01C"


@dataclass(frozen=True)
class PlodinaType:
    class Meta:
        name = "plodinaType"

    id: Optional[int] = field(
        default=None,
        metadata={
            "name": "ID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "total_digits": 10,
        }
    )
    nazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "NAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "min_length": 0,
            "max_length": 150,
        }
    )
    limitn: Optional[Union[str, int]] = field(
        default=None,
        metadata={
            "name": "LIMITN",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "max_length": 0,
            "total_digits": 5,
        }
    )
    limitn2012: Optional[Union[str, int]] = field(
        default=None,
        metadata={
            "name": "LIMITN2012",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "max_length": 0,
            "total_digits": 5,
        }
    )
    idskupplodin: Optional[Union[str, int]] = field(
        default=None,
        metadata={
            "name": "IDSKUPPLODIN",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "max_length": 0,
            "total_digits": 10,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
        }
    )
    platnostdo: Optional[Union[str, XmlDate]] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
            "max_length": 0,
        }
    )
    datzmeny: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATZMENY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
            "required": True,
        }
    )
    produkt: Tuple[ProduktType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PRODUKT",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GPL01C",
        }
    )
