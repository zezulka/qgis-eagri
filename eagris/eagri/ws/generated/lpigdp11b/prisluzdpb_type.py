from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional
from xsdata.models.datatype import XmlDate

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class PrisluzdpbType:
    class Meta:
        name = "prisluzdpbType"

    fbid: Optional[int] = field(
        default=None,
        metadata={
            "name": "FBID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "total_digits": 10,
        }
    )
    ctverec: Optional[str] = field(
        default=None,
        metadata={
            "name": "CTVEREC",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "min_length": 1,
            "max_length": 8,
        }
    )
    zkod: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "min_length": 1,
            "max_length": 10,
        }
    )
    vymeradpb: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERADPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
