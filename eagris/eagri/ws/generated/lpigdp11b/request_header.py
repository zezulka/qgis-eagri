from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class RequestHeader:
    request_id: Optional[str] = field(
        default=None,
        metadata={
            "name": "RequestID",
            "type": "Element",
            "namespace": "http://www.pds.eu/vOKO/v0200",
            "max_length": 100,
        }
    )
    subject: Optional["RequestHeader.Subject"] = field(
        default=None,
        metadata={
            "name": "Subject",
            "type": "Element",
            "namespace": "http://www.pds.eu/vOKO/v0200",
        }
    )
    request_type: Optional["RequestHeader.RequestType"] = field(
        default=None,
        metadata={
            "name": "RequestType",
            "type": "Element",
            "namespace": "http://www.pds.eu/vOKO/v0200",
        }
    )

    @dataclass(frozen=True)
    class Subject:
        value: str = field(
            default="",
            metadata={
                "required": True,
            }
        )
        subject_id: Optional[str] = field(
            default=None,
            metadata={
                "name": "subjectID",
                "type": "Attribute",
                "required": True,
                "length": 10,
            }
        )

    @dataclass(frozen=True)
    class RequestType:
        value: str = field(
            default="",
            metadata={
                "required": True,
            }
        )
        code: Optional[str] = field(
            default=None,
            metadata={
                "type": "Attribute",
                "required": True,
                "max_length": 15,
            }
        )
