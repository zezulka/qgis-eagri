from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class GeoinfoType:
    class Meta:
        name = "geoinfoType"

    kod: Optional[str] = field(
        default=None,
        metadata={
            "name": "KOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    hodnota: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "HODNOTA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
