from dataclasses import dataclass, field
from typing import Tuple
from .chyba_type import ChybaType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class ChybyType:
    class Meta:
        name = "chybyType"

    chyba: Tuple[ChybaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "CHYBA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "min_occurs": 1,
        }
    )
