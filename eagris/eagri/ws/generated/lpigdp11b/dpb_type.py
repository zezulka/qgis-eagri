from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .aeko_udaje_type import AekoUdajeType
from .apl_pasmo_type import AplPasmoType
from .bpej_type import BpejType
from .chyby_type import ChybyType
from .eroze2g_type import Eroze2GType
from .honitba_type import HonitbaType
from .lfa_type import LfaType
from .mzp_type import MzpType
from .neprod_plochy_type import NeprodPlochyType
from .opatreni_eroze_type import OpatreniErozeType
from .opv_type import OpvType
from .opvz_type import OpvzType
from .prekryv_katuze_type import PrekryvKatuzeType
from .seznam_evp_type import SeznamEvpType
from .stav_id_type import StavIdType
from .upob_type import UpobType
from .uzivatel_type import UzivatelType
from .vynos_hladina_type import VynosHladinaType
from .zakladni_type import ZakladniType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class DpbType:
    class Meta:
        name = "dpbType"

    chyba: Optional[str] = field(
        default=None,
        metadata={
            "name": "CHYBA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    chyby: Optional[ChybyType] = field(
        default=None,
        metadata={
            "name": "CHYBY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    iddpb: Optional[int] = field(
        default=None,
        metadata={
            "name": "IDDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "total_digits": 10,
        }
    )
    ctverec: Optional[str] = field(
        default=None,
        metadata={
            "name": "CTVEREC",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "min_length": 1,
            "max_length": 8,
        }
    )
    zkod: Optional[str] = field(
        default=None,
        metadata={
            "name": "ZKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "min_length": 1,
            "max_length": 10,
        }
    )
    stav: Optional[str] = field(
        default=None,
        metadata={
            "name": "STAV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    stavid: Optional[StavIdType] = field(
        default=None,
        metadata={
            "name": "STAVID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    ucinnost_dle_zakona: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "UCINNOST_DLE_ZAKONA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    geometrie: Optional[str] = field(
        default=None,
        metadata={
            "name": "GEOMETRIE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    platnostod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    platnostdo: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "PLATNOSTDO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    vymera: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    vymeraopv: Optional[Decimal] = field(
        default=None,
        metadata={
            "name": "VYMERAOPV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
            "total_digits": 7,
            "fraction_digits": 2,
        }
    )
    kultura: Optional[str] = field(
        default=None,
        metadata={
            "name": "KULTURA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    kulturaid: Optional[int] = field(
        default=None,
        metadata={
            "name": "KULTURAID",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    kulturanazev: Optional[str] = field(
        default=None,
        metadata={
            "name": "KULTURANAZEV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    kulturaod: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "KULTURAOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    uzivatel: Optional[UzivatelType] = field(
        default=None,
        metadata={
            "name": "UZIVATEL",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    zakladni: Tuple[ZakladniType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "ZAKLADNI",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    prekryvkatuze: Tuple[PrekryvKatuzeType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "PREKRYVKATUZE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    seznamevp: Tuple[SeznamEvpType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "SEZNAMEVP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    mzp: Tuple[MzpType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "MZP",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    lfa: Tuple[LfaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "LFA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    opv: Tuple[OpvType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "OPV",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    aplpasmo: Tuple[AplPasmoType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "APLPASMO",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    vynoshladina: Tuple[VynosHladinaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "VYNOSHLADINA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    bpej: Tuple[BpejType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "BPEJ",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    opatrenieroze: Tuple[OpatreniErozeType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "OPATRENIEROZE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    opvz: Tuple[OpvzType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "OPVZ",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    eroze2_g: Tuple[Eroze2GType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "EROZE2G",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    upob: Tuple[UpobType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "UPOB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    honitba: Tuple[HonitbaType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "HONITBA",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    neprodplochy: Tuple[NeprodPlochyType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "NEPRODPLOCHY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    aekoudaje: Tuple[AekoUdajeType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "AEKOUDAJE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
