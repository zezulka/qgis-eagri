from enum import Enum

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


class TypDataKodType(Enum):
    ZAKLAD = "ZAKLAD"
    KATUZE = "KATUZE"
    EVP = "EVP"
    NS = "NS"
    EROZE = "EROZE"
    LFA = "LFA"
    MZP = "MZP"
    OPVZ = "OPVZ"
    OPV = "OPV"
    BPEJ = "BPEJ"
    ZAKLADMIN = "ZAKLADMIN"
    EROZE2_G = "EROZE2G"
    UPOB = "UPOB"
    HON = "HON"
    NEPRODPLOCHY = "NEPRODPLOCHY"
    AEKO = "AEKO"
