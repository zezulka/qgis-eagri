from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class ChybaType:
    class Meta:
        name = "chybaType"

    kodchyby: Optional[str] = field(
        default=None,
        metadata={
            "name": "KODCHYBY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    popis: Optional[str] = field(
        default=None,
        metadata={
            "name": "POPIS",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
