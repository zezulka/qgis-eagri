from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class OpvType:
    class Meta:
        name = "opvType"

    kodzakres: Optional[str] = field(
        default=None,
        metadata={
            "name": "KODZAKRES",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    geometrie: Optional[str] = field(
        default=None,
        metadata={
            "name": "GEOMETRIE",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
