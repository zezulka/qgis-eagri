from dataclasses import dataclass, field
from typing import Optional

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class PtopatreniType:
    class Meta:
        name = "ptopatreniType"

    kod: Optional[str] = field(
        default=None,
        metadata={
            "name": "KOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
