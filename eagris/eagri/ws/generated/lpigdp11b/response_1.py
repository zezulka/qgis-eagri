from dataclasses import dataclass, field
from typing import Optional
from .response import Response
from .response_header import ResponseHeader
from .srvid import Srvid
from .time_stamp import TimeStamp

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


@dataclass(frozen=True)
class Response1:
    class Meta:
        name = "Response"
        namespace = "http://www.pds.eu/vOKO/v0200"

    srvid: Optional[Srvid] = field(
        default=None,
        metadata={
            "name": "SRVid",
            "type": "Element",
        }
    )
    time_stamp: Optional[TimeStamp] = field(
        default=None,
        metadata={
            "name": "TimeStamp",
            "type": "Element",
        }
    )
    response_header: Optional[ResponseHeader] = field(
        default=None,
        metadata={
            "name": "ResponseHeader",
            "type": "Element",
        }
    )
    response_content: Optional["Response1.ResponseContent"] = field(
        default=None,
        metadata={
            "name": "ResponseContent",
            "type": "Element",
            "required": True,
        }
    )
    v_okoid: Optional[str] = field(
        default=None,
        metadata={
            "name": "vOKOid",
            "type": "Attribute",
            "required": True,
            "max_length": 15,
        }
    )
    sid: Optional[str] = field(
        default=None,
        metadata={
            "name": "SID",
            "type": "Attribute",
            "required": True,
            "max_length": 50,
        }
    )

    @dataclass(frozen=True)
    class ResponseContent:
        response: Optional[Response] = field(
            default=None,
            metadata={
                "name": "Response",
                "type": "Element",
                "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            }
        )
