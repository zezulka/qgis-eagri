
from .lpi_gdp11_b_process_input import LpiGdp11BProcessInput
from .lpi_gdp11_b_process_output import LpiGdp11BProcessOutput

__NAMESPACE__ = "http://www.pds.eu/vOKO/v0200"


class LpiGdp11BProcess:
    style = "document"
    location = "https://eagri.cz/ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.ashx"
    transport = "http://schemas.xmlsoap.org/soap/http"
    soap_action = "process"
    input = LpiGdp11BProcessInput
    output = LpiGdp11BProcessOutput
