from dataclasses import dataclass, field
from typing import Optional
from .typ_data_kod_type import TypDataKodType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class TypDataType:
    class Meta:
        name = "typDataType"

    typdatakod: Optional[TypDataKodType] = field(
        default=None,
        metadata={
            "name": "TYPDATAKOD",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
