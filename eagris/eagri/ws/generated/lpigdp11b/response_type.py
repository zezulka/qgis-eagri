from dataclasses import dataclass, field
from typing import Optional, Tuple
from xsdata.models.datatype import XmlDate
from .dpb_type import DpbType
from .erozni_pozemky_type import ErozniPozemkyType

__NAMESPACE__ = "http://sitewell.cz/lpis/schemas/LPI_GDP11B"


@dataclass(frozen=True)
class ResponseType:
    datzmenydpb: Optional[XmlDate] = field(
        default=None,
        metadata={
            "name": "DATZMENYDPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
            "required": True,
        }
    )
    dpb: Tuple[DpbType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "DPB",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
    erozenipozemky: Tuple[ErozniPozemkyType, ...] = field(
        default_factory=tuple,
        metadata={
            "name": "EROZENIPOZEMKY",
            "type": "Element",
            "namespace": "http://sitewell.cz/lpis/schemas/LPI_GDP11B",
        }
    )
