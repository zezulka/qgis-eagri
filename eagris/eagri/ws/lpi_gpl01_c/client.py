from xsdata.formats.dataclass.parsers import XmlParser

from eagris.common.plugin_constants import DEFAULT_NS
from eagris.common.xml import parse_xml_string
from eagris.eagri.eagri_soap_service_type import EagriSoapServiceType
from eagris.eagri.env import EagriEnv
from eagris.eagri.ws.common.requests import do_send_request, no_auth_request
from eagris.eagri.ws.generated.lpigpl01c import ResponseType


def request(env: EagriEnv = EagriEnv.PROD) -> ResponseType:
    r = parse_xml_string(no_auth_request(
        inner_xml=f"<{DEFAULT_NS}:GETDATA>1</{DEFAULT_NS}:GETDATA>",
        service=EagriSoapServiceType.LPI_GPL01C)
    )
    return XmlParser().from_bytes(do_send_request(r, env, EagriSoapServiceType.LPI_GPL01C))
