from datetime import date

from pydantic.dataclasses import dataclass


@dataclass
class EagriCrop:
    id: int
    name: str
    valid_from: date
    changed: date
