from enum import Enum

from eagris.eagri.namespaces import SERVICE_NS_PREFIX


class EagriSoapServiceType(Enum):
    LPI_GDP11B = 'LPI_GDP11B'
    """
        Lists all available DPB.
        https://eagri.cz/public/portal/mze/farmar/elektronicka-vymena-dat/prehled-vystavenych-sluzeb/LPISPF/lpi-gdp11b.html
    """
    LPI_DDP01B = 'LPI_DDP01B'
    """
        Returns a DPB detail, including agricultural parcels.
        https://eagri.cz/public/portal/mze/farmar/elektronicka-vymena-dat/prehled-vystavenych-sluzeb/LPISPF/lpi-ddp01b.html
    """
    LPI_ZZP01B = 'LPI_ZZP01B'
    """
        Enables user to create or modify an agricultural parcel, including its name, crops and geometry.
        https://eagri.cz/public/portal/mze/farmar/elektronicka-vymena-dat/prehled-vystavenych-sluzeb/LPISPF/lpi-zzp01b
    """
    LPI_GPL01C = "LPI_GPL01C"
    """
        Lists global crop catalogue, i.e. a catalogue of crops common to all eAGRI subjects.
    """

    def namespace(self):
        return "/".join([SERVICE_NS_PREFIX, self.value])
