from datetime import date

from eagris.eagri.ws.lpi_gdp11_b.client import request
from eagris.eagri.ws.lpi_gdp11_b.model.request import DpbListRequest
from eagris.error.exceptions import EagriRequestException
from eagris.model.auth import EagriAuthData


def auth_data_probe(eagri_auth_data: EagriAuthData) -> bool:
    """
    Checks whether eagri_auth_data are valid.

    eAGRI web services do not offer a dedicated endpoint for
    such functionality, therefore the only way to test out
    credentials is to try to call any of the WS.
    """
    try:
        r = request(DpbListRequest(eagri_auth_data, date_from=date.today(), date_to=date.today()))
        return r is not None
    except EagriRequestException:
        return False
