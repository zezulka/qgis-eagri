import hashlib
import string
from enum import Enum


def is_hex(s: str) -> bool:
    return set(s).issubset(string.hexdigits)


class WsKeyHashMethod(Enum):
    """
    Tells what format a WS key is in.
    """
    NONE = 'none'
    """
    No hashing was applied. The key is in the form as received from the Farmer portal.
    """
    SHA1 = 'sha-1'
    """
    WS key is hashed using SHA-1, therefore needs no further modifications for authentication token computation.
    """

    def hex_hash(self, ws_key: str) -> str:
        # import here inside method intentional to prevent cyclic import dependency
        from eagris.error.exceptions import InvalidWsKeyFormatException

        if self == WsKeyHashMethod.NONE:
            return hashlib.sha1(ws_key.encode()).hexdigest()
        elif self == WsKeyHashMethod.SHA1:
            if is_hex(ws_key):
                return ws_key
            else:
                raise InvalidWsKeyFormatException(ws_key, self)
        else:
            raise InvalidWsKeyFormatException(ws_key, self)
