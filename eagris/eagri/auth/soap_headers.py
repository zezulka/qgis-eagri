import base64
import hashlib

from eagris.common.xml import XmlNode, find_xml_subnode, canonicalize_and_serialize_xml

"""
This is the only exception when we use the standard XML API since
defusedxml does not seem to support canonicalization.

Albeit a catch-all library, lxml is not an option since it is a binary.
QGIS and QGIS plugin publishing chaintools prohibit including bins: https://plugins.qgis.org/publish/
"""


# https://eagri.cz/public/web/mze/farmar/elektronicka-vymena-dat/postup-tvorby-hashe/
def compute_authentication_token(request_without_auth_token: XmlNode, hashed_ws_key: str):
    wskey_hash_bin = bytes.fromhex(hashed_ws_key)
    request_body_bin = canonicalize_and_serialize_xml(find_xml_subnode(request_without_auth_token, 'Body'))
    ws_key_and_body_bin = request_body_bin + wskey_hash_bin
    final_hash_bin = hashlib.sha512(ws_key_and_body_bin).digest()
    return base64.b64encode(final_hash_bin).decode()
