from enum import Enum

from eagris.eagri.eagri_soap_service_type import EagriSoapServiceType


class EagriEnv(Enum):
    PROD = 'https://eagri.cz/ssl/nosso-app/EPO/WS/Online/vOKOsrv.aspx'
    TEST = 'https://eagritest.cz/ssl/nosso-app/EPO/WS/Online/vOKOsrv.aspx'

    @staticmethod
    def fromStr(env_name: str):
        if env_name == 'PROD':
            return EagriEnv.PROD
        elif env_name == 'TEST':
            return EagriEnv.TEST
        else:
            raise NotImplementedError


def requestUrl(env: EagriEnv, service: EagriSoapServiceType):
    return f'{env.value}?SERVICEID={service.value}'
