import functools

from eagris.eagri.crop_list.mapper import map_response
from eagris.eagri.ws.lpi_gpl01_c.client import request


@functools.lru_cache(maxsize=None)
def crop_list():
    """
        @functools.cache was introduced in python 3.9, lru_cache is backwards compatible with older versions of Python 3.
    """
    return map_response(request())


def find_crop_by_id(crop_id: int):
    return [c for c in crop_list() if c.id == crop_id][0]
