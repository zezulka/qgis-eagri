from typing import List

from eagris.eagri.ws.generated.lpigpl01c import ResponseType, PlodinaType
from eagris.eagri.ws.lpi_gpl01_c.model.response import EagriCrop


def map_crop(crop: PlodinaType) -> EagriCrop:
    return EagriCrop(
        id=crop.id,
        name=crop.nazev,
        valid_from=crop.platnostod.to_date(),
        changed=crop.datzmeny.to_date()
    )


def map_response(response_type: ResponseType) -> List[EagriCrop]:
    crops = [map_crop(c) for c in response_type.plodina]
    crops.sort(key=lambda c: c.name)
    return crops
