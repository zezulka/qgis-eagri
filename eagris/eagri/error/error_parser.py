from eagris.eagri.namespaces import SOAP_NS
from eagris.eagri.ws.lpi_gdp11_b.model.response import EagriErrorResponse
from eagris.common.xml import find_xml_text, XmlNode, canonicalize_and_serialize_xml
from eagris.error.exceptions import XmlSubnodeNotFoundException


def parse_error_response(response_element: XmlNode) -> EagriErrorResponse:
    try:
        return EagriErrorResponse(
            code=find_xml_text(response_element, 'faultcode', SOAP_NS),
            message=find_xml_text(response_element, 'faultstring', SOAP_NS)
        )
    except XmlSubnodeNotFoundException:
        return EagriErrorResponse(
            code=None,
            message=f"Error message could not be parsed. "
                    f"This very likely means eAGRI services are not available at the moment. "
                    f"Error response: {canonicalize_and_serialize_xml(response_element)}"
        )
