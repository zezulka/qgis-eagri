# flake8: noqa

def classFactory(iface):
    """
    resources_rc.py is generated for us from resources.qrc, load it dynamically on plugin startup.
    """
    from . import resources_rc
    from .mainPlugin import Eagris
    return Eagris(iface)
