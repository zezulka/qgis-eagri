from datetime import date
from datetime import MAXYEAR

__MAX_DATE = date(MAXYEAR, 12, 31)


def intervals_intersect(
        valid_from_l: date,
        valid_to_l: date,  # date | None
        valid_from_r: date,
        valid_to_r: date  # date | None
):
    return (valid_from_l < (valid_to_r if valid_to_r else __MAX_DATE)
            and (valid_to_l if valid_to_l else __MAX_DATE) > valid_from_r)
