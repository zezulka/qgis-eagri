from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidgetAction

from .configuration.cred_settings import is_eagris_plugin_set_up
from eagris.controller.panel.panel import QgisEagriPanel
from eagris.controller.modal.auth_data import QgisEagriAuthDataModal
from .controller.eagriswelcomewizard import QgisEagriWelcomeWizard
from eagris.controller.modal.crops import QgisCropsModal
from .controller.modal.download_prompt import QgisEagriDownloadPrompt


class Eagris:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        self.action = QWidgetAction(self.iface.mainWindow())
        self.panel = None
        self.modal = None
        self.welcomeWizard = QgisEagriWelcomeWizard(
            self.iface,
            lambda: self.__init_eagris_components()
        )

    def initGui(self):
        """
        Do not set this function name to init_gui as QGIS expects camelCase.
        """
        if is_eagris_plugin_set_up():
            self.__init_eagris_components()
        else:
            self.welcomeWizard.show()

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu("eagris", self.action)
        self.iface.removeDockWidget(self.panel)

    def eagri(self):
        self.modal.show()
        self.modal.adjustSize()

    def __init_eagris_components(self):
        print("eagris plugin is set up, loading components...")
        self.panel = QgisEagriPanel(
            self.iface,
            lambda: self.modal.show(),
            lambda: self.crop_modal.show(),
            lambda: self.download_prompt.show()
        )
        self.download_prompt = QgisEagriDownloadPrompt(self.iface, self.panel)
        self.crop_modal = QgisCropsModal(self.panel)
        self.modal = QgisEagriAuthDataModal(self.iface, self.panel)
        self.action.setDefaultWidget(self.panel)
        # Register the panel with the QGIS interface
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.panel)
