from typing import List

from eagris.auth.auth_settings import AuthCredSettings
from eagris.auth.sqlite.utils import create_session, execute_sqlite_statement, execute_sqlite_valued_statement
from eagris.common.plugin_constants import DEFAULT_ENV
from eagris.eagri.env import EagriEnv
from eagris.model.auth import EagriAuthData


def delete(settings: AuthCredSettings, auth_data: EagriAuthData):
    session = create_session(settings.authCredsPath)
    execute_sqlite_statement(
        f"""
        DELETE FROM \"{settings.tableName}\"
        WHERE
            "{settings.wsKeyField}"='{auth_data.wskey}'
            AND "{settings.szridField}"='{auth_data.szrid}'
            AND "{settings.loginField}"='{auth_data.login}';
        """,
        session
    )


def insert(settings: AuthCredSettings, auth_data: EagriAuthData):
    session = create_session(settings.authCredsPath)
    execute_sqlite_statement(
        f"""
        INSERT INTO \"{settings.tableName}\"(
            "{settings.loginField}", "{settings.wsKeyField}", "{settings.szridField}"
            {__append_optional_fields(settings)}
        )
        VALUES (
            '{auth_data.login}', '{auth_data.wskey}', '{auth_data.szrid}'
            {__append_optional_values(settings, auth_data)}
        );
        """,
        session
    )


def find_all(settings: AuthCredSettings) -> List[EagriAuthData]:
    session = create_session(settings.authCredsPath)
    return execute_sqlite_valued_statement(
        settings,
        __sqlite_row_to_auth_data,
        f"""
        SELECT
            \"{settings.loginField}\", \"{settings.szridField}\", \"{settings.wsKeyField}\"
            {__append_optional_fields(settings)}
        FROM \"{settings.tableName}\";
        """,
        session
    )


def __append_optional_fields(settings: AuthCredSettings):
    return f"""
            {f', "{settings.nameField}"' if settings.nameField else ''}
            {f', "{settings.envField}"' if settings.envField else ''}
            """


def __append_optional_values(settings: AuthCredSettings, auth_data: EagriAuthData):
    return f"""
            {f", '{auth_data.name}'" if settings.nameField else ''}
            {f", '{auth_data.env.name}'" if settings.envField else ''}
            """


def __sqlite_row_to_auth_data(settings: AuthCredSettings, tup) -> EagriAuthData:
    login = tup[settings.loginField]
    wskey = tup[settings.wsKeyField]
    szrid = tup[settings.szridField]
    name = tup[settings.nameField] if settings.nameField else None
    env = EagriEnv.fromStr(tup[settings.envField]) if settings.envField else DEFAULT_ENV
    return EagriAuthData(
        login=login,
        wskey=wskey,
        szrid=szrid,
        name=name.strip(),
        env=env
    )
