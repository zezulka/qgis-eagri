import sqlite3
from sqlite3 import Connection
from typing import TypeVar, Callable, Any, List

from ...auth.auth_settings import AuthCredSettings

V = TypeVar("V")


def execute_sqlite_statement(sql: str, conn: Connection):
    try:
        result = conn.execute(sql)
        conn.commit()
        return result
    except Exception as e:
        conn.rollback()
        raise e


def execute_sqlite_valued_statement(
        auth_settings: AuthCredSettings,
        mapper: Callable[[AuthCredSettings, Any], V],
        sql: str,
        conn: Connection
) -> List[V]:
    try:
        result = conn.execute(sql)
        mapped_result = [mapper(auth_settings, row) for row in result.fetchall()]
        conn.commit()
        return mapped_result
    except Exception as e:
        conn.rollback()
        raise e


def create_session(db_path: str):
    conn = sqlite3.connect(db_path)
    conn.row_factory = sqlite3.Row
    return conn
