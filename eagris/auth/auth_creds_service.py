from PyQt5.QtCore import qDebug

from eagris.configuration.cred_settings import load_settings
from eagris.model.auth import EagriAuthData
from eagris.auth.sqlite.auth_creds_dao import delete as sqliteDelete, insert as sqliteInsert, find_all as sqliteFindAll


def delete(auth_data: EagriAuthData):
    qDebug(f"Deleting auth data {str(auth_data.szrid)} using SQLite.")
    sqliteDelete(load_settings(), auth_data)


def insert(auth_data: EagriAuthData):
    qDebug(f"Inserting new auth data {str(auth_data.szrid)} using SQLite.")
    sqliteInsert(load_settings(), auth_data)


def find_all():
    qDebug("Fetching auth data using SQLite.")
    return sqliteFindAll(load_settings())
