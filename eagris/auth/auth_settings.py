from pydantic.dataclasses import dataclass
from typing import Optional


@dataclass
class AuthCredSettings:
    authCredsPath: str
    tableName: str
    loginField: str
    szridField: str
    wsKeyField: str
    nameField: Optional[str]
    envField: Optional[str]
