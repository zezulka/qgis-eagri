from pydantic.dataclasses import dataclass

from eagris.eagri.env import EagriEnv


@dataclass
class EagriModalData:
    login: str
    ws_key: str
    szrid: str
    name: str
    env: EagriEnv

    # support data class unpacking
    def __iter__(self): return iter((self.login, self.ws_key, self.szrid, self.name, self.env))
