from pydantic.dataclasses import dataclass
from typing import Optional

from eagris.eagri.env import EagriEnv


@dataclass
class EagriAuthData:
    login: str
    wskey: str
    szrid: str
    name: Optional[str]
    env: EagriEnv

    # support data class unpacking
    def __iter__(self): return iter((self.login, self.wskey, self.szrid, self.name, self.env))

    # return any useful identifier of the auth data, tailored especially for logging
    def identifier(self):
        return self.name if self.name else self.szrid


def logically_equal(first: EagriAuthData, second: EagriAuthData):
    return first.env == second.env and \
        first.login == second.login and \
        first.szrid == second.szrid
