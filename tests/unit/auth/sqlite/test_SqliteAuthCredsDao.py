import pytest

from eagris.auth.sqlite.auth_creds_dao import insert, delete, find_all
from eagris.auth.sqlite.utils import create_session, execute_sqlite_statement
from eagris.eagri.env import EagriEnv
from eagris.model.auth import EagriAuthData
from ...fixtures import auth_settings, auth_data, sqlite_table_setup


@pytest.fixture
def session(auth_settings):
    s = create_session(auth_settings.authCredsPath)
    yield s
    s.close()


def test_findAllShouldFindEntries(sqlite_table_setup, auth_data, auth_settings):
    insert(auth_settings, auth_data)
    assert find_all(auth_settings) == [auth_data]


def test_deleteExistingEntry(sqlite_table_setup, auth_data, auth_settings):
    insert(auth_settings, auth_data)
    delete(auth_settings, auth_data)
    assert find_all(auth_settings) == []
