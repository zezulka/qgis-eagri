import pytest

from eagris.common.xml import parse_xml_string
from eagris.eagri.error.error_parser import parse_error_response
from eagris.eagri.ws.lpi_gdp11_b.model.response import EagriErrorResponse


@pytest.fixture
def error_response():
    with open("resources/error_response.xml") as response:
        yield response


def test_mapError(error_response):
    mocked_error = parse_xml_string(error_response.read())
    assert parse_error_response(mocked_error) == EagriErrorResponse(
        code="X220061",
        message="Definovany identifikator uzivatele v uvedene autorizacni databazi neexistuje."
    )


def test_mapErrorFallback():
    mocked_error = parse_xml_string("<a></a>")
    assert parse_error_response(mocked_error) == EagriErrorResponse(
        code=None,
        message="Error message could not be parsed. This very likely means eAGRI services are not available at the moment. Error response: b'<a></a>'"
    )
