from eagris.common.xml import serialize_xml
from eagris.eagri.ws.generated.lpiddp01b import TypDataKodType
from eagris.eagri.ws.lpi_ddp01_b.client import lpiddp01bRequest


def test_shouldPassAllParametersToRequestString():
    login = "99myfarm"
    szrid = "100005432"
    iddpb = 1234
    assert serialize_xml(lpiddp01bRequest(
        login,
        szrid,
        iddpb,
        data_types=[TypDataKodType.ZAKLAD, TypDataKodType.ZEMPARCELY, TypDataKodType.AEKO]
    )) == b'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><vOKO-wss:Token xmlns:vOKO-wss="http://www.pds.eu/vOKO/v0200/wss" type="A01"/></soap:Header><soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><Request xmlns="http://www.pds.eu/vOKO/v0200" xmlns:ns2="http://sitewell.cz/lpis/schemas/LPI_DDP01B" vOKOid="LPI_DDP01B"><UIDkey addressAD="default" dn="99myfarm"/><RequestHeader><Subject subjectID="100005432"/></RequestHeader><RequestContent><ns2:Request><ns2:IDDPB>1234</ns2:IDDPB><ns2:TYPDATA><ns2:TYPDATAKOD>ZAKLAD</ns2:TYPDATAKOD></ns2:TYPDATA><ns2:TYPDATA><ns2:TYPDATAKOD>ZEMPARCELY</ns2:TYPDATAKOD></ns2:TYPDATA><ns2:TYPDATA><ns2:TYPDATAKOD>AEKO</ns2:TYPDATAKOD></ns2:TYPDATA></ns2:Request></RequestContent></Request></soap:Body></soap:Envelope>'
