from ...fixtures import lpigdb11b_request

from eagris.common.xml import parse_xml_string
from eagris.eagri.auth.soap_headers import compute_authentication_token


def test_computeAuthenticationToken(lpigdb11b_request):
    assert compute_authentication_token(parse_xml_string(lpigdb11b_request.read()), hashed_ws_key="cafebabe") == \
           "JTO1X947LhDQ9PfDednGl2251GPVkEUPD/1RXv5B5KGtRJiQBtdPSOW2UfcpQKNUccrYQO87Kx3LPAIydARo9g=="
