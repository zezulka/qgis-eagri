from datetime import date

import pytest
from requests import Response

from eagris.eagri.dpb_list.service import dpb_list
from eagris.model.auth import EagriAuthData
from ...fixtures import auth_data, lpi_gdb11b_response
from ...xml_utils import wrapResponseXmlWithSoapElements


@pytest.fixture
def mocked_response(lpi_gdb11b_response):
    r = Response()
    r.status_code = 200
    r._content = bytes(wrapResponseXmlWithSoapElements(lpi_gdb11b_response.read()), encoding="UTF-8")
    return r


def test_shouldRetrieveResponseFromRequestsLib(mocker, mocked_response: Response, auth_data: EagriAuthData):
    """
    Mainly deals with logic in __sendRequest. Mapping itself already covered by other tests.
    """
    mocker.patch('eagris.eagri.ws.common.requests.post', return_value=mocked_response)

    response = dpb_list(auth_data, validity_date=date(2023, 5, 14))
    assert response.changed == date(2023, 5, 14)
    assert len(response.blocks) == 2