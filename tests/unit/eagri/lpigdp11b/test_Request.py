from datetime import date

from eagris.common.xml import serialize_xml
from eagris.eagri.ws.lpi_gdp11_b.client import lpiGdp11bRequest


def test_shouldPassValidityRangeToRequestString():
    date_from = date(2023, 3, 3)
    date_to = date(2023, 6, 6)
    login = "99myfarm"
    szrid = "100005432"
    assert serialize_xml(lpiGdp11bRequest(login, szrid, date_from, date_to)) == b'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><vOKO-wss:Token xmlns:vOKO-wss="http://www.pds.eu/vOKO/v0200/wss" type="A01"/></soap:Header><soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><Request xmlns="http://www.pds.eu/vOKO/v0200" xmlns:ns2="http://sitewell.cz/lpis/schemas/LPI_GDP11B" vOKOid="LPI_GDP11B"><UIDkey addressAD="default" dn="99myfarm"/><RequestHeader><Subject subjectID="100005432"/></RequestHeader><RequestContent><ns2:Request><ns2:GETDATA>true</ns2:GETDATA><ns2:DATOD>2023-03-03</ns2:DATOD><ns2:DATDO>2023-06-06</ns2:DATDO><ns2:TYPDATA><ns2:TYPDATAKOD>ZAKLADMIN</ns2:TYPDATAKOD></ns2:TYPDATA></ns2:Request></RequestContent></Request></soap:Body></soap:Envelope>'
