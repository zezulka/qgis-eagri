import pytest as pytest

from xsdata.formats.dataclass.parsers import XmlParser
from eagris.eagri.parcel.management.mapper import map_creation_response
from eagris.eagri.ws.generated.lpizzp01b import ResponseType
from eagris.eagri.ws.lpi_zzp01_b.model.response import ParcelsCreationResponse, ParcelCreationResponse, ParcelErrorCode


@pytest.fixture
def lpi_zzp01b_response_ok():
    with open("resources/lpizzp01b/response_ok.xml") as r:
        yield r


@pytest.fixture
def lpi_zzp01b_response_nok():
    with open("resources/lpizzp01b/response_nok.xml") as r:
        yield r


def test_mapWholeResponseOk(lpi_zzp01b_response_ok):
    mocked_ok = XmlParser().from_string(lpi_zzp01b_response_ok.read(), ResponseType)
    assert map_creation_response(mocked_ok) == ParcelsCreationResponse(
        new_parcels=[
            ParcelCreationResponse(success=True, error_code=None, id=857028),
            ParcelCreationResponse(success=True, error_code=None, id=857029)
        ]
    )


def test_mapWholeResponseNok(lpi_zzp01b_response_nok):
    mocked_nok = XmlParser().from_string(lpi_zzp01b_response_nok.read(), ResponseType)
    assert map_creation_response(mocked_nok) == ParcelsCreationResponse(
        new_parcels=[
            ParcelCreationResponse(success=True, error_code=None, id=857028),
            ParcelCreationResponse(success=False, error_code=ParcelErrorCode.E01, id=857029)
        ]
    )
