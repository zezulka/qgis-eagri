import datetime

import pytest
from xsdata.formats.dataclass.parsers import XmlParser

from eagris.eagri.crop_list.mapper import map_response
from eagris.eagri.ws.generated.lpigpl01c import ResponseType
from eagris.eagri.ws.lpi_gpl01_c.model.response import EagriCrop


@pytest.fixture
def lpi_gpl01c_response_dpb():
    with open("resources/lpigpl01c/response_ok.xml") as response:
        yield response


def test_mappedResponseShouldBeSortedByNameByDefault(lpi_gpl01c_response_dpb):
    response = XmlParser().from_string(lpi_gpl01c_response_dpb.read(), ResponseType)
    assert map_response(response) == [
        EagriCrop(id=223, name='Česnek', valid_from=datetime.date(2000, 1, 1), changed=datetime.date(2022, 7, 22)),
        EagriCrop(id=361, name='Česnek jarní', valid_from=datetime.date(2000, 1, 1),
                  changed=datetime.date(2023, 2, 27)),
        EagriCrop(id=416, name='Šalotka', valid_from=datetime.date(2000, 1, 1), changed=datetime.date(2023, 2, 28)),
    ]
