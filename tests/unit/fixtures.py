import pytest

from eagris.auth.auth_settings import AuthCredSettings
from eagris.auth.sqlite.utils import execute_sqlite_statement
from eagris.eagri.env import EagriEnv
from eagris.model.auth import EagriAuthData


@pytest.fixture
def auth_data():
    return EagriAuthData(
        login="99testfarm",
        wskey="cafebabe",
        szrid="100099999",
        name="My Farm",
        env=EagriEnv.PROD
    )


@pytest.fixture
def lpi_gdb11b_response():
    with open("resources/lpigdb11b/response.xml") as response:
        yield response


@pytest.fixture
def lpigdb11b_request():
    with open("resources/lpigdb11b/request.xml") as request:
        yield request


@pytest.fixture
def auth_settings():
    return AuthCredSettings(
        # create an in-memory database
        authCredsPath="file:memory?mode=memory&cache=shared",
        tableName="lpis_auth_data",
        loginField="login",
        szridField="szrid",
        wsKeyField="wskey",
        nameField="name",
        envField="env"
    )


@pytest.fixture
def sqlite_table_setup(session, auth_settings):
    execute_sqlite_statement(
        f"""
            CREATE TABLE IF NOT EXISTS {auth_settings.tableName}(
                {auth_settings.loginField} text NOT NULL,
                {auth_settings.szridField} text NOT NULL,
                {auth_settings.wsKeyField} text NOT NULL,
                {auth_settings.nameField} text NULL,
                {auth_settings.envField} text NOT NULL
            );
            """,
        session
    )
    yield auth_settings
    execute_sqlite_statement(
        f"""DROP TABLE {auth_settings.tableName}""",
        session
    )
