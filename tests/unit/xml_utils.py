def wrapResponseXmlWithSoapElements(response: str):
    return f"""
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope/" soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding">
            <soap:Body>
            {response}
            </soap:Body>
        </soap:Envelope>    
    """
