from eagris.common.download_vector_feature import map_to_eagris_vector_layers, DownloadVectorFeature
from eagris.common.vector_layer_type import VectorLayerType


def test_allFeaturesShouldBeMappedToUniqueVectorLayers():
    assert (
            map_to_eagris_vector_layers([f for f in DownloadVectorFeature]) ==
            set([layer for layer in VectorLayerType])
    )


def test_baseParcelLayerShouldBeAddedWhenAnyParcelFeatureIsRequested():
    assert (list(map_to_eagris_vector_layers([DownloadVectorFeature.AGRICULTURAL_PARCEL_CROP]))
            .index(VectorLayerType.AGRICULTURAL_PARCEL_PARCEL) >= 0)

    assert (list(map_to_eagris_vector_layers([DownloadVectorFeature.AGRICULTURAL_PARCEL_VERSION]))
            .index(VectorLayerType.AGRICULTURAL_PARCEL_PARCEL) >= 0)

def test_noFeatures():
    assert map_to_eagris_vector_layers([]) == set()