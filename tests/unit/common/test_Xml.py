import pytest

from ..fixtures import lpigdb11b_request

from eagris.common.xml import parse_xml_string, find_xml_subnode, serialize_xml, canonicalize_and_serialize_xml
from eagris.eagri.namespaces import SOAP_NS
from eagris.error.exceptions import XmlSubnodeNotFoundException


def test_throwsEagriExceptionIfSubmoduleNotFound(lpigdb11b_request):
    request = parse_xml_string(lpigdb11b_request.read())
    with pytest.raises(XmlSubnodeNotFoundException):
        find_xml_subnode(request, 'MissingElement', SOAP_NS)


def test_serialization(lpigdb11b_request):
    request = parse_xml_string(lpigdb11b_request.read())
    print(serialize_xml(find_xml_subnode(request, 'Body', SOAP_NS)))
    assert serialize_xml(find_xml_subnode(request, 'Body', SOAP_NS)) == \
           b'<soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\n        <Request xmlns="http://www.pds.eu/vOKO/v0200" vOKOid="LPI_GDP11B">\n            <UIDkey addressAD="default" dn="99testfarm"/>\n            <RequestHeader>\n                <Subject subjectID="1000099999"/>\n            </RequestHeader>\n            <RequestContent>\n                <lpigdp11b:Request xmlns:lpigdp11b="http://sitewell.cz/lpis/schemas/LPI_GDP11B">\n                    <lpigdp11b:GETDATA>true</lpigdp11b:GETDATA>\n                    <lpigdp11b:DATOD>2023-04-22</lpigdp11b:DATOD>\n                    <lpigdp11b:DATDO>2023-04-23</lpigdp11b:DATDO>\n                    <lpigdp11b:TYPDATA>\n                        <lpigdp11b:TYPDATAKOD>ZAKLADMIN</lpigdp11b:TYPDATAKOD>\n                    </lpigdp11b:TYPDATA>\n                </lpigdp11b:Request>\n            </RequestContent>\n        </Request>\n    </soap:Body>\n'


def test_c14n(lpigdb11b_request):
    request = parse_xml_string(lpigdb11b_request.read())
    print(canonicalize_and_serialize_xml(find_xml_subnode(request, 'Body', SOAP_NS)))
    assert canonicalize_and_serialize_xml(find_xml_subnode(request, 'Body', SOAP_NS)) == \
           b'<soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\n        <Request xmlns="http://www.pds.eu/vOKO/v0200" vOKOid="LPI_GDP11B">\n            <UIDkey addressAD="default" dn="99testfarm"></UIDkey>\n            <RequestHeader>\n                <Subject subjectID="1000099999"></Subject>\n            </RequestHeader>\n            <RequestContent>\n                <lpigdp11b:Request xmlns:lpigdp11b="http://sitewell.cz/lpis/schemas/LPI_GDP11B">\n                    <lpigdp11b:GETDATA>true</lpigdp11b:GETDATA>\n                    <lpigdp11b:DATOD>2023-04-22</lpigdp11b:DATOD>\n                    <lpigdp11b:DATDO>2023-04-23</lpigdp11b:DATDO>\n                    <lpigdp11b:TYPDATA>\n                        <lpigdp11b:TYPDATAKOD>ZAKLADMIN</lpigdp11b:TYPDATAKOD>\n                    </lpigdp11b:TYPDATA>\n                </lpigdp11b:Request>\n            </RequestContent>\n        </Request>\n    </soap:Body>'
