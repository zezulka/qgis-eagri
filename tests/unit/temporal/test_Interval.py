from eagris.temporal.interval import intervals_intersect
from datetime import date


def test_no_overlaps():
    assert not intervals_intersect(
        valid_from_l=date(2022, 3, 1),
        valid_to_l=date(2023, 5, 1),
        valid_from_r=date(2023, 5, 2),
        valid_to_r=date(2023, 6, 1)
    )


def test_no_overlaps_inverted_order():
    assert not intervals_intersect(
        valid_from_r=date(2023, 5, 2),
        valid_to_r=date(2023, 6, 1),
        valid_from_l=date(2022, 3, 1),
        valid_to_l=date(2023, 5, 1)
    )


def test_no_overlaps_unbounded_right():
    assert not intervals_intersect(
        valid_from_l=date(2022, 3, 1),
        valid_to_l=date(2023, 5, 1),
        valid_from_r=date(2023, 5, 2),
        valid_to_r=None
    )


def test_overlaps_unbounded_left():
    assert intervals_intersect(
        valid_from_l=date(2022, 3, 1),
        valid_to_l=None,
        valid_from_r=date(2023, 5, 2),
        valid_to_r=date(2023, 6, 1)
    )


def test_overlaps_unbounded_both():
    assert intervals_intersect(
        valid_from_l=date(2022, 3, 1),
        valid_to_l=None,
        valid_from_r=date(2023, 5, 2),
        valid_to_r=None
    )
