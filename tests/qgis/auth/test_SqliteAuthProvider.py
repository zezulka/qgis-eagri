import pytest
from PyQt5.QtCore import Qt

from eagris.auth.auth_creds_service import find_all
from eagris.eagri.env import EagriEnv
from eagris.mainPlugin import Eagris
from eagris.model.auth import EagriAuthData
from ..conftest import auth_settings, modal_with_data, eagris, session, mocked_eagri_auth_data, sqlite_table_setup, tree_with_one_auth_item


def test_authDataArePersistedIfTheyAreCorrect(mocker, qtbot, modal_with_data, sqlite_table_setup, eagris: Eagris):
    mocker.patch('eagris.controller.worker.add_auth_data.auth_data_probe', return_value=True)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0

    assert find_all() == []

    qtbot.mouseClick(eagris.modal.addAuthBtn, Qt.LeftButton)
    qtbot.waitUntil(lambda: eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1)

    assert find_all() == [EagriAuthData(
        login="99testfarm",
        wskey="4db6cf8d1d9949790c7e836f29f12dc37c15b3a9",
        szrid="100004321",
        name='Abernathy Farm, co.',
        env=EagriEnv.PROD
    )]


def test_authDataAreRemovedOnceUserPressesDelete(qtbot, tree_with_one_auth_item, sqlite_table_setup, eagris: Eagris):
    assert find_all() == [EagriAuthData(
        login="99testfarm",
        wskey="cafebabe",
        szrid="100004321",
        name='Abernathy Farm, co.',
        env=EagriEnv.TEST
    )]

    eagris.panel.eagriUsersTreeWidget.setCurrentItem(eagris.panel.eagriUsersTreeWidget.topLevelItem(0))
    qtbot.mouseClick(
        eagris.panel.deleteButton,
        Qt.LeftButton
    )

    assert find_all() == []
