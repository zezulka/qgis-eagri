from datetime import date

from PyQt5.QtWidgets import QLineEdit, QTreeWidget, QLabel
from qgis.core import QgsFeature, QgsProject, QgsVectorLayer

from eagris.common.vector_layer_type import VectorLayerType
from eagris.controller.panel.update_handler import ParcelDetailUpdateHandler
from eagris.controller.vector_layer import update_qgis_layer, create_qgis_layer
from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelVersion
from eagris.eagri.ws.lpi_gpl01_c.model.response import EagriCrop
from eagris.qgis.feature_mapper import parcel_version_to_feature
from tests.qgis.conftest import sqlite_table_setup, functionfixture

"""
Abandon the idea of writing integration test for this component. It would result in a very hacky test suite, anyway.

If we were to strictly use what fixtures pyqt test give us, the following errors start to occur:

    RuntimeError: wrapped C/C++ object of type QgsVectorLayer has been deleted
    
At least test the component itself (in the context of QGIS).
"""


@functionfixture
def parcel_line_edit():
    return QLineEdit()


@functionfixture
def tree_widget():
    return QTreeWidget()


@functionfixture
def parcel_id_label():
    return QLabel()


@functionfixture
def block_id_label():
    return QLabel()


@functionfixture
def parcel_version_layer(mocked_eagri_auth_data) -> QgsVectorLayer:
    return create_qgis_layer(
        layer_type=VectorLayerType.AGRICULTURAL_PARCEL_VERSION,
        eagri_auth_data=mocked_eagri_auth_data,
        validity=date(2023, 5, 12)
    )


@functionfixture
def update_handler(parcel_line_edit, tree_widget, parcel_id_label, block_id_label):
    return ParcelDetailUpdateHandler(
        parcel_name_edit=parcel_line_edit,
        parcel_detail_crops=tree_widget,
        parcel_id_label=parcel_id_label,
        block_id_label=block_id_label
    )


@functionfixture
def parcel_version_feature(parcel_id) -> QgsFeature:
    return parcel_version_to_feature(
        parcel_id=parcel_id,
        parcel_version=ParcelVersion(
            id=1,
            geometry="Polygon ((-806921.85270000004675239 -1059064.13739999989047647, -806927.98580000002402812 -1059033.70650000008754432, -806928.05240000004414469 -1059033.46650000009685755, -806946.09999999997671694 -1059035.69999999995343387, -806976.09999999997671694 -1059036.5, -807010.09999999997671694 -1059035.19999999995343387, -807016.40000000002328306 -1059037.39999999990686774, -807018.40099999995436519 -1059042.69999999995343387, -807012.90000000002328306 -1059058.10000000009313226, -807004.40000000002328306 -1059072.10000000009313226, -807000.60530000005383044 -1059078.50139999995008111, -806995.31359999999403954 -1059084.85150000010617077, -806989.66920000000391155 -1059087.14449999993667006, -806980.14410000003408641 -1059088.02649999991990626, -806969.88850000000093132 -1059085.49259999999776483, -806955.8628000000026077 -1059081.17540000006556511, -806931.79890000005252659 -1059070.83009999990463257, -806921.85270000004675239 -1059064.13739999989047647))",
            valid_from=date(2022, 5, 6),
            valid_to=date(2023, 5, 14),
            area=0.38897826
        )
    )


@functionfixture
def parcel_crop_layer(mocked_eagri_auth_data) -> QgsVectorLayer:
    return create_qgis_layer(
        layer_type=VectorLayerType.AGRICULTURAL_PARCEL_CROP,
        eagri_auth_data=mocked_eagri_auth_data,
        validity=date(2023, 5, 12)
    )


class TestShouldNotSwitchToParcelDetail:
    def testShouldFilterLayersWhichAreNotParcelVersions(
            self,
            sqlite_table_setup,
            tree_widget: QTreeWidget,
            update_handler: ParcelDetailUpdateHandler
    ):
        empty_vector_layer = QgsVectorLayer()
        # vector layer name should not determine behaviour of ParcelDetailUpdateHandler
        empty_vector_layer.setName(VectorLayerType.AGRICULTURAL_PARCEL_VERSION.layer_name())

        update_handler.update_parcel_detail(empty_vector_layer)

        assert tree_widget.topLevelItemCount() == 0

    def testWhenUserInteractsWithParcelVersionLayerWhichHasNoFeatures(
            self,
            sqlite_table_setup,
            tree_widget: QTreeWidget,
            update_handler: ParcelDetailUpdateHandler,
            parcel_version_layer: QgsVectorLayer
    ):
        update_handler.update_parcel_detail(parcel_version_layer)

        assert tree_widget.topLevelItemCount() == 0

    def testWhenUserInteractsWithParcelVersionLayerWhichHasNoSelectedFeatures(
            self,
            sqlite_table_setup,
            tree_widget: QTreeWidget,
            update_handler: ParcelDetailUpdateHandler,
            parcel_version_layer: QgsVectorLayer,
            parcel_version_feature: QgsFeature
    ):
        parcel_version_layer.addFeatures([parcel_version_feature])

        update_handler.update_parcel_detail(parcel_version_layer)

        assert tree_widget.topLevelItemCount() == 0

    def testWhenUserInteractsWithParcelVersionLayerWhichHasSelectedFeaturesButNoAssociatedCropLayer(
            self,
            sqlite_table_setup,
            tree_widget: QTreeWidget,
            update_handler: ParcelDetailUpdateHandler,
            parcel_version_layer: QgsVectorLayer,
            parcel_version_feature: QgsFeature
    ):
        update_qgis_layer(parcel_version_layer, [parcel_version_feature])
        parcel_version_layer.selectAll()

        update_handler.update_parcel_detail(parcel_version_layer)

        assert tree_widget.topLevelItemCount() == 0


def testShouldSwitchToParcelDetailWhenUserInteractsWithParcelVersionLayerWhichHasSelectedFeaturesAndAssociatedEmptyCropLayer(
        sqlite_table_setup,
        update_handler: ParcelDetailUpdateHandler,
        tree_widget: QTreeWidget,
        parcel_line_edit: QLineEdit,
        parcel_version_layer: QgsVectorLayer,
        parcel_version_feature: QgsFeature,
        parcel_crop_layer: QgsVectorLayer
):
    __insert_crop_and_parcel_version_to_qgis([], parcel_crop_layer,
                                             parcel_version_feature, parcel_version_layer)
    update_handler.update_parcel_detail(parcel_version_layer)

    assert parcel_line_edit.text() == ""

    assert tree_widget.topLevelItemCount() == 0


def testShouldSwitchToParcelDetailWhenUserInteractsWithParcelVersionLayerWhichHasSelectedFeaturesAndAssociatedNonemptyCropLayer(
        sqlite_table_setup,
        tree_widget: QTreeWidget,
        update_handler: ParcelDetailUpdateHandler,
        parcel_version_layer: QgsVectorLayer,
        parcel_crop_layer: QgsVectorLayer,
        parcel_version_feature: QgsFeature,
        parcel_crop_feature_corn: QgsFeature,
        parcel_crop_feature_barley: QgsFeature,
        mocker
):
    __insert_crop_and_parcel_version_to_qgis([parcel_crop_feature_corn, parcel_crop_feature_barley], parcel_crop_layer,
                                             parcel_version_feature, parcel_version_layer)
    proj = QgsProject()
    proj.addMapLayers([parcel_crop_layer, parcel_version_layer])

    mocker.patch('eagris.controller.panel.update_handler.QgsProject.instance', return_value=proj)
    mocker.patch(
        'eagris.qgis.feature_mapper.find_crop_by_id',
        return_value=EagriCrop(
            id=99,
            name='Kuk',
            valid_from=date(2020, 1, 1),
            changed=date(2023, 4, 20)
        )
    )
    update_handler.update_parcel_detail(parcel_version_layer)

    corn, barley = [
        [tree_widget.topLevelItem(i).text(te) for te in range(tree_widget.topLevelItem(i).columnCount())] for i in
        range(tree_widget.topLevelItemCount())
    ]
    assert corn == ['99', 'Kuk', '2022-05-06', '2023-04-27']
    assert barley == ['98', 'Kuk', '2023-07-01', '']


def __insert_crop_and_parcel_version_to_qgis(
        crop_features,
        parcel_crop_layer,
        parcel_version_feature,
        parcel_version_layer
):
    update_qgis_layer(parcel_version_layer, [parcel_version_feature])
    update_qgis_layer(parcel_crop_layer, crop_features)
    parcel_version_layer.selectAll()
