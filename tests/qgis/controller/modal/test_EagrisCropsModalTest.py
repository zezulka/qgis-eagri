from datetime import date

from PyQt5.QtCore import QDate, Qt
from PyQt5.QtWidgets import QTableWidget, QDialogButtonBox
from qgis.core import QgsVectorLayer

from eagris.common.qt_constants import NO_ITEM_SELECTED_INDEX
from eagris.controller.panel.crops import table_widget_crops
from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelCrop
from eagris.eagri.ws.lpi_gpl01_c.model.response import EagriCrop
from eagris.mainPlugin import Eagris
from eagris.qgis.feature_mapper import crop_feature_to_tree_item
from tests.qgis.conftest import functionfixture, eagri_crops


@functionfixture
def crops_widget_with_two_crops(
        mocker,
        eagris: Eagris,
        parcel_crop_feature_corn: QgsVectorLayer,
        parcel_crop_feature_barley: QgsVectorLayer
):
    crops: QTableWidget = eagris.panel.parcelDetailCrops
    mocker.patch(
        'eagris.qgis.feature_mapper.find_crop_by_id',
        return_value=EagriCrop(
            id=99,
            name='Kuk',
            valid_from=date(2020, 1, 1),
            changed=date(2023, 4, 20)
        )
    )
    crops.addTopLevelItem(crop_feature_to_tree_item(parcel_crop_feature_corn))
    crops.addTopLevelItem(crop_feature_to_tree_item(parcel_crop_feature_barley))
    yield
    crops.clear()


@functionfixture
def reset_crop_modal(eagris: Eagris, crops_widget_with_two_crops):
    eagris.crop_modal.dateFromEdit.clear()
    eagris.crop_modal.dateToEdit.clear()
    eagris.crop_modal.cropsComboBox.setCurrentIndex(-1)
    yield
    eagris.crop_modal.dateFromEdit.clear()
    eagris.crop_modal.dateToEdit.clear()
    eagris.crop_modal.cropsComboBox.setCurrentIndex(-1)
    eagris.crop_modal.hide()


def test_cropsShouldBePopulatedIfEmpty(mocker, sqlite_table_setup, eagris: Eagris, reset_crop_modal, eagri_crops):
    mocker.patch('eagris.controller.modal.crops.crop_list', return_value=eagri_crops)
    assert eagris.crop_modal.cropsComboBox.count() == 0
    eagris.crop_modal.show()
    assert eagris.crop_modal.cropsComboBox.count() == len(eagri_crops)
    assert \
        [eagris.crop_modal.cropsComboBox.itemText(i) for i in range(eagris.crop_modal.cropsComboBox.count())] == \
        ['Česnek', 'Česnek jarní', 'Šalotka']


def test_cropsShouldNotBeModifiedIfNotEmpty(mocker, sqlite_table_setup, eagris: Eagris, reset_crop_modal, eagri_crops):
    mocker.patch(
        'eagris.controller.modal.crops.crop_list',
        return_value=eagri_crops
    )
    assert eagris.crop_modal.cropsComboBox.count() == 0
    eagris.crop_modal.show()
    assert eagris.crop_modal.cropsComboBox.count() == len(eagri_crops)
    eagris.crop_modal.hide()
    eagris.crop_modal.show()
    assert eagris.crop_modal.cropsComboBox.count() == len(eagri_crops)


def test_okButtonShouldBeDisabledIffCropIsNotSelected(mocker, sqlite_table_setup, eagris: Eagris, reset_crop_modal,
                                                      eagri_crops):
    mocker.patch(
        'eagris.controller.modal.crops.crop_list',
        return_value=eagri_crops
    )
    eagris.crop_modal.show()
    assert not eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok).isEnabled()

    eagris.crop_modal.cropsComboBox.setCurrentIndex(0)
    assert eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok).isEnabled()

    eagris.crop_modal.cropsComboBox.setCurrentIndex(NO_ITEM_SELECTED_INDEX)
    assert not eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok).isEnabled()


def test_newCropShouldNotOverlapExistingCrop(
        mocker, qtbot, sqlite_table_setup, eagris: Eagris, reset_crop_modal, eagri_crops):
    mocker.patch(
        'eagris.controller.modal.crops.crop_list',
        return_value=eagri_crops
    )
    eagris.crop_modal.show()

    eagris.crop_modal.cropsComboBox.setCurrentIndex(0)
    eagris.crop_modal.dateFromEdit.setDate(QDate(2022, 10, 29))
    eagris.crop_modal.dateToEdit.clear()

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 2

    qtbot.mouseClick(eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok), Qt.LeftButton)

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 2


def test_validToIsOptional(mocker, qtbot, sqlite_table_setup, eagris: Eagris, reset_crop_modal, eagri_crops):
    mocker.patch(
        'eagris.controller.modal.crops.crop_list',
        return_value=eagri_crops
    )
    eagris.crop_modal.show()

    eagris.crop_modal.cropsComboBox.setCurrentIndex(0)
    eagris.crop_modal.dateFromEdit.setDate(QDate(2023, 10, 29))
    eagris.crop_modal.dateToEdit.clear()

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 2

    qtbot.mouseClick(eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok), Qt.LeftButton)

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 3
    assert eagris.panel.parcelDetailCrops.topLevelItem(2).data(0, Qt.UserRole) == ParcelCrop(
        crop_id=99,
        crop_name='Kuk',
        catch_crop=False,
        valid_from=date(2023, 10, 29),
        valid_to=None
    )


def test_shouldSplitExistingNewestCropIfItsValidToIsNullAndNewCropIsYoungest(
        mocker, qtbot, sqlite_table_setup, eagris: Eagris, reset_crop_modal, eagri_crops
):
    mocker.patch(
        'eagris.controller.modal.crops.crop_list',
        return_value=eagri_crops
    )
    eagris.crop_modal.show()

    eagris.crop_modal.cropsComboBox.setCurrentIndex(0)
    eagris.crop_modal.dateFromEdit.setDate(QDate(2023, 10, 29))
    eagris.crop_modal.dateToEdit.clear()

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 2

    qtbot.mouseClick(eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok), Qt.LeftButton)

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 3
    assert table_widget_crops(eagris.panel.parcelDetailCrops) == [
        ParcelCrop(
            crop_id=99,
            crop_name='Kuk',
            catch_crop=False,
            valid_from=date(2022, 5, 6),
            valid_to=date(2023, 4, 27)
        ),
        ParcelCrop(
            crop_id=99,
            crop_name='Kuk',
            catch_crop=False,
            valid_from=date(2023, 7, 1),
            valid_to=date(2023, 10, 28)
        ),
        ParcelCrop(
            crop_id=99,
            crop_name='Kuk',
            catch_crop=False,
            valid_from=date(2023, 10, 29),
            valid_to=None
        )
    ]


def test_shouldAddCropWhichIsOlderThanParcelValidity(
        mocker, qtbot, sqlite_table_setup, eagris: Eagris, reset_crop_modal, eagri_crops
):
    """
    AKA magic feature of eAGRI :)
    In practice, adding crop before parcel validity means exteding parcel validity to the validity of the new crop
    """
    mocker.patch(
        'eagris.controller.modal.crops.crop_list',
        return_value=eagri_crops
    )
    eagris.crop_modal.show()

    eagris.crop_modal.cropsComboBox.setCurrentIndex(0)
    eagris.crop_modal.dateFromEdit.setDate(QDate(2020, 5, 6))
    eagris.crop_modal.dateToEdit.setDate(QDate(2021, 5, 6))

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 2

    qtbot.mouseClick(eagris.crop_modal.buttonBox.button(QDialogButtonBox.Ok), Qt.LeftButton)

    assert eagris.panel.parcelDetailCrops.topLevelItemCount() == 3
    assert table_widget_crops(eagris.panel.parcelDetailCrops) == [
        ParcelCrop(crop_id=99,
                   crop_name='Kuk',
                   catch_crop=False,
                   valid_from=date(2020, 5, 6),
                   valid_to=date(2021, 5, 6)),
        ParcelCrop(crop_id=99,
                   crop_name='Kuk',
                   catch_crop=False,
                   valid_from=date(2022, 5, 6),
                   valid_to=date(2023, 4, 27)),
        ParcelCrop(crop_id=99,
                   crop_name='Kuk',
                   catch_crop=False,
                   valid_from=date(2023, 7, 1),
                   valid_to=None)
    ]
