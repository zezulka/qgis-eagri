import datetime

from PyQt5.QtWidgets import QDialogButtonBox
from dateutil.utils import today

from eagris.common.download_vector_feature import DownloadVectorFeature
from eagris.mainPlugin import Eagris


def __assert_prompt_is_set_to_default_state(eagris: Eagris):
    assert eagris.download_prompt.checkableComboBox.checkedItemsData() == [f.name for f in DownloadVectorFeature]
    # time-dependent tests are yuck but at least assert date this way without bothering with mocking
    assert eagris.download_prompt.validityDateEdit.date() == today()


def test_allFeaturesAndTodayValidityIsDefault(sqlite_table_setup, eagris: Eagris):
    __assert_prompt_is_set_to_default_state(eagris)


def test_promptResetsToDefaultWheneverUserInteractsWithButtonBox(sqlite_table_setup, eagris: Eagris):
    eagris.download_prompt.validityDateEdit.setDateTime(datetime.datetime(2022, 2, 2))
    eagris.download_prompt.checkableComboBox.deselectAllOptions()

    # no items are selected, so no mocking is required
    eagris.download_prompt.buttonBox.button(QDialogButtonBox.Ok).click()

    __assert_prompt_is_set_to_default_state(eagris)
