import pytest
from PyQt5.QtCore import Qt
from qgis.core import Qgis, QgsApplication

from eagris.eagri.env import EagriEnv
from eagris.model.auth import EagriAuthData

from eagris.mainPlugin import Eagris


def __assertModalWasCleared(eagris: Eagris):
    assert eagris.modal.loginInput.text() == ""
    assert eagris.modal.szridInput.text() == ""
    assert eagris.modal.wsKeyInput.text() == ""
    assert eagris.modal.nameInput.text() == ""


def test_userClicksPanelToolButtonAndModalAppears(sqlite_table_setup, qgis_new_project, qtbot, eagris: Eagris):
    assert not eagris.modal.isVisible()
    qtbot.mouseClick(eagris.panel.toolButton, Qt.LeftButton)
    assert eagris.modal.isVisible()


def test_canAddAuthDataIffAllRequiredInputsAreNonempty(sqlite_table_setup, qgis_new_project, eagris: Eagris):
    _assertUserCannotAddAuthData(eagris)

    eagris.modal.szridInput.setText("100004321")

    _assertUserCannotAddAuthData(eagris)

    eagris.modal.wsKeyInput.setText("weaofij4sadf452")

    _assertUserCannotAddAuthData(eagris)

    eagris.modal.loginInput.setText("99testfarm")

    _assertUserCanAddAuthData(eagris)

    eagris.modal.loginInput.setText(None)

    _assertUserCannotAddAuthData(eagris)

    # optional fields have no effect on button being enabled

    eagris.modal.nameInput.setText("Abernathy Farm, co.")

    _assertUserCannotAddAuthData(eagris)


def test_addsValidAuthDataWithPlainWsKey(sqlite_table_setup, modal_with_data, mocker, qtbot, eagris: Eagris):
    mocker.patch('eagris.controller.worker.add_auth_data.auth_data_probe', return_value=True)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0

    qtbot.mouseClick(eagris.modal.addAuthBtn, Qt.LeftButton)

    # this is a very cumbersome way but to my knowledge, there is no suitable signal on QTreeView which would
    # elegantly go with qtbot.waitSignal :(
    qtbot.waitUntil(lambda: eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItem(0).data(0, Qt.UserRole) == EagriAuthData(
        login='99testfarm',
        szrid='100004321',
        wskey='4db6cf8d1d9949790c7e836f29f12dc37c15b3a9',
        name='Abernathy Farm, co.',
        env=EagriEnv.PROD
    )
    __assertModalWasCleared(eagris)


def test_addsValidAuthDataWithHashedKey(sqlite_table_setup, modal_with_data, mocker, qtbot, eagris: Eagris):
    mocker.patch('eagris.controller.worker.add_auth_data.auth_data_probe', return_value=True)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0

    eagris.modal.hashedWsKeyRadioButton.setChecked(True)
    qtbot.mouseClick(eagris.modal.addAuthBtn, Qt.LeftButton)

    # this is a very cumbersome way but to my knowledge, there is no suitable signal on QTreeView which would
    # elegantly go with qtbot.waitSignal :(
    qtbot.waitUntil(lambda: eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItem(0).data(0, Qt.UserRole) == EagriAuthData(
        login='99testfarm',
        szrid='100004321',
        wskey='cafebabe',
        name='Abernathy Farm, co.',
        env=EagriEnv.PROD
    )
    __assertModalWasCleared(eagris)


def test_addsValidAuthDataWithNondefaultEagriEnvironment(sqlite_table_setup, modal_with_data, mocker, qtbot,
                                                         eagris: Eagris):
    mocker.patch('eagris.controller.worker.add_auth_data.auth_data_probe', return_value=True)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0

    eagris.modal.hashedWsKeyRadioButton.setChecked(True)
    eagris.modal.envComboBox.setCurrentIndex(eagris.modal.envComboBox.findText(EagriEnv.TEST.name))
    qtbot.mouseClick(eagris.modal.addAuthBtn, Qt.LeftButton)

    # this is a very cumbersome way but to my knowledge, there is no suitable signal on QTreeView which would
    # elegantly go with qtbot.waitSignal :(
    qtbot.waitUntil(lambda: eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItem(0).data(0, Qt.UserRole) == EagriAuthData(
        login='99testfarm',
        szrid='100004321',
        wskey='cafebabe',
        name='Abernathy Farm, co.',
        env=EagriEnv.TEST
    )
    __assertModalWasCleared(eagris)


def test_doesNotAddDataIfTheyAreAlreadyPresent(
        sqlite_table_setup,
        modal_with_data,
        auth_cred_tree_with_one_item,
        mocker,
        qtbot,
        eagris: Eagris
):
    mocker.patch('eagris.controller.worker.add_auth_data.auth_data_probe', return_value=True)
    qtbot.waitUntil(lambda: eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1)

    # passing lambda directly does not seem to work
    def checkErrorMessageSubmitted(msg, _tag, level):
        return level == Qgis.MessageLevel.Warning \
            and msg == "Authentication data are already present. Please check the form data and try again."

    message_recv_callback = QgsApplication.messageLog().messageReceived

    # now add second row - with exactly the same data
    with qtbot.waitSignal(message_recv_callback, check_params_cb=checkErrorMessageSubmitted):
        qtbot.mouseClick(eagris.modal.addAuthBtn, Qt.LeftButton)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1


def test_doesNotAddDataIfProbeFails(sqlite_table_setup, modal_with_data, mocker, qtbot, eagris: Eagris):
    mocker.patch('eagris.controller.worker.add_auth_data.auth_data_probe', return_value=False)

    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0

    def checkErrorMessageSubmitted(msg, _tag, level):
        return level == Qgis.MessageLevel.Warning \
            and msg == "Authentication data are invalid. Please try again."

    message_recv_callback = QgsApplication.messageLog().messageReceived

    # now add second row - with exactly the same data
    with qtbot.waitSignal(message_recv_callback, check_params_cb=checkErrorMessageSubmitted):
        qtbot.mouseClick(eagris.modal.addAuthBtn, Qt.LeftButton)


def _assertUserCannotAddAuthData(eagris: Eagris):
    assert not eagris.modal.forceAddBtn.isEnabled()
    assert not eagris.modal.addAuthBtn.isEnabled()


def _assertUserCanAddAuthData(eagris: Eagris):
    assert eagris.modal.forceAddBtn.isEnabled()
    assert eagris.modal.addAuthBtn.isEnabled()
