import sqlite3

import pytest
from PyQt5.QtWidgets import QWizard

from eagris.controller.eagriswelcomewizard import QgisEagriWelcomeWizard

from PyQt5.QtCore import Qt


@pytest.fixture
def wizard(qgis_iface):
    return QgisEagriWelcomeWizard(qgis_iface, lambda: None)


def test_shouldStayOnTheSamePageIfSqliteErrorIsThrown(mocker, qtbot, wizard: QgisEagriWelcomeWizard):
    wizard.show()
    __assert_wizard_is_at_the_first_page(wizard)

    mocker.patch('eagris.configuration.cred_settings.create_session', side_effect=sqlite3.Error())
    wizard.authPathFileWidget.setFilePath("/tmp/credentials.db")
    qtbot.mouseClick(wizard.button(QWizard.WizardButton.NextButton), Qt.LeftButton)

    __assert_wizard_is_at_the_first_page(wizard)

def test_shouldStayOnTheSamePageIfFileIsInvalid(qtbot, wizard: QgisEagriWelcomeWizard):
    wizard.show()
    __assert_wizard_is_at_the_first_page(wizard)

    wizard.authPathFileWidget.setFilePath("")
    qtbot.mouseClick(wizard.button(QWizard.WizardButton.NextButton), Qt.LeftButton)
    __assert_wizard_is_at_the_first_page(wizard)

    wizard.authPathFileWidget.setFilePath("/tmp/credentials.txt")
    qtbot.mouseClick(wizard.button(QWizard.WizardButton.NextButton), Qt.LeftButton)
    __assert_wizard_is_at_the_first_page(wizard)


def test_wizardIsShown(mocker, qtbot, wizard: QgisEagriWelcomeWizard):
    assert wizard.authPathFileWidget.filePath()
    wizard.show()
    __assert_wizard_is_at_the_first_page(wizard)
    qtbot.mouseClick(wizard.button(QWizard.WizardButton.NextButton), Qt.LeftButton)

    assert wizard.currentId() == 1
    with qtbot.waitSignal(wizard.find_finish_button().clicked):
        qtbot.mouseClick(wizard.find_finish_button(), Qt.LeftButton)


def __assert_wizard_is_at_the_first_page(wizard: QgisEagriWelcomeWizard):
    assert wizard.currentId() == 0
