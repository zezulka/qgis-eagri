from datetime import date, datetime

import pytest
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialogButtonBox
from qgis.PyQt.QtCore import NULL
from qgis.gui import QgsMapCanvas
from qgis.core import Qgis, QgsApplication, QgsVectorLayer

from eagris.eagri.ws.lpi_ddp01_b.model.response import DpbDetailResponse, Parcel, ParcelVersion, ParcelCrop
from eagris.eagri.ws.lpi_gdp11_b.model.response import DpbListResponse, Dpb
from eagris.mainPlugin import Eagris
from tests.qgis.conftest import functionfixture


@functionfixture
def mocked_dpb_list_response(qgis_new_project):
    return DpbListResponse(
        changed=datetime(
            2022, 2, 2,
            2, 2, 2
        ),
        blocks=[
            Dpb(
                id=1,
                zkod="0001",
                square="123-4321",
                status="UCINNY",
                valid_from=date(2023, 5, 15),
                valid_to=None,
                area=9.4213,
                culture="R",
                culture_name="standardni orna puda",
                geometry="Polygon ((-806921.85270000004675239 -1059064.13739999989047647, -806927.98580000002402812 -1059033.70650000008754432, -806928.05240000004414469 -1059033.46650000009685755, -806946.09999999997671694 -1059035.69999999995343387, -806976.09999999997671694 -1059036.5, -807010.09999999997671694 -1059035.19999999995343387, -807016.40000000002328306 -1059037.39999999990686774, -807018.40099999995436519 -1059042.69999999995343387, -807012.90000000002328306 -1059058.10000000009313226, -807004.40000000002328306 -1059072.10000000009313226, -807000.60530000005383044 -1059078.50139999995008111, -806995.31359999999403954 -1059084.85150000010617077, -806989.66920000000391155 -1059087.14449999993667006, -806980.14410000003408641 -1059088.02649999991990626, -806969.88850000000093132 -1059085.49259999999776483, -806955.8628000000026077 -1059081.17540000006556511, -806931.79890000005252659 -1059070.83009999990463257, -806921.85270000004675239 -1059064.13739999989047647))"
            )
        ]
    )


@functionfixture
def mock_dpb_detail_response(qgis_new_project) -> DpbDetailResponse:
    return DpbDetailResponse(
        parcels=[
            Parcel(
                id=2134,
                name="Za Vodárnou 1",
                valid_from=date(2022, 5, 6),
                valid_to=None,
                crops=[ParcelCrop(
                    crop_id=99,
                    crop_name='Kukuřice',
                    catch_crop=False,
                    valid_from=date(2022, 6, 6),
                    valid_to=date(2022, 9, 9)
                ),
                    ParcelCrop(
                        crop_id=99,
                        crop_name='Kukuřice',
                        catch_crop=False,
                        valid_from=date(2023, 1, 29),
                        valid_to=None
                    )],
                versions=[
                    ParcelVersion(
                        id=1,
                        geometry="Polygon ((-806921.85270000004675239 -1059064.13739999989047647, -806927.98580000002402812 -1059033.70650000008754432, -806928.05240000004414469 -1059033.46650000009685755, -806946.09999999997671694 -1059035.69999999995343387, -806976.09999999997671694 -1059036.5, -807010.09999999997671694 -1059035.19999999995343387, -807016.40000000002328306 -1059037.39999999990686774, -807018.40099999995436519 -1059042.69999999995343387, -807012.90000000002328306 -1059058.10000000009313226, -807004.40000000002328306 -1059072.10000000009313226, -807000.60530000005383044 -1059078.50139999995008111, -806995.31359999999403954 -1059084.85150000010617077, -806989.66920000000391155 -1059087.14449999993667006, -806980.14410000003408641 -1059088.02649999991990626, -806969.88850000000093132 -1059085.49259999999776483, -806955.8628000000026077 -1059081.17540000006556511, -806931.79890000005252659 -1059070.83009999990463257, -806921.85270000004675239 -1059064.13739999989047647))",
                        valid_from=date(2022, 5, 6),
                        valid_to=date(2023, 5, 14),
                        area=0.38897826
                    ),
                    ParcelVersion(
                        id=2,
                        geometry="Polygon ((-806928.80728168867062777 -1059070.01972922566346824, -806931.24185954371932894 -1059071.65793268126435578, -806931.40481205657124519 -1059071.74677644832991064, -806955.4687120565213263 -1059082.09207644849084318, -806955.56926232541445643 -1059082.12904388108290732, -806969.59496232541278005 -1059086.44624388101510704, -806969.64916623674798757 -1059086.46126938005909324, -806979.90476623678114265 -1059088.99516937998123467, -806980.06939027435146272 -1059089.0214972235262394, -806980.23609998601023108 -1059089.02004769421182573, -806989.76119998598005623 -1059088.13804769422858953, -806990.04474302497692406 -1059088.0689287178684026, -806995.68914302496705204 -1059085.77592871803790331, -806995.90222916018683463 -1059085.65717780799604952, -806996.08013268990907818 -1059085.49027120601385832, -807001.37183268996886909 -1059079.14017120585776865, -807001.46362209424842149 -1059079.0102066439576447, -807005.25563754071481526 -1059072.61333530279807746, -807013.75290534575469792 -1059058.61783538851886988, -807013.83964894793462008 -1059058.43564992630854249, -807019.34064894786570221 -1059043.0356499261688441, -807019.3849065329413861 -1059042.86591851222328842, -807019.39875803096219897 -1059042.69105964689515531, -807019.38177538313902915 -1059042.51647706935182214, -807019.33448341151233763 -1059042.34756597992964089, -807017.3334834115812555 -1059037.04756597988307476, -807017.23491673835087568 -1059036.85363468411378562, -807017.09666326874867082 -1059036.68567422800697386, -807016.9252947208005935 -1059036.55166841694153845, -807016.72895689925644547 -1059036.45798706123605371, -807010.42895689920987934 -1059034.25798706128261983, -807010.24799952795729041 -1059034.21323905722238123, -807010.0618767534615472 -1059034.20293047558516264, -806976.09423155512195081 -1059035.50169338029809296, -806946.17474735924042761 -1059034.70384046831168234, -806929.82896616356447339 -1059032.68095087097026408, -806929.61894577427301556 -1059032.67723810090683401, -806929.41280189482495189 -1059032.71757480315864086, -806929.21967026533093303 -1059032.80017336411401629, -806927.28868893918115646 -1059033.87925116391852498, -806927.11363104148767889 -1059034.00353626068681479, -806926.96921187930274755 -1059034.16239203931763768, -806926.86211743974126875 -1059034.34846415929496288, -806926.79730573575943708 -1059034.55313828052021563, -806920.87456958019174635 -1059063.94026578171178699, -806920.85490541462786496 -1059064.13475736300460994, -806920.87353909818921238 -1059064.3293503753375262, -806920.92975542368367314 -1059064.51657585240900517, -806921.02139666792936623 -1059064.68924761330708861, -806921.14494541112799197 -1059064.84073808370158076, -806921.29565954371355474 -1059064.96523268125019968, -806924.91082101536449045 -1059067.3978392225690186, -806925.12560287350788713 -1059067.50726854847744107, -806927.20412377815227956 -1059068.26627899124287069, -806928.61912203952670097 -1059069.85543088475242257, -806928.80728168867062777 -1059070.01972922566346824))",
                        valid_from=date(2023, 5, 15),
                        valid_to=None,
                        area=0.41425832
                    )
                ],
                iddpb=4321
            ),
            Parcel(
                id=2135,
                name="Za Vodárnou - kuk.",
                valid_from=date(2022, 5, 6),
                valid_to=None,
                crops=[
                    ParcelCrop(
                        crop_id=99,
                        crop_name='Kukuřice',
                        catch_crop=False,
                        valid_from=date(2022, 6, 6),
                        valid_to=date(2022, 9, 9)
                    ),
                    ParcelCrop(
                        crop_id=99,
                        crop_name='Kukuřice',
                        catch_crop=False,
                        valid_from=date(2023, 1, 29),
                        valid_to=None
                    )
                ],
                versions=[
                    ParcelVersion(
                        id=1,
                        geometry="Polygon ((-808515 -1057473.69999999995343387, -808515.30000000004656613 -1057462.19999999995343387, -808517.69999999995343387 -1057443.89999999990686774, -808521.54209999996237457 -1057425.95779999997466803, -808525.30000000004656613 -1057417.5, -808528 -1057413.89999999990686774, -808530.59999999997671694 -1057411, -808534.40000000002328306 -1057409.10000000009313226, -808546.412999999942258 -1057409.20079999999143183, -808630.90339999995194376 -1057422.60639999993145466, -808689.99380000005476177 -1057421.01890000002458692, -808708.86750000005122274 -1057421.9007999999448657, -808717.68689999997150153 -1057422.42999999993480742, -808727.212000000057742 -1057424.899500000057742, -808738.80000000004656613 -1057431.60000000009313226, -808740.90000000002328306 -1057439, -808729.69999999995343387 -1057457.10000000009313226, -808724.38970000005792826 -1057478.69819999998435378, -808722.97860000003129244 -1057489.63430000003427267, -808723.68420000001788139 -1057503.74539999989792705, -808722.62580000003799796 -1057518.9148999999742955, -808718.30000000004656613 -1057526.60000000009313226, -808710.98410000000149012 -1057530.90940000000409782, -808699.34250000002793968 -1057531.2621999999973923, -808685.23129999998491257 -1057528.61630000011064112, -808656.12899999995715916 -1057521.62000000011175871, -808631.30000000004656613 -1057518.30000000004656613, -808615.50379999994765967 -1057518.23949999990873039, -808598.09499999997206032 -1057518.73849999997764826, -808586.30260000005364418 -1057520.44860000000335276, -808578.98499999998603016 -1057522.38149999990127981, -808574.28249999997206032 -1057524.7357999999076128, -808567.05050000001210719 -1057529.32190000009723008, -808558.23109999997541308 -1057534.61360000004060566, -808547.29489999997895211 -1057540.6107999999076128, -808535.30299999995622784 -1057547.48160000005736947, -808522.51560000004246831 -1057557.69219999993219972, -808514.23149999999441206 -1057553.5501999999396503, -808498.78789999999571592 -1057540.6107999999076128, -808484.85309999994933605 -1057521.73710000002756715, -808461.537999999942258 -1057490.13419999997131526, -808451.59999999997671694 -1057479, -808458.04200000001583248 -1057479.40369999990798533, -808482.91289999999571592 -1057482.57869999995455146, -808504.40000000002328306 -1057483.80000000004656613, -808510.30000000004656613 -1057482.10000000009313226, -808513.78099999995902181 -1057477.81620000000111759, -808515 -1057473.69999999995343387))",
                        valid_from=date(2022, 5, 6),
                        valid_to=date(2023, 5, 14),
                        area=2.56765338
                    ),
                    ParcelVersion(
                        id=2,
                        geometry="Polygon ((-808515 -1057473.69999999995343387, -808515.30000000004656613 -1057462.19999999995343387, -808517.69999999995343387 -1057443.89999999990686774, -808521.54209999996237457 -1057425.95779999997466803, -808525.30000000004656613 -1057417.5, -808528 -1057413.89999999990686774, -808530.59999999997671694 -1057411, -808534.40000000002328306 -1057409.10000000009313226, -808546.412999999942258 -1057409.20079999999143183, -808630.90339999995194376 -1057422.60639999993145466, -808689.99380000005476177 -1057421.01890000002458692, -808708.86750000005122274 -1057421.9007999999448657, -808717.68689999997150153 -1057422.42999999993480742, -808727.212000000057742 -1057424.899500000057742, -808738.80000000004656613 -1057431.60000000009313226, -808740.90000000002328306 -1057439, -808729.69999999995343387 -1057457.10000000009313226, -808724.38970000005792826 -1057478.69819999998435378, -808722.97860000003129244 -1057489.63430000003427267, -808723.68420000001788139 -1057503.74539999989792705, -808722.62580000003799796 -1057518.9148999999742955, -808718.30000000004656613 -1057526.60000000009313226, -808710.98410000000149012 -1057530.90940000000409782, -808699.34250000002793968 -1057531.2621999999973923, -808685.23129999998491257 -1057528.61630000011064112, -808656.12899999995715916 -1057521.62000000011175871, -808631.30000000004656613 -1057518.30000000004656613, -808615.50379999994765967 -1057518.23949999990873039, -808598.09499999997206032 -1057518.73849999997764826, -808586.30260000005364418 -1057520.44860000000335276, -808578.98499999998603016 -1057522.38149999990127981, -808574.28249999997206032 -1057524.7357999999076128, -808567.05050000001210719 -1057529.32190000009723008, -808558.23109999997541308 -1057534.61360000004060566, -808547.29489999997895211 -1057540.6107999999076128, -808535.30299999995622784 -1057547.48160000005736947, -808522.51560000004246831 -1057557.69219999993219972, -808514.23149999999441206 -1057553.5501999999396503, -808498.78789999999571592 -1057540.6107999999076128, -808484.85309999994933605 -1057521.73710000002756715, -808461.537999999942258 -1057490.13419999997131526, -808451.59999999997671694 -1057479, -808458.04200000001583248 -1057479.40369999990798533, -808482.91289999999571592 -1057482.57869999995455146, -808504.40000000002328306 -1057483.80000000004656613, -808510.30000000004656613 -1057482.10000000009313226, -808513.78099999995902181 -1057477.81620000000111759, -808515 -1057473.69999999995343387))",
                        valid_from=date(2023, 5, 15),
                        valid_to=None,
                        area=2.56765338
                    )
                ],
                iddpb=4321
            )
        ]
    )


def test_layersAreImportedAfterDoubleClickOnRowWithAuthData(
        mocker,
        mocked_dpb_list_response,
        mock_dpb_detail_response,
        tree_with_one_auth_item,
        qtbot,
        qgis_canvas,
        eagris: Eagris
):
    mocker.patch('eagris.controller.worker.eagri_data_download_worker.dpb_list', return_value=mocked_dpb_list_response)
    mocker.patch('eagris.controller.worker.block_detail_downloader.dpb_detail', return_value=mock_dpb_detail_response)

    def checkVectorLayerImportFinished(msg, _tag, level):
        return level == Qgis.MessageLevel.Info and msg == "eAGRI layer import finished."

    message_recv_callback = QgsApplication.messageLog().messageReceived

    with qtbot.waitSignal(message_recv_callback, check_params_cb=checkVectorLayerImportFinished):
        eagris.panel.eagriUsersTreeWidget.setCurrentItem(eagris.panel.eagriUsersTreeWidget.topLevelItem(0))
        __accept_download_prompt(qtbot, eagris)
    __should_have_imported_all_layers(qgis_canvas, mocked_dpb_list_response, mock_dpb_detail_response)


def test_onlyLayersSelectedByUserAreImportedAsQgisLayers(
        mocker,
        mocked_dpb_list_response,
        mock_dpb_detail_response,
        tree_with_one_auth_item,
        qtbot,
        qgis_canvas,
        eagris: Eagris
):
    mocker.patch('eagris.controller.worker.eagri_data_download_worker.dpb_list', return_value=mocked_dpb_list_response)
    mocker.patch('eagris.controller.worker.block_detail_downloader.dpb_detail', return_value=mock_dpb_detail_response)

    def checkVectorLayerImportFinished(msg, _tag, level):
        return level == Qgis.MessageLevel.Info and msg == "eAGRI layer import finished."

    message_recv_callback = QgsApplication.messageLog().messageReceived

    with qtbot.waitSignal(message_recv_callback, check_params_cb=checkVectorLayerImportFinished):
        # select and import only DPB feature
        eagris.download_prompt.checkableComboBox.deselectAllOptions()
        eagris.download_prompt.checkableComboBox.toggleItemCheckState(0)

        eagris.panel.eagriUsersTreeWidget.setCurrentItem(eagris.panel.eagriUsersTreeWidget.topLevelItem(0))
        __accept_download_prompt(qtbot, eagris)

    dpb_list_layer = qgis_canvas.layer(0)
    __assertDpbListLayerWasImported(dpb_list_layer, mocked_dpb_list_response)
    assert qgis_canvas.layerCount() == 1


def test_layersAreImportedAfterDownloadButtonClick(
        mocker,
        mocked_dpb_list_response,
        mock_dpb_detail_response,
        tree_with_one_auth_item,
        qtbot,
        qgis_canvas,
        eagris: Eagris
):
    mocker.patch('eagris.controller.worker.eagri_data_download_worker.dpb_list', return_value=mocked_dpb_list_response)
    mocker.patch('eagris.controller.worker.block_detail_downloader.dpb_detail', return_value=mock_dpb_detail_response)

    def checkVectorLayerImportFinished(msg, _tag, level):
        return level == Qgis.MessageLevel.Info and msg == "eAGRI layer import finished."

    eagris.panel.eagriUsersTreeWidget.setCurrentItem(eagris.panel.eagriUsersTreeWidget.topLevelItem(0))
    message_recv_callback = QgsApplication.messageLog().messageReceived

    with qtbot.waitSignal(message_recv_callback, check_params_cb=checkVectorLayerImportFinished):
        __accept_download_prompt(qtbot, eagris)

    __should_have_imported_all_layers(qgis_canvas, mocked_dpb_list_response, mock_dpb_detail_response)


def test_authDataIsRemovedAfterDeleteButtonPress(qtbot, tree_with_one_auth_item, eagris: Eagris):
    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 1

    eagris.panel.eagriUsersTreeWidget.setCurrentItem(eagris.panel.eagriUsersTreeWidget.topLevelItem(0))
    qtbot.mouseClick(
        eagris.panel.deleteButton,
        Qt.LeftButton
    )
    assert eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0


def __accept_download_prompt(qtbot, eagris):
    qtbot.mouseClick(
        eagris.download_prompt.buttonBox.button(QDialogButtonBox.Ok),
        Qt.MouseButton.LeftButton,
        pos=eagris.download_prompt.buttonBox.button(QDialogButtonBox.Ok).visibleRegion().boundingRect().center()
    )


def __should_have_imported_all_layers(
        qgis_canvas: QgsMapCanvas,
        mocked_dpb_list_response: DpbListResponse,
        mocked_parcel_response: DpbDetailResponse
):
    dpb_list_layer = qgis_canvas.layer(0)
    parcel_layer = qgis_canvas.layer(1)
    parcel_crop_layer = qgis_canvas.layer(2)
    parcel_version_layer = qgis_canvas.layer(3)

    __assertDpbListLayerWasImported(dpb_list_layer, mocked_dpb_list_response)
    __assertParcelVersionLayerWasImported(parcel_version_layer, mocked_parcel_response)
    __assertParcelLayerWasImported(parcel_layer, mocked_parcel_response)
    __assertParcelCropLayerWasImported(parcel_crop_layer, mocked_parcel_response)


def __assertDpbListLayerWasImported(
        layer: QgsVectorLayer,
        dpb_list_response: DpbListResponse
):
    # source name also contains a timestamp at the end
    assert layer.sourceName().startswith(f"eAGRI / DPB / Abernathy Farm, co.")
    iterator = layer.getFeatures()
    features = list(iterator)
    assert len(features) > 0
    for feature, dpb in zip(features, dpb_list_response.blocks):
        assert feature.geometry().asWkt() == dpb.geometry
        assert feature.attributes() == [
            dpb.id,
            dpb.square,
            dpb.zkod,
            dpb.status,
            dpb.valid_from,
            NULL,  # valid to
            dpb.area,
            dpb.culture,
            dpb.culture_name,
            dpb.geometry
        ]


def __assertParcelVersionLayerWasImported(
        layer: QgsVectorLayer,
        dpb_detail_response: DpbDetailResponse
):
    # source name also contains a timestamp at the end
    assert layer.sourceName().startswith(
        f"eAGRI / ZP / VERSION / Abernathy Farm, co.")
    iterator = layer.getFeatures()
    features = list(iterator)
    assert len(features) == 4

    for parcel, features in [
        (dpb_detail_response.parcels[0], features[:2]),
        (dpb_detail_response.parcels[1], features[2:])
    ]:
        for feature, parcel_version in zip(
                features[:2],
                [version for version in parcel.versions]
        ):
            assert feature.geometry().asWkt() == parcel_version.geometry
            assert feature.attributes() == [
                parcel.id,
                parcel_version.valid_from,
                parcel_version.valid_to,
                parcel_version.id,
                parcel_version.area,
                parcel_version.geometry,
                parcel.name,
                parcel.valid_from,
                parcel.valid_to,
                parcel.iddpb
            ]


def __assertParcelLayerWasImported(
        layer: QgsVectorLayer,
        mocked_parcel_response: DpbDetailResponse
):
    assert layer.sourceName().startswith(
        f"eAGRI / ZP / PARCEL / Abernathy Farm, co.")
    iterator = layer.getFeatures()
    features = list(iterator)
    for feature, parcel in zip(features, mocked_parcel_response.parcels):
        assert not feature.geometry()
        assert feature.attributes() == [
            parcel.id,
            parcel.name,
            parcel.valid_from,
            parcel.valid_to,
            parcel.iddpb
        ]


def __assertParcelCropLayerWasImported(
        layer: QgsVectorLayer,
        mocked_parcel_response: DpbDetailResponse
):
    assert layer.sourceName().startswith(
        f"eAGRI / ZP / CROP / Abernathy Farm, co.")
    iterator = layer.getFeatures()
    features = list(iterator)
    for feature, (parcel, crop) in zip(features,
                                       [(parcel, crop) for parcel in mocked_parcel_response.parcels for crop in
                                        parcel.crops]):
        assert not feature.geometry()
        assert feature.attributes() == [
            parcel.id,
            crop.crop_id,
            'Kukuřice',
            crop.catch_crop,
            crop.valid_from,
            crop.valid_to,
            parcel.name,
            parcel.valid_from,
            parcel.valid_to,
            parcel.iddpb
        ]
