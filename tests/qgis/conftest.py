from datetime import date
from typing import List

import pytest
from qgis.core import QgsFeature, QgsFields
from qgis.gui import QgisInterface

from eagris.auth.auth_settings import AuthCredSettings
from eagris.auth.sqlite.utils import execute_sqlite_statement, create_session
from eagris.configuration.cred_settings import setup_local_creds
from eagris.controller.attribute_table_qgs_fields import LpisBlockDetailParcelCropAttributeTableField
from eagris.eagri.env import EagriEnv
from eagris.eagri.ws.lpi_ddp01_b.model.response import ParcelCrop
from eagris.eagri.ws.lpi_gpl01_c.model.response import EagriCrop
from eagris.mainPlugin import Eagris
from eagris.model.auth import EagriAuthData
from eagris.qgis.feature_mapper import parcel_crop_to_feature

functionfixture = pytest.fixture(scope="function")


class StatusBarStub:
    def showMessage(self, msg):
        pass

    def clearMessage(self):
        pass


def statusBarIface():
    return StatusBarStub()


@functionfixture
def eagris(qgis_iface: QgisInterface):
    """
    Main fixture of integration tests.
    Contains a reference to the initialized Eagris plugin.
    """
    qgis_iface.statusBarIface = statusBarIface
    plugin = Eagris(qgis_iface)
    plugin.initGui()
    return plugin


@pytest.fixture
def auth_settings():
    return AuthCredSettings(
        # create an in-memory database
        authCredsPath="file:memory?mode=memory&cache=shared",
        tableName="lpis_auth_data",
        loginField="login",
        szridField="szrid",
        wsKeyField="wskey",
        nameField="name",
        envField="env"
    )


@pytest.fixture
def mocked_eagri_auth_data() -> EagriAuthData:
    return EagriAuthData(
        name="Abernathy Farm, co.",
        szrid="100004321",
        wskey="cafebabe",
        login="99testfarm",
        env=EagriEnv.PROD
    )


@pytest.fixture
def modal_with_data(sqlite_table_setup, mocked_eagri_auth_data: EagriAuthData, eagris: Eagris):
    """
    Sets up Eagris modal form so that all data are filled in so that
    the form is ready for submission.
    """
    eagris.modal.nameInput.setText(mocked_eagri_auth_data.name)
    eagris.modal.szridInput.setText(mocked_eagri_auth_data.szrid)
    eagris.modal.wsKeyInput.setText(mocked_eagri_auth_data.wskey)
    eagris.modal.loginInput.setText(mocked_eagri_auth_data.login)


@pytest.fixture
def auth_cred_tree_with_one_item(mocked_eagri_auth_data: EagriAuthData, eagris: Eagris):
    # bypass adding auth data via modal so that the modal is still filled in
    eagris.panel.creds_tree.add_auth_item(mocked_eagri_auth_data)


@pytest.fixture
def session(auth_settings):
    return create_session(auth_settings.authCredsPath)


@pytest.fixture
def sqlite_table_setup(session, auth_settings):
    """
    Use for test cases which assume that eagris is correctly setup (i.e. welcome wizard is not shown).
    """
    setup_local_creds()
    execute_sqlite_statement(
        f"""
            CREATE TABLE IF NOT EXISTS {auth_settings.tableName}(
                {auth_settings.loginField} text NOT NULL,
                {auth_settings.szridField} text NOT NULL,
                {auth_settings.wsKeyField} text NOT NULL,
                {auth_settings.nameField} text NULL,
                {auth_settings.envField} text NULL
            );
            """,
        session
    )
    yield auth_settings


@pytest.fixture
def tree_with_one_auth_item(sqlite_table_setup, eagris: Eagris):
    if eagris.panel.eagriUsersTreeWidget.topLevelItemCount() == 0:
        eagris.panel.creds_tree.add_auth_item(EagriAuthData(
            name="Abernathy Farm, co.",
            env=EagriEnv.TEST,
            login="99testfarm",
            wskey="cafebabe",
            szrid="100004321"
        ))
    yield
    eagris.panel.creds_tree.remove_all()


@pytest.fixture
def parcel_id():
    return 1243


@pytest.fixture
def crop_table_fields():
    fields = QgsFields()
    [
        fields.append(f.value)
        for f in LpisBlockDetailParcelCropAttributeTableField
    ]
    return fields


@pytest.fixture
def parcel_crop_feature_corn(crop_table_fields, parcel_id) -> QgsFeature:
    return parcel_crop_to_feature(
        parcel_id=parcel_id,
        parcel_crop=ParcelCrop(
            crop_id=99,
            crop_name='Kukuřice',
            catch_crop=False,
            valid_from=date(2022, 5, 6),
            valid_to=date(2023, 4, 27)
        ),
        fields=crop_table_fields
    )


@pytest.fixture
def parcel_crop_feature_barley(crop_table_fields, parcel_id) -> QgsFeature:
    return parcel_crop_to_feature(
        parcel_id=parcel_id,
        parcel_crop=ParcelCrop(
            crop_id=98,
            crop_name='Ječmen',
            catch_crop=False,
            valid_from=date(2023, 7, 1),
            valid_to=None
        ),
        fields=crop_table_fields
    )


@functionfixture
def eagri_crops() -> List[EagriCrop]:
    return [
        EagriCrop(id=223, name='Česnek', valid_from=date(2000, 1, 1), changed=date(2022, 7, 22)),
        EagriCrop(id=361, name='Česnek jarní', valid_from=date(2000, 1, 1),
                  changed=date(2023, 2, 27)),
        EagriCrop(id=416, name='Šalotka', valid_from=date(2000, 1, 1), changed=date(2023, 2, 28))
    ]
