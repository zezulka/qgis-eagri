FROM qgis/qgis:final-3_28_6

# headless QGIS
ENV QT_QPA_PLATFORM=offscreen

# add new regular user and run tests under his privileges
RUN useradd -ms /bin/bash eagris &&\
    apt-get update &&\
    apt-get install -y python3-venv

USER eagris
WORKDIR /home/eagris

COPY --chown=eagris:eagris ./requirements      /home/eagris/requirements

RUN mkdir -p /home/eagris/requirements &&\
    mkdir -p /home/eagris/tests &&\
    mkdir -p /home/eagris/eagris &&\
    python3 -m venv tests &&\
    . tests/bin/activate &&\
    pip3 install pytest &&\
    pip3 install -r requirements/testing-qgis.txt