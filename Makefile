# version used locally, i.e. dev version not intended for relea
SNAPSHOT_VER = snapshot
QGIS_CONTAINER_NAME = qgis
PATH_TO_PROJECT = ~/git/eagris

REQUIRED_EXECUTABLES = make python3 flake8 docker-compose pyrcc5 # pyrcc5 is available in an apt package pyqt5-dev-tools
EXECUTABLES_VERIFICATION_RESULT := $(foreach exec,$(REQUIRED_EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

all: git codecov test install clean

quick: git qgis-plugin install clean

git:
    # qgis-plugin-ci does not bundle files outside of version control ¯\_(ツ)_/¯
	git add .

codecov:
	flake8 eagris/ --count --select=E9,F63,F7,F82 --show-source --statistics
	flake8 eagris/ --count --exit-zero --max-complexity=10 --max-line-length=127 --statistics

test: unit-test qgis-test-from-scratch

unit-test:
	python3 -m venv venv-unit-tests; . venv-unit-tests/bin/activate ; pip3 install -r requirements/testing.txt; cd tests; python3 -m pytest -vvl unit; deactivate

qgis-test-from-scratch:
	docker-compose build qgis-base && docker-compose build qgis-test && docker-compose run qgis-test

qgis-plugin:
	qgis-plugin-ci package ${SNAPSHOT_VER} --allow-uncommitted-changes

install: qgis-plugin
	rm -rf ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/eagris
	unzip -q -o eagris.${SNAPSHOT_VER} -d ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins

documentation:
	sphinx-build -b html -d docs/build/cache -j auto -v docs/source target/docs && xdg-open target/docs/index.html

clean:
	find . -name "*.pyc" -o -name "Ui_.*" -o -name "resources_*.py" -exec rm \{\} \;
	rm  eagris.${SNAPSHOT_VER}.zip
	rm -rf venv-unit-tests
