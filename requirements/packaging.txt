# Packaging
# ---------

qgis-plugin-ci>=2.4,<2.6
flake8>=6.1.0
toml>=0.10.2