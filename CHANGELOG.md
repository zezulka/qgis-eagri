# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.1] - 2024-01-14
### Fixed

- backwards compatibility with older (< 3.9) Python version

## [1.4.0] - 2023-11-18
### Added

- user can now import only a subset of layers
- user can pick validity for which data will be downloaded (does not apply to crops)

### Fixed

- added pytest-timeout to stabilize test suite runs

## [1.3.0] - 2023-10-29
### Added

- support of the LPI_ZZP01B eAGRI service (parcel management); this feature is turned off as LPI_ZZP01B is nonoperational
- support of the LPI_GPL01C eAGRI service (global crop catalogue)
  - user can also add/remove crops from the parcel detail; this was supposed to be a part of parcel update feature which, as mentioned, is not technically possible due to third-party issues

### Fixed

- parcel-dependent features of Eagris (by moving QGIS related logic out of worker threads)

### Changed

- unification of function naming conventions to *snake_case*

## [1.2.0] -  2023-10-01
### Added

parcel detail (read only)

## [1.1.0] -  2023-07-30
### Added

- support of the LPI_DDP01B eAGRI service (DPB detail, including agricultural parcels)

## [1.0.0] - 2023-06-30

### Added

- management of authentication credentials
- support of the LPI_GDP11B eAGRI service (DPB list)
