FROM eagris/qgis-base

# mount only necessary files
COPY --chown=eagris:eagris ./tests/qgis        /home/eagris/tests/qgis
COPY --chown=eagris:eagris ./eagris            /home/eagris/eagris
COPY --chown=eagris:eagris ./setup.cfg         /home/eagris/setup.cfg
COPY --chown=eagris:eagris ./tests/__init__.py /home/eagris/tests/__init__.py

ENV PYTHONPATH=$PYTHONPATH:/usr/lib/python3/dist-packages
CMD . tests/bin/activate && python3 -m pytest -vvl tests/qgis