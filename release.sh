#!/bin/bash
OSGEO_DEFAULT_USERNAME=eagris
read -p "Username [${OSGEO_DEFAULT_USERNAME}]: " OSGEO_USER_NAME
read -sp 'Password: ' OSGEO_USER_PASSWORD
RELEASE_VERSION=$(grep 'version=' eagris/metadata.txt | sed 's/version=\([0-9.]*\).*/\1/')

OSGEO_USER_NAME=${OSGEO_USER_NAME:-$OSGEO_DEFAULT_USERNAME}

cat << EagrisReleaseInfo

=================================
OSGEO user name: $OSGEO_USER_NAME
release version: $RELEASE_VERSION
=================================
EagrisReleaseInfo

read -p "Do you want to release the plugin? (y/n) " yn

case $yn in
    y|Y|yes )
        qgis-plugin-ci release $RELEASE_VERSION --allow-uncommitted-changes --osgeo-username $OSGEO_USER_NAME --osgeo-password $OSGEO_USER_PASSWORD
	git tag $RELEASE_VERSION
	git push --tags
	echo "Release $RELEASE_VERSION successful.";;
    * ) echo "Release aborted."
esac

rm eagris/resources_rc.py eagris.$RELEASE_VERSION.zip
