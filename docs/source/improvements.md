## Possible improvements

1) The plugin currently assumes that all input geometries are in [S-JTSK EPSG:5514](https://epsg.io/5514). It could be useful
   to implement CRS auto recognition based on the WKT given.
2) Currently, *all* possible data are downloaded to QGIS. It would be sometimes useful to pick only a subset of layers.
3) Give user the ability to import authentication credentials from an external source (text/binary file, database).

A list of all current issues and improvements is available in the plugin repository [issue tracker](https://gitlab.com/zezulka/qgis-eagri/-/issues).