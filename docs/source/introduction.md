## General information
eAGRI is a critical source of information for many farmers in the Czech Republic in regard to the soil they manage.
However, the user interface which the Farmer Portal provides might be sometimes cumbersome to use. Eagris aims to be a more 
accessible alternative.

Since many agronomists already know how to use GIS software like Grass or QGIS, we decided to implement eagris as an extension to QGIS.

The idea is very straightforward: inside the plugin, you add authentication credentials to your eAGRI account and eagris
will download all possible data available from eAGRI web services as a set of QGIS layers, ready to be used for further analysis/processing.

If you have any improvements or other feedback to share, don't hesitate to create an issue on [Github](https://gitlab.com/zezulka/qgis-eagri/-/issues) or even better,
create a PR with your fixes/added functionality. Any feedback is very welcome!

The plugin is available for installation from the official plugin repository. If you're new to QGIS or you're not sure
how to install a plugin, [consult QGIS documentation](https://docs.qgis.org/latest/en/docs/training_manual/qgis_plugins/fetching_plugins.html).

### External Links
- [eAGRI portal](https://eagri.cz/public/web/mze/)
- [LPIS application (shows public DPB information)](https://eagri.cz/public/app/lpisext/lpis/verejny2/plpis/)
- [eAGRI SOAP WS](https://eagri.cz/public/web/mze/farmar/elektronicka-vymena-dat/prehled-vystavenych-sluzeb/)