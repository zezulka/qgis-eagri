Welcome to the eagris documentation.

> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **Python minimum version:** {{ python_version_min }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

```{toctree}
---
maxdepth: 1
---
introduction
installation
usage
improvements
```
