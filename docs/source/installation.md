## Prerequisites (for all installation types!)

Make sure Python environment used in your QGIS installation has installed all the [required packages](https://gitlab.com/zezulka/qgis-eagri/-/blob/master/requirements/minimum.txt).
Unit tests use Python 3 virtual environments. Make sure these are available, too.
You can either use embedded Python console in QGIS or for Window users, use the OSGeo4W shell.

Also, make sure libxml2 and libxslt development packages are installed in the environment you're running QGIS on, e.g.

```sudo apt-get install libxml2-dev libxslt-dev```

## Plugin installation

### Official QGIS plugin repository

If you only wish to try the plugin out, the most straightforward way is to download the plugin from the official
QGIS repository.
TBD

### Manual (tested on Linux only)

If you want to try out the latest version or like to dig deeper into the plugin, follow the instructions below.

Clone the eagris repository and at the root of this project, run `make all` . This will install eagris locally and QGIS will automatically recognize eagris as a Python plugin.
In QGIS plugin manager, make sure the eagris [plugin is enabled](https://docs.qgis.org/3.28/en/docs/training_manual/qgis_plugins/fetching_plugins.html).