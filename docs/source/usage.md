## How to use the plugin

The centerpiece component of the eagris plugin is a side panel, containing all eAGRI authentication credentials.
Each credential item is then available to be used for data imports (even after a user closes QGIS and returns back).

### Authentication credentials
#### Adding authentication credentials to the panel

![add auth credentials](_static/add_user.png)

Currently, the only way of adding authentication credentials is manually on an item-by-item basis. Credential import from external
source will be added in forthcoming releases.

![credentials input modal](_static/modal.png)

A short description of mandatory fields:

- `login`: the same login a user would use when logging in to the Farmer Portal
- `WS key`: a secret key generated on the Portal; once user loses this key, he is required to generate a new one

    - note that eagris stores WS keys in a hashed form and original WK keys CANNOT be retrieved back
- `SZR ID`: the subject ID used to download data from eAGRI web services

By default, the recommended way of adding new authentication credentials is via `Add` button. Pressing this button
verifies that the data is valid before saving. If the user does not wish to validate for any reason, it is possible
to add data directly via the `Force Add` button.

#### Downloading vector layers using eAGRI web services

Once we have at least one set of authentication credentials available in the panel, we can then import subject data
either using the download button or by double-clicking on the row the user wishes to import. A quick modal will appear
requesting user what data to import. If you're unsure, go with default values.

|                                        ![vector layer import](_static/download_eagri_layers.png)                                         |
|:----------------------------------------------------------------------------------------------------------------------------------------:| 
| *Download button enables user to import multiple data sets at once. Alternatively, one data set can be downloaded using a double click.* |


|                        ![vector layer import](_static/download_prompt.png)                        |
|:-------------------------------------------------------------------------------------------------:| 
| *After choosing eAGRI users, the following prompt appears to adjust which data will be imported.* |

Currently, eagris supports creation of the following layers:
- DPB (díl půdního bloku)
- agricultural parcels associated with all DPBs, if any
- agricultural parcel crops associated with all agricultural parcels, if any

#### Removing authentication credentials

All authentication credentials are persisted to a local unencrypted file. If user does not wish to persist credentials,
once of the possibilities is to remove them manually. This can be achieved by selecting rows in the panel to delete
and then either pressing `delete` key or pressing delete button at the top of the panel.

![credentials deletion](_static/delete_user.png)

### Parcel detail

Once user downloads eAGRI layers, it is possible to view each parcel detail by clicking on it in map, like
shown in the following animation.

![parcel_detail](_static/parcel_detail.gif)
*Parcel detail in the Eagris plugin is enabled using a map. Sensitive data were anonymised and/or blurred out.*

Currently, parcel detail is only read-only and in the foreseeable future will be. The plugin itself does
have capabilities to update a parcel detail to eAGRI; unfortunately, at the time of writing this (29th October 2023),
the eAGRI service for parcel update is not in working order.