from eagris.common.xml import serialize_xml
from eagris.eagri.crop_list.mapper import map_response
from eagris.eagri.ws.lpi_gpl01_c.client import request

if __name__ == "__main__":
    from eagris.eagri.env import EagriEnv
    r = request()
    print(serialize_xml(r))
    [print(c) for c in map_response(r)]
