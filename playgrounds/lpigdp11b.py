if __name__ == "__main__":
    from eagris.eagri.env import EagriEnv
    from eagris.eagri.auth import EagriAuthData
    from eagris.eagri.ws.lpi_gdp11_b.client import inject_signature_token_into_request, sendRequest, wsdlTest
    from eagris.eagri.dpb_list.mapper import map_response

    request = inject_signature_token_into_request(EagriAuthData(
        name='Demo Farm Name',
        env=EagriEnv.TEST,
        login='FILL_IN',
        szrid='FILL_IN',
        wskey='FILL_IN'
    ))
    response = sendRequest(request, wsdlTest)
    print(map_response(response))
